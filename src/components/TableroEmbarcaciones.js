import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Apiurl } from '../services/Apirest'
import "../css/TableroEmbarcaciones.css"
// import swal from 'sweetalert';
// import swal from '@sweetalert/with-react'
import 'animate.css';

const TableroEmbarcaciones = () => {
  const [data, setData] = useState([])
  const [indicators, setIndicators] = useState({
    AvailableSlips: "",
    BusySlips: "",
    TotalSlips: ""
  })
  const [errorConnection, setErrorConnection] = useState(false);
  const rightPosition = 'right';
  const leftPosition = 'left';
  const centerPosition = 'center';

  let url;
  let interval;
  let intervalErrorConnection;
  let NombresIndicadoresX = 14.329;
  let ValoresIndicadoresX = 85.028;
  let PosicionX_Slips600Der = 35.48;
  let PosicionX_Slips500Izq = 137.901;
  let PosicionX_Slips500Der = 167.501;
  let PosicionX_Slips400Der = 299.918;
  let PosicionX_Slips400Izq = 270.0;
  let PosicionX_Slips300Der = 433.801;//432.801
  let PosicionX_Slips300Izq = 403.801; //404.801
  let PosicionX_Slips200Der = 563.381;
  let PosicionX_Slips200Izq = 534.383;
  let PosicionX_Slips100Izq = 659.562;
  let PosicionX_Slips800Der = 621.799;
  let PosicionX_Slips800Izq = 592.062;

  let PosicionX_Slips706 = 461.989; //458.200
  let PosicionX_Slips705 = 410.634;//406.531
  let PosicionX_Slips704 = 358.631;//353.590
  let PosicionX_Slips703 = 304.981;//300.567
  let PosicionX_Slips702 = 253.053;//247.796
  let PosicionX_Slips701 = 199.65; //197.375
  let PosicionX_Slips700 = 144.704;//153.622

  let PosicionX_Pies300Der = 443.957;

  let PosicionY_Slips700 = 35.505;

  let agencia = {
    Empresa: 1,
    Sucursal: 1
  }

  useEffect(() => {
    getEmbarcaciones()
    refreshEmbarcaciones();
    return () => clearInterval(interval);
  }, [])

  useEffect(() => {
    if ( errorConnection ){
      clearInterval(interval);
      clearInterval(intervalErrorConnection)
      refreshEmbarcacionesNoConnection()
      // setTimeout(refreshEmbarcacionesNoConnection, 15000);
      return;
    }
    
    clearInterval(interval);
    clearInterval(intervalErrorConnection)
    refreshEmbarcaciones()
    // setTimeout(refreshEmbarcaciones, 30000);
    
  }, [errorConnection])
  
  const getObjectSlip = (slip) => {
    let slipFound = data.find(registro => registro.Slip === slip)
    return slipFound;
  }

  const refreshEmbarcaciones = () => {    
    interval = setInterval(() => {
      /* setEmbarcacion(embarcacion + ` ${cont}`)
      cont++; */
      getEmbarcaciones()

    }, 600000); //60000 = 1 minuto | 5000 = 5 segundos | 60 minutos = 3600000 | 30 minutos = 1800000 | 600000 = 10 min
  }

  const refreshEmbarcacionesNoConnection = () => {
    intervalErrorConnection = setInterval(() => {
      getEmbarcaciones()
    }, 120000); // 180000 = 3 minutos | 120000 = 2 minutos  
  }

  const getEmbarcaciones = async () => {
    url = Apiurl + "api/embarcaciones/get"
    await axios.post(url, agencia)
      .then(response => {
        setErrorConnection(false);
        setData(response['data']);
        console.log('response BD', response['data']);
        setIndicatorsValues(response['data']);
        clearInterval(intervalErrorConnection);
      })
      .catch(err => {
        setErrorConnection(true);
      })

  }

  const getNombreEmbarcacion = (slip, side) => {
    if ( getObjectSlip(slip) === undefined ) return "";
    if ( getObjectSlip(slip).Embarcacion !== 0 ) return side === rightPosition ? `  ${getObjectSlip(slip).Nombre_barco.substring(0,12)}`: `${getObjectSlip(slip).Nombre_barco.substring(0,12)}` // ${getObjectSlip(slip).Longitud} //Embarcacion 
    if ( getObjectSlip(slip).Embarcacion === 0 ) return getObjectSlip(slip).Compartido === "S" 
      ? side === rightPosition ? `  ${getObjectSlip(slip).Observacion}` : getObjectSlip(slip).Observacion 
      : side === rightPosition ? "  DISPONIBLE" : "DISPONIBLE" 
    
  }

  const getNombreClaseCSS = (slip, position) => {
    if ( getObjectSlip(slip) === undefined ) return position === "center"
    ? "text-color-normal slips-names-align-center"  
    : position === "right" 
    ? "text-color-normal slips-names-align-right" 
    : "text-color-normal slips-names-align-left";
    
    if ( getObjectSlip(slip).Embarcacion !== 0 ){
      if ( getObjectSlip(slip).Tipo  === "M") return position === "center"
      ? "text-color-normal slips-names-align-center"  
      : position === "right" 
      ? "text-color-normal slips-names-align-right" 
      : "text-color-normal slips-names-align-left"; 
      
      return position === "center"
      ? "text-color-velero slips-names-align-center" 
      : position === "right" 
      ? "text-color-velero slips-names-align-right" 
      : "text-color-velero slips-names-align-left";
    } 
   
    if ( getObjectSlip(slip).Embarcacion === 0 ){
      return getObjectSlip(slip).Compartido === "S" 
      ? position === "center"
      ? "text-color-compartido slips-names-align-center"
      : position === "right" 
      ? "text-color-compartido slips-names-align-right" 
      : "text-color-compartido slips-names-align-left" 
      
      : position === "center"
      ? "text-color-compartido slips-names-align-center"
      : position === "right" 
      ? "text-color-libre slips-names-align-right" 
      : "text-color-libre slips-names-align-left";
    } 
  }

  const getColorEnBaseFactura = (slip,position) => {

      if ( getObjectSlip(slip) === undefined ) return "transparentBullet"
      if ( getObjectSlip(slip).Embarcacion === 0 ) return "transparentBullet"
      if ( getObjectSlip(slip).diasVencidos > 30 ) return "cherryBullet"
      if ( getObjectSlip(slip).diasVencidos > 10 ) return "redBullet"
      return "transparentBullet"

  }

  const getBulletPosition = ( slip, positionX, side ) => {
    /* NOTA: Letra=3px, espacio entre letra=0.5px,  espacio entre palabra=1px*/
    // if ( getObjectSlip(slip) === undefined || getObjectSlip(slip).Embarcacion === 0 ) return positionX;
    // let pixeles = 0; 
    let posicionFinal = positionX;
    let tamañoCadena = 0;
    if ( side === leftPosition ) {
      tamañoCadena = calcularLongitud(slip)
      posicionFinal = (positionX - tamañoCadena) - 2; //- 3
      // pixeles = calcularTotalPixeles(tamañoCadena);
    }
    if ( side === rightPosition ) {
      tamañoCadena = calcularLongitud(slip)
      posicionFinal = (positionX + tamañoCadena) + 2; //+ 3.5
    }
    if ( side === centerPosition ) {
      tamañoCadena = calcularLongitud(slip)
      posicionFinal = (positionX + (tamañoCadena / 2)) + 1;
    }
    return posicionFinal;
  }

  const calcularTotalPixeles = ( cadena ) => {
    /* 1.- obtener el total de espacios               | letra = 3.2px,
       2.- obtener el total de letras                 | espacioentreletra= .5px,
       3.- obtener el total de espacios entre letras. | espacioentrepalabra=1px

        E    L      A    M    I    G    O      1    1    7
        3 .5 3   1  3 .5 3 .5 3 .5 3 .5 3   1  3 .5 3 .5 3
    */
    let cantidadTotal = 0;
    let totalLetras = 0;
    let totalEspaciosEntreLetras = 0;
    let totalEspacios = cadena.split(" ")

    if ( totalEspacios.length === 0 ){
       totalLetras = cadena;
       totalEspaciosEntreLetras = cadena.length - 1;
    }
    
    if ( totalEspacios.length > 0 ) {
      let sumaTotalLetras = 0;
      let sumaTotalEspaciosEntreLetras = 0;
      for (const piece of totalEspacios) {
        sumaTotalLetras = sumaTotalLetras + piece.length;
        sumaTotalEspaciosEntreLetras = piece.length > 1  ? sumaTotalEspaciosEntreLetras + piece.length - 1: sumaTotalEspaciosEntreLetras;  
      }
      totalLetras = sumaTotalLetras;
      totalEspaciosEntreLetras = sumaTotalEspaciosEntreLetras;
    }

    cantidadTotal = (totalLetras * 3.2) + (totalEspaciosEntreLetras * 0.65) + (totalEspacios.length > 2 ? totalEspacios.length - 1 : totalEspacios.length > 1 ? 1 : 0)
    return cantidadTotal;
  }

  const calcularLongitud = (slip) => {
    let cadena = '';
    let tamañoCadena = 0;
    if ( getObjectSlip(slip) === undefined ) return tamañoCadena;
    if ( getObjectSlip(slip).Embarcacion !== 0 ){
       cadena = `${getObjectSlip(slip).Nombre_barco.substring(0,12)} ${getObjectSlip(slip).Longitud}`
       
       tamañoCadena = calcularTotalPixeles(cadena);
      //  tamañoCadena = cadena.length;
       return tamañoCadena;
    }
    
    if ( getObjectSlip(slip).Embarcacion === 0 ){
      if ( getObjectSlip(slip).Compartido === "S" ) cadena = getObjectSlip(slip).Observacion;
      else cadena = "DISPONIBLE" 

      // tamañoCadena = cadena.length;
      tamañoCadena = calcularTotalPixeles(cadena);
      return tamañoCadena;
    }
  }

  const getTamañoEmbarcacion = ( slip, side ) => {
    if ( getObjectSlip(slip) === undefined ) return "";
    if ( getObjectSlip(slip).Embarcacion !== 0 ) return side === leftPosition ? ` ${getObjectSlip(slip).Longitud}  ` : ` ${getObjectSlip(slip).Longitud}`//Embarcacion 
    if ( getObjectSlip(slip).Embarcacion === 0 ) return getObjectSlip(slip).Compartido === "S" 
    ? side === leftPosition ? "  " : "" 
    : side === leftPosition ? "  " : "" //1. compartido / 2. disponible
  }

  const getColorLongitudCSS = ( slip ) => {
    if ( getObjectSlip(slip) === undefined ) return "undefined"
    if ( getObjectSlip(slip).Embarcacion !== 0 ){

      if ( getObjectSlip(slip).Longitud < getObjectSlip(slip).Pies ) return "longitudMenorMuelle"
      if ( getObjectSlip(slip).Tipo  === "M") return "longitudMayorMuelleBarco"
      if ( getObjectSlip(slip).Tipo  !== "M") return "longitudMayorMuelleVelero"

    }
    
  }

  const getPieNumber = ( slip1, slip2, defaultValue) => {    
    if ( getObjectSlip(slip1) === undefined || getObjectSlip(slip2) === undefined ) return `${defaultValue}'  `;
    if ( slip1 === 201 || slip1 === 301 || slip1 === 401 || slip1 === 501 || slip1 === 813 ) return `${defaultValue}'  `
    if ( getObjectSlip(slip1).Pies !== 0 ) return `${getObjectSlip(slip1).Pies}'  `; 
    if ( getObjectSlip(slip2).Pies !== 0 ) return `${getObjectSlip(slip2).Pies}'  `; 
    return `${defaultValue}'  `
  }

  const setIndicatorsValues = (data) => {
    const total_slips = getTotalSlips(data);
    const available_slips = getAvailableSlips(data);
    const busy_slips = getBusySlips(total_slips, available_slips);

    setIndicators({
      ...indicators,
      AvailableSlips: available_slips,
      BusySlips: busy_slips,
      TotalSlips: total_slips
    })

  }
  
  const getAvailableSlips = (data) => {
    const available_slips = data.reduce(( cont, element) => {
      if ( element.Embarcacion === 0 ) element.Compartido === "N" && cont++;
      return cont;
    },0)

    //-1 = slip combustible
    return available_slips - 1;
  }
  
  const getBusySlips = ( total_slips, available_slips ) => {
    const busy_slips = total_slips - available_slips;
    return busy_slips;
  }
  
  const getTotalSlips = (data) => {
    //-1 = slip combustible
    const total_slips = data.length - 1;
    return total_slips;
  }


  return (
    <>


      <svg
        xmlns="http://www.w3.org/2000/svg"
        id="svg5"
        width="100%"
        height="100%"
        // width="2532.859"
        // height="1247.692"
        version="1.1"
        viewBox="0 0 670.152 330.119"
        xmlSpace="preserve"
      >
        <g
          id="layer1"
          strokeWidth="0.265"
          display="inline"
          fontFamily="Arial"
          fontSize="5.644"
          textAnchor="middle"
          transform="translate(-5.848 -6.058)"
        >
          
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS812-813"
            x="561.148"
            y="54.261"
          >
            <tspan
              id="tspan6266"
              x="561.148"
              y="54.261"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 90&apos; */}
              {/* {`${getObjectSlip(813) !== undefined ?  getObjectSlip(813).Longitud !== 0 ? `${getObjectSlip(813).Longitud}'` : getObjectSlip(812).Longitud !== 0 ? `${getObjectSlip(812).Longitud}'` : "90'"  : "90'"}`} */}
              {getPieNumber(813, 812, 90)}
            </tspan>
          </text>
          <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          x="580.444"
          y="75.633"
          display="inline"
        >
          <tspan x="580.444" y="75.633" strokeWidth="0.265" className='text-color-size-pies'>
            {/* 90&apos; */}
            {getPieNumber(808, 807, 90)}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          x="580.444"
          y="106.881"
          display="inline"
        >
          <tspan x="580.444" y="106.881" strokeWidth="0.265" className='text-color-size-pies'>
           {/* 52&apos; */}
           {getPieNumber(804, 803, 52)}
          </tspan>
        </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS811-814"
            x="636.684"
            y="54.524"
            display="inline"
          >
            <tspan
              id="tspan6266-1"
              x="637.684"
              y="54.524"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 56&apos; */}
              {/* {`${getObjectSlip(814) !== undefined ?  getObjectSlip(814).Longitud !== 0 ? `${getObjectSlip(814).Longitud}'` : getObjectSlip(811).Longitud !== 0 ? `${getObjectSlip(811).Longitud}'` : "56'"  : "56'"}`} */}
              {getPieNumber(814, 811, 56)}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS809-810"
            x="634.83"
            y="75.537"
            display="inline"
          >
            <tspan
              id="tspan6266-1-5"
              x="637.684"
              y="75.537"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 52&apos; */}
              {/* {`${getObjectSlip(810) !== undefined ?  getObjectSlip(810).Longitud !== 0 ? `${getObjectSlip(810).Longitud}'` : getObjectSlip(809).Longitud !== 0 ? `${getObjectSlip(809).Longitud}'` : "52'"  : "52'"}`} */}
              {getPieNumber(810, 809, 52)}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS805-806"
            x="634.583"
            y="96.302"
            display="inline"
          >
            <tspan
              id="tspan6266-1-5-1"
              x="637.684"
              y="96.302"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 52&apos; */}
              {/* {`${getObjectSlip(806) !== undefined ?  getObjectSlip(806).Longitud !== 0 ? `${getObjectSlip(806).Longitud}'` : getObjectSlip(805).Longitud !== 0 ? `${getObjectSlip(805).Longitud}'` : "52'"  : "52'"}`} */}
              {getPieNumber(806, 805, 52)}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS802-801"
            x="634.706"
            y="117.437"
            display="inline"
          >
            <tspan
              id="tspan6266-1-5-0"
              x="637.684"
              y="117.437"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 52&apos; */}
              {/* {`${getObjectSlip(802) !== undefined ?  getObjectSlip(802).Longitud !== 0 ? `${getObjectSlip(802).Longitud}'` : getObjectSlip(801).Longitud !== 0 ? `${getObjectSlip(801).Longitud}'` : "52'"  : "52'"}`} */}
              {getPieNumber(802, 801, 52)}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text750-5-4-9-9-4-8-6-0-5-8-4-3-45"
            x="460.521"
            y="24.35"
            display="inline"
            transform="rotate(.285)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-0-6-60"
              x="460.521"
              y="24.35"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 120&apos; */}
              {/* {`${getObjectSlip(706) !== undefined ?  getObjectSlip(706).Longitud !== 0 ? `${getObjectSlip(706).Longitud}'` : "120'"  : "120'"}`} */}
              {`  ${getPieNumber(706, 706, 120)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text750-5-4-9-9-4-8-6-0-5-8-4-3-4"
            x="410.629"
            y="24.598"
            display="inline"
            transform="rotate(.285)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-0-6-5"
              x="410.629"
              y="24.598"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 120&apos; */}
              {/* {`${getObjectSlip(705) !== undefined ?  getObjectSlip(705).Longitud !== 0 ? `${getObjectSlip(705).Longitud}'` : "120'"  : "120'"}`} */}
              {`  ${getPieNumber(705, 705, 120)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text750-5-4-9-9-4-8-6-0-5-8-4-3-2"
            x="359"
            y="24.855"
            display="inline"
            transform="rotate(.285)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-0-6-7"
              x="359"
              y="24.855"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 120&apos; */}
              {/* {`${getObjectSlip(704) !== undefined ?  getObjectSlip(704).Longitud !== 0 ? `${getObjectSlip(704).Longitud}'` : "120'"  : "120'"}`} */}
              {`  ${getPieNumber(704, 704, 120)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text750-5-4-9-9-4-8-6-0-5-8-4-3-6"
            x="304.051"
            y="25.128"
            display="inline"
            transform="rotate(.285)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-0-6-6"
              x="304.051"
              y="25.128"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 120&apos; */}
              {/* {`${getObjectSlip(703) !== undefined ?  getObjectSlip(703).Longitud !== 0 ? `${getObjectSlip(703).Longitud}'` : "120'"  : "120'"}`} */}
              {`  ${getPieNumber(703, 703, 120)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text750-5-4-9-9-4-8-6-0-5-8-4-3"
            x="251.254"
            y="25.39"
            display="inline"
            transform="rotate(.285)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-0-6"
              x="251.254"
              y="25.39"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 120&apos; */}
              {/* {`${getObjectSlip(702) !== undefined ?  getObjectSlip(702).Longitud !== 0 ? `${getObjectSlip(702).Longitud}'` : "120'"  : "120'"}`} */}
              {`  ${getPieNumber(702, 702, 120)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text750-5-4-9-9-4-8-6-0-5-8-4-5"
            x="197.487"
            y="25.651" //34.098
            display="inline"
            transform="rotate(.285)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-0-9"
              x="197.487"
              y="25.651" //34.098
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 92&apos; */}
              {/* {`${getObjectSlip(701) !== undefined ?  getObjectSlip(701).Longitud !== 0 ? `${getObjectSlip(701).Longitud}'` : "92'"  : "92'"}`} */}
              {`  ${getPieNumber(701, 701, 92)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text750-5-4-9-9-4-8-6-0-5-8-4"
            x="143.005"//153.63
            y="25.891" //34.28
            display="inline"
            transform="rotate(.285)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-0"
              x="143.005"//153.63
              y="25.891" //34.28
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 54&apos; */}
              {/* {`${getObjectSlip(700) !== undefined ?  getObjectSlip(700).Longitud !== 0 ? `${getObjectSlip(700).Longitud}'` : "54'"  : "54'"}`} */}
              {`  ${getPieNumber(700, 700, 54)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text750-5-4-9-9-4-8-6-0-5-8-57-3-8-4-7-4-7"
            x="65.515"
            y="311.873"
            transform="rotate(1.151)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-8-5-3-3-0-0"
              x="65.515"
              y="311.873"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 60&apos; */}
              {/* {`${getObjectSlip(613) !== undefined ?  getObjectSlip(613).Longitud !== 0 ? `${getObjectSlip(613).Longitud}'` : getObjectSlip(614).Longitud !== 0 ? `${getObjectSlip(614).Longitud}'` : "60'"  : "60'"}`} */}
              {`  ${getPieNumber(613, 614, 60)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11882-95-7"
            x="57.466"
            y="292.057"
            display="inline"
          >
            <tspan id="tspan11880-1-6" x="57.466" y="292.057" strokeWidth="0.265" className='text-color-size-pies'>
              {/* 60&apos; */}
              {/* {`${getObjectSlip(611) !== undefined ?  getObjectSlip(611).Longitud !== 0 ? `${getObjectSlip(611).Longitud}'` : getObjectSlip(612).Longitud !== 0 ? `${getObjectSlip(612).Longitud}'` : "60'"  : "60'"}`} */}
              {`  ${getPieNumber(611, 612, 60)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11882-95-4"
            x="57.466"
            y="270.057"
            display="inline"
          >
            <tspan id="tspan11880-1-2" x="57.466" y="270.057" strokeWidth="0.265" className='text-color-size-pies'>
              {/* 60&apos; */}
              {/* {`${getObjectSlip(609) !== undefined ?  getObjectSlip(609).Longitud !== 0 ? `${getObjectSlip(609).Longitud}'` : getObjectSlip(610).Longitud !== 0 ? `${getObjectSlip(610).Longitud}'` : "60'"  : "60'"}`} */}
              {`  ${getPieNumber(609, 610, 60)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11882-95"
            x="57.466"
            y="249.057"
            display="inline"
          >
            <tspan id="tspan11880-1" x="57.466" y="249.057" strokeWidth="0.265" className='text-color-size-pies'>
              {/* 60&apos; */}
              {/* {`${getObjectSlip(607) !== undefined ?  getObjectSlip(607).Longitud !== 0 ? `${getObjectSlip(607).Longitud}'` : getObjectSlip(608).Longitud !== 0 ? `${getObjectSlip(608).Longitud}'` : "60'"  : "60'"}`} */}
              {`  ${getPieNumber(607, 608, 60)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11882"
            x="63.133"
            y="228.057"
          >
            <tspan id="tspan11880" x="63.133" y="228.057" strokeWidth="0.265" className='text-color-size-pies'>
              {/* 100&apos; */}
              {/* {`${getObjectSlip(605) !== undefined ?  getObjectSlip(605).Longitud !== 0 ? `${getObjectSlip(605).Longitud}'` : getObjectSlip(606).Longitud !== 0 ? `${getObjectSlip(606).Longitud}'` : "100'"  : "100'"}`} */}
              {`  ${getPieNumber(605, 606, 100)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11882-6"
            x="63.133"
            y="217.56"
            display="inline"
          >
            <tspan id="tspan11880-2" x="63.133" y="217.56" strokeWidth="0.265" className='text-color-size-pies'>
              {/* 100&apos; */}
              {/* {`${getObjectSlip(604) !== undefined ?  getObjectSlip(604).Longitud !== 0 ? `${getObjectSlip(604).Longitud}'` : getObjectSlip(605).Longitud !== 0 ? `${getObjectSlip(605).Longitud}'` : "100'"  : "100'"}`} */}
              {`  ${getPieNumber(604, 605, 100)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS603-604"
            x="63.133"
            y="207.057"
            display="inline"
          >
            <tspan id="tspan11880-9" x="63.133" y="207.057" strokeWidth="0.265" className='text-color-size-pies'>
              {/* 100&apos; */}
              {/* {`${getObjectSlip(603) !== undefined ?  getObjectSlip(603).Longitud !== 0 ? `${getObjectSlip(603).Longitud}'` : getObjectSlip(604).Longitud !== 0 ? `${getObjectSlip(604).Longitud}'` : "100'"  : "100'"}`} */}
              {`  ${getPieNumber(603, 604, 100)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS601-602"
            x="61.867"
            y="185.464"
            transform="rotate(.591)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-8-5"
              x="61.867"
              y="185.464"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 80&apos; */}
              {/* {`${getObjectSlip(601) !== undefined ?  getObjectSlip(601).Longitud !== 0 ? `${getObjectSlip(601).Longitud}'` : getObjectSlip(602).Longitud !== 0 ? `${getObjectSlip(602).Longitud}'` : "80'"  : "80'"}`} */}
              {`  ${getPieNumber(601, 602, 80)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS530-532"
            x="187.98"
            y="312.406"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-9"
              x="187.98"
              y="312.406"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(530) !== undefined ?  getObjectSlip(530).Longitud !== 0 ? `${getObjectSlip(530).Longitud}'` : getObjectSlip(532).Longitud !== 0 ? `${getObjectSlip(532).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(530, 532, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS529-531"
            x="126.695"
            y="312.619"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-869-4"
              x="126.695"
              y="312.619"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(529) !== undefined ?  getObjectSlip(529).Longitud !== 0 ? `${getObjectSlip(529).Longitud}'` : getObjectSlip(531).Longitud !== 0 ? `${getObjectSlip(531).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(529, 531, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS526-528"
            x="188.601"
            y="291.403"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-38"
              x="188.601"
              y="291.403"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(526) !== undefined ?  getObjectSlip(526).Longitud !== 0 ? `${getObjectSlip(526).Longitud}'` : getObjectSlip(528).Longitud !== 0 ? `${getObjectSlip(528).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(526, 528, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS525-527"
            x="126.622"
            y="291.619"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-869-39"
              x="126.622"
              y="291.619"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(525) !== undefined ?  getObjectSlip(525).Longitud !== 0 ? `${getObjectSlip(525).Longitud}'` : getObjectSlip(527).Longitud !== 0 ? `${getObjectSlip(527).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(525, 527, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS522-524"
            x="188.684"
            y="269.403"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-7"
              x="188.684"
              y="269.403"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(522) !== undefined ?  getObjectSlip(522).Longitud !== 0 ? `${getObjectSlip(522).Longitud}'` : getObjectSlip(524).Longitud !== 0 ? `${getObjectSlip(524).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(522, 524, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS521-523"
            x="126.546"
            y="269.619"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-869-8"
              x="126.546"
              y="269.619"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(521) !== undefined ?  getObjectSlip(521).Longitud !== 0 ? `${getObjectSlip(521).Longitud}'` : getObjectSlip(523).Longitud !== 0 ? `${getObjectSlip(523).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(521, 523, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS518-520"
            x="187.227"
            y="248.408"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-0"
              x="187.227"
              y="248.408"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(518) !== undefined ?  getObjectSlip(518).Longitud !== 0 ? `${getObjectSlip(518).Longitud}'` : getObjectSlip(520).Longitud !== 0 ? `${getObjectSlip(520).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(518, 520, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS517-519"
            x="126.472"
            y="248.619"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-869-75"
              x="126.472"
              y="248.619"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(517) !== undefined ?  getObjectSlip(517).Longitud !== 0 ? `${getObjectSlip(517).Longitud}'` : getObjectSlip(519).Longitud !== 0 ? `${getObjectSlip(519).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(517, 519, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS514-516"
            x="186.827"
            y="227.409"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-86"
              x="186.827"
              y="227.409"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(514) !== undefined ?  getObjectSlip(514).Longitud !== 0 ? `${getObjectSlip(514).Longitud}'` : getObjectSlip(516).Longitud !== 0 ? `${getObjectSlip(516).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(514, 516, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS513-515"
            x="126.399"
            y="227.619"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-869-3"
              x="126.399"
              y="227.619"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(513) !== undefined ?  getObjectSlip(513).Longitud !== 0 ? `${getObjectSlip(513).Longitud}'` : getObjectSlip(515).Longitud !== 0 ? `${getObjectSlip(515).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(513, 515, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS510-512"
            x="186.822"
            y="206.409"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-2"
              x="186.822"
              y="206.409"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(510) !== undefined ?  getObjectSlip(510).Longitud !== 0 ? `${getObjectSlip(510).Longitud}'` : getObjectSlip(512).Longitud !== 0 ? `${getObjectSlip(512).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(510, 512, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS509-511"
            x="126.326"
            y="206.62"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-869-7"
              x="126.326"
              y="206.62"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(509) !== undefined ?  getObjectSlip(509).Longitud !== 0 ? `${getObjectSlip(509).Longitud}'` : getObjectSlip(511).Longitud !== 0 ? `${getObjectSlip(511).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(509, 511, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS506-508"
            x="186.962"
            y="185.408"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-3"
              x="186.962"
              y="185.408"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(506) !== undefined ?  getObjectSlip(506).Longitud !== 0 ? `${getObjectSlip(506).Longitud}'` : getObjectSlip(508).Longitud !== 0 ? `${getObjectSlip(508).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(506, 508, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS505-507"
            x="126.253"
            y="185.62"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-869"
              x="126.253"
              y="185.62"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(505) !== undefined ?  getObjectSlip(505).Longitud !== 0 ? `${getObjectSlip(505).Longitud}'` : getObjectSlip(507).Longitud !== 0 ? `${getObjectSlip(507).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(505, 507, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS502-504"
            x="185.139"
            y="164.415"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222"
              x="185.139"
              y="164.415"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(502) !== undefined ?  getObjectSlip(502).Longitud !== 0 ? `${getObjectSlip(502).Longitud}'` : getObjectSlip(504).Longitud !== 0 ? `${getObjectSlip(504).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(502, 504, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS501-503"
            x="121.638"
            y="164.636"
            transform="rotate(.2)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7-222-8"
              x="121.638"
              y="164.636"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(501) !== undefined ?  getObjectSlip(501).Longitud !== 0 ? `${getObjectSlip(501).Longitud}'` : getObjectSlip(503).Longitud !== 0 ? `${getObjectSlip(503).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(501, 503, 50)}`}
            </tspan>
          </text>
          <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="TLM5"
          x="153.009"
          y="151.887"
          className='text-color-size-pies'
          display="inline"
        >
          <tspan
            id="tspan3711-3-1"
            x="153.009"
            y="151.887"
            strokeWidth="0.265"
            fontSize="5.644"
          >
            M5: 103&apos;
          </tspan>
        </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS434-436"
            x="317.606"
            y="313.057"
            display="inline"
          >
            <tspan
              id="tspan25387-30"
              x="317.606"
              y="313.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(434) !== undefined ?  getObjectSlip(434).Longitud !== 0 ? `${getObjectSlip(434).Longitud}'` : getObjectSlip(436).Longitud !== 0 ? `${getObjectSlip(436).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(434, 436, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS433-435"
            x="254.606"
            y="313.057"
            display="inline"
          >
            <tspan
              id="tspan25387-26"
              x="254.606"
              y="313.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(433) !== undefined ?  getObjectSlip(433).Longitud !== 0 ? `${getObjectSlip(433).Longitud}'` : getObjectSlip(435).Longitud !== 0 ? `${getObjectSlip(435).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(433, 435, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS430-432"
            x="317.606"
            y="292.057"
            display="inline"
          >
            <tspan
              id="tspan25387-4"
              x="317.606"
              y="292.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(430) !== undefined ?  getObjectSlip(430).Longitud !== 0 ? `${getObjectSlip(430).Longitud}'` : getObjectSlip(432).Longitud !== 0 ? `${getObjectSlip(432).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(430, 432, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS429-431"
            x="254.606"
            y="292.057"
            display="inline"
          >
            <tspan
              id="tspan25387-7"
              x="254.606"
              y="292.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(429) !== undefined ?  getObjectSlip(429).Longitud !== 0 ? `${getObjectSlip(429).Longitud}'` : getObjectSlip(431).Longitud !== 0 ? `${getObjectSlip(431).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(429, 431, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS426-428"
            x="317.606"
            y="270.057"
            display="inline"
          >
            <tspan
              id="tspan25387-11"
              x="317.606"
              y="270.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(426) !== undefined ?  getObjectSlip(426).Longitud !== 0 ? `${getObjectSlip(426).Longitud}'` : getObjectSlip(428).Longitud !== 0 ? `${getObjectSlip(428).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(426, 428, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS425-427"
            x="254.606"
            y="270.057"
            display="inline"
          >
            <tspan
              id="tspan25387-23"
              x="254.606"
              y="270.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(425) !== undefined ?  getObjectSlip(425).Longitud !== 0 ? `${getObjectSlip(425).Longitud}'` : getObjectSlip(427).Longitud !== 0 ? `${getObjectSlip(427).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(425, 427, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS422-424"
            x="317.606"
            y="249.057"
            display="inline"
          >
            <tspan
              id="tspan25387-1"
              x="317.606"
              y="249.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(422) !== undefined ?  getObjectSlip(422).Longitud !== 0 ? `${getObjectSlip(422).Longitud}'` : getObjectSlip(424).Longitud !== 0 ? `${getObjectSlip(424).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(422, 424, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS421-423"
            x="256.606"
            y="249.057"
            display="inline"
          >
            <tspan
              id="tspan25387-83"
              x="256.606"
              y="249.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(421) !== undefined ?  getObjectSlip(421).Longitud !== 0 ? `${getObjectSlip(421).Longitud}'` : getObjectSlip(423).Longitud !== 0 ? `${getObjectSlip(423).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(421, 423, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS418-420"
            x="317.606"
            y="228.057"
            display="inline"
          >
            <tspan
              id="tspan25387-3"
              x="317.606"
              y="228.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(418) !== undefined ?  getObjectSlip(418).Longitud !== 0 ? `${getObjectSlip(418).Longitud}'` : getObjectSlip(420).Longitud !== 0 ? `${getObjectSlip(420).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(418, 420, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS417-419"
            x="256.606"
            y="228.057"
            display="inline"
          >
            <tspan
              id="tspan25387-16"
              x="256.606"
              y="228.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(417) !== undefined ?  getObjectSlip(417).Longitud !== 0 ? `${getObjectSlip(417).Longitud}'` : getObjectSlip(419).Longitud !== 0 ? `${getObjectSlip(419).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(417, 419, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS414-416"
            x="317.606"
            y="207.057"
            display="inline"
          >
            <tspan
              id="tspan25387-8"
              x="317.606"
              y="207.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(414) !== undefined ?  getObjectSlip(414).Longitud !== 0 ? `${getObjectSlip(414).Longitud}'` : getObjectSlip(416).Longitud !== 0 ? `${getObjectSlip(416).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(414, 416, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS413-415"
            x="256.606"
            y="207.057"
            display="inline"
          >
            <tspan
              id="tspan25387-6"
              x="256.606"
              y="207.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(413) !== undefined ?  getObjectSlip(413).Longitud !== 0 ? `${getObjectSlip(413).Longitud}'` : getObjectSlip(415).Longitud !== 0 ? `${getObjectSlip(415).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(413, 415, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS410-412"
            x="317.606"
            y="186.057"
            display="inline"
          >
            <tspan
              id="tspan25387-2"
              x="317.606"
              y="186.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(410) !== undefined ?  getObjectSlip(410).Longitud !== 0 ? `${getObjectSlip(410).Longitud}'` : getObjectSlip(412).Longitud !== 0 ? `${getObjectSlip(412).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(410, 412, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS409-411"
            x="256.606"
            y="186.057"
            display="inline"
          >
            <tspan
              id="tspan25387-73"
              x="256.606"
              y="186.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(409) !== undefined ?  getObjectSlip(409).Longitud !== 0 ? `${getObjectSlip(409).Longitud}'` : getObjectSlip(411).Longitud !== 0 ? `${getObjectSlip(411).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(409, 411, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS406-408"
            x="317.606"
            y="165.057"
          >
            <tspan
              id="tspan25387"
              x="317.606"
              y="165.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(406) !== undefined ?  getObjectSlip(406).Longitud !== 0 ? `${getObjectSlip(406).Longitud}'` : getObjectSlip(408).Longitud !== 0 ? `${getObjectSlip(408).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(406, 408, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS405-407"
            x="256.606"
            y="165.057"
            display="inline"
          >
            <tspan
              id="tspan25387-64"
              x="256.606"
              y="165.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(405) !== undefined ?  getObjectSlip(405).Longitud !== 0 ? `${getObjectSlip(405).Longitud}'` : getObjectSlip(407).Longitud !== 0 ? `${getObjectSlip(407).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(405, 407, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS402-404"
            x="318.318"
            y="142.478"
            transform="rotate(.285)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71-7"
              x="318.318"
              y="142.478"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(402) !== undefined ?  getObjectSlip(402).Longitud !== 0 ? `${getObjectSlip(402).Longitud}'` : getObjectSlip(404).Longitud !== 0 ? `${getObjectSlip(404).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(402, 404, 40)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS401-403"
            x="256.606"
            y="144.057"
            display="inline"
          >
            <tspan
              id="tspan25387-40"
              x="256.606"
              y="144.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 40&apos; */}
              {/* {`${getObjectSlip(401) !== undefined ?  getObjectSlip(401).Longitud !== 0 ? `${getObjectSlip(401).Longitud}'` : getObjectSlip(403).Longitud !== 0 ? `${getObjectSlip(403).Longitud}'` : "40'"  : "40'"}`} */}
              {`  ${getPieNumber(401, 403, 40)}`}
            </tspan>
          </text>
          <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="TLM4"
          x="285.849"
          y="130.689"
          className='text-color-size-pies'
          display="inline"
        >
          <tspan
            id="tspan3711-3-5"
            x="285.849"
            y="130.689"
            strokeWidth="0.265"
            fontSize="5.644"
          >
            M4: 88&apos;
          </tspan>
        </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS342-344"
            x="449.957"
            y="310.776"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5"
              x={PosicionX_Pies300Der} //449.957
              y="310.776"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(342) !== undefined ?  getObjectSlip(342).Longitud !== 0 ? `${getObjectSlip(342).Longitud}'` : getObjectSlip(344).Longitud !== 0 ? `${getObjectSlip(344).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(342, 344, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS341-343"
            x="397.038"
            y="311.047"
            display="inline"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-0-1"
              x="397.038"
              y="311.047"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(341) !== undefined ?  getObjectSlip(341).Longitud !== 0 ? `${getObjectSlip(341).Longitud}'` : getObjectSlip(343).Longitud !== 0 ? `${getObjectSlip(343).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(341, 343, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS338-340"
            x="450.579"
            y="289.773"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-29"
              x={PosicionX_Pies300Der}
              y="289.773"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(338) !== undefined ?  getObjectSlip(338).Longitud !== 0 ? `${getObjectSlip(338).Longitud}'` : getObjectSlip(340).Longitud !== 0 ? `${getObjectSlip(340).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(338, 340, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS337-339"
            x="396.931"
            y="290.047"
            display="inline"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-0-7"
              x="396.931"
              y="290.047"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(337) !== undefined ?  getObjectSlip(337).Longitud !== 0 ? `${getObjectSlip(337).Longitud}'` : getObjectSlip(339).Longitud !== 0 ? `${getObjectSlip(339).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(337, 339, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS334-336"
            x="450.563"
            y="267.772"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-24"
              x={PosicionX_Pies300Der}
              y="267.772"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(334) !== undefined ?  getObjectSlip(334).Longitud !== 0 ? `${getObjectSlip(334).Longitud}'` : getObjectSlip(336).Longitud !== 0 ? `${getObjectSlip(336).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(334, 336, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS333-335"
            x="396.818"
            y="268.047"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-0"
              x="396.818"
              y="268.047"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(333) !== undefined ?  getObjectSlip(333).Longitud !== 0 ? `${getObjectSlip(333).Longitud}'` : getObjectSlip(335).Longitud !== 0 ? `${getObjectSlip(335).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(333, 335, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS330-332"
            x="451.564"
            y="246.767"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-89"
              x={PosicionX_Pies300Der}
              y="246.767"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(330) !== undefined ?  getObjectSlip(330).Longitud !== 0 ? `${getObjectSlip(330).Longitud}'` : getObjectSlip(332).Longitud !== 0 ? `${getObjectSlip(332).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(330, 332, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS329-331"
            x="394.876"
            y="247.041"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4"
              x="394.876"
              y="247.041"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 44&apos; */}
              {/* {`${getObjectSlip(329) !== undefined ?  getObjectSlip(329).Longitud !== 0 ? `${getObjectSlip(329).Longitud}'` : getObjectSlip(331).Longitud !== 0 ? `${getObjectSlip(331).Longitud}'` : "44'"  : "44'"}`} */}
              {`  ${getPieNumber(329, 331, 44)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS326-328"
            x="451.654"
            y="225.766"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-8"
              x={PosicionX_Pies300Der}
              y="225.766"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(326) !== undefined ?  getObjectSlip(326).Longitud !== 0 ? `${getObjectSlip(326).Longitud}'` : getObjectSlip(328).Longitud !== 0 ? `${getObjectSlip(328).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(326, 328, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS325-327"
            x="394.768"
            y="226.042"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-0"
              x="394.768"
              y="226.042"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 44&apos; */}
              {/* {`${getObjectSlip(325) !== undefined ?  getObjectSlip(325).Longitud !== 0 ? `${getObjectSlip(325).Longitud}'` : getObjectSlip(327).Longitud !== 0 ? `${getObjectSlip(327).Longitud}'` : "44'"  : "44'"}`} */}
              {`  ${getPieNumber(325, 327, 44)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS322-324"
            x="451.743"
            y="204.766"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-7"
              x={PosicionX_Pies300Der}
              y="204.766"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(322) !== undefined ?  getObjectSlip(322).Longitud !== 0 ? `${getObjectSlip(322).Longitud}'` : getObjectSlip(324).Longitud !== 0 ? `${getObjectSlip(324).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(322, 324, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS321-323"
            x="394.661"
            y="205.042"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-7"
              x="394.661"
              y="205.042"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 44&apos; */}
              {/* {`${getObjectSlip(321) !== undefined ?  getObjectSlip(321).Longitud !== 0 ? `${getObjectSlip(321).Longitud}'` : getObjectSlip(323).Longitud !== 0 ? `${getObjectSlip(323).Longitud}'` : "44'"  : "44'"}`} */}
              {`  ${getPieNumber(321, 323, 44)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS318-320"
            x="451.681"
            y="183.766"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-2"
              x={PosicionX_Pies300Der}
              y="183.766"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(318) !== undefined ?  getObjectSlip(318).Longitud !== 0 ? `${getObjectSlip(318).Longitud}'` : getObjectSlip(320).Longitud !== 0 ? `${getObjectSlip(320).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(318, 320, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS317-319"
            x="394.553"
            y="184.042"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-77"
              x="394.553"
              y="184.042"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 44&apos; */}
              {/* {`${getObjectSlip(317) !== undefined ?  getObjectSlip(317).Longitud !== 0 ? `${getObjectSlip(317).Longitud}'` : getObjectSlip(319).Longitud !== 0 ? `${getObjectSlip(319).Longitud}'` : "44'"  : "44'"}`} */}
              {`  ${getPieNumber(317, 319, 44)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS314-316"
            x="452.302"
            y="162.762"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-98"
              x={PosicionX_Pies300Der}
              y="162.762"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(314) !== undefined ?  getObjectSlip(314).Longitud !== 0 ? `${getObjectSlip(314).Longitud}'` : getObjectSlip(316).Longitud !== 0 ? `${getObjectSlip(316).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(314, 416, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS313-315"
            x="394.446"
            y="163.043"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-3"
              x="394.446"
              y="163.043"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 44&apos; */}
              {/* {`${getObjectSlip(313) !== undefined ?  getObjectSlip(313).Longitud !== 0 ? `${getObjectSlip(313).Longitud}'` : getObjectSlip(315).Longitud !== 0 ? `${getObjectSlip(315).Longitud}'` : "44'"  : "44'"}`} */}
              {`  ${getPieNumber(313, 315, 44)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS310-312"
            x="452.772"
            y="141.76"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-9"
              x={PosicionX_Pies300Der}
              y="141.76"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(310) !== undefined ?  getObjectSlip(310).Longitud !== 0 ? `${getObjectSlip(310).Longitud}'` : getObjectSlip(312).Longitud !== 0 ? `${getObjectSlip(312).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(310, 312, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS309-311"
            x="394.338"
            y="142.043"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-5"
              x="394.338"
              y="142.043"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 44&apos; */}
              {/* {`${getObjectSlip(309) !== undefined ?  getObjectSlip(309).Longitud !== 0 ? `${getObjectSlip(309).Longitud}'` : getObjectSlip(311).Longitud !== 0 ? `${getObjectSlip(311).Longitud}'` : "44'"  : "44'"}`} */}
              {`  ${getPieNumber(309, 311, 44)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS306-308"
            x="453.242"
            y="120.757"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4"
              x={PosicionX_Pies300Der}
              y="120.757"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(306) !== undefined ?  getObjectSlip(306).Longitud !== 0 ? `${getObjectSlip(306).Longitud}'` : getObjectSlip(308).Longitud !== 0 ? `${getObjectSlip(308).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(306, 308, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS305-307"
            x="394.231"
            y="121.043"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-4"
              x="394.231"
              y="121.043"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 44&apos; */}
              {/* {`${getObjectSlip(305) !== undefined ?  getObjectSlip(305).Longitud !== 0 ? `${getObjectSlip(305).Longitud}'` : getObjectSlip(307).Longitud !== 0 ? `${getObjectSlip(307).Longitud}'` : "44'"  : "44'"}`} */}
              {`  ${getPieNumber(305, 307, 44)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS302-304"
            x="453.281"
            y="99.756"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8"
              x={PosicionX_Pies300Der}
              y="99.756"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 36&apos; */}
              {/* {`${getObjectSlip(302) !== undefined ?  getObjectSlip(302).Longitud !== 0 ? `${getObjectSlip(302).Longitud}'` : getObjectSlip(304).Longitud !== 0 ? `${getObjectSlip(304).Longitud}'` : "36'"  : "36'"}`} */}
              {`  ${getPieNumber(302, 304, 36)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS301-303"
            x="394.123"
            y="100.043"
            transform="rotate(.293)"
          >
            <tspan
              id="tspan748-9-5-1-7-8-4-5-4-71"
              x="394.123"
              y="100.043"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 44&apos; */}
              {/* {`${getObjectSlip(301) !== undefined ?  getObjectSlip(301).Longitud !== 0 ? `${getObjectSlip(301).Longitud}'` : getObjectSlip(303).Longitud !== 0 ? `${getObjectSlip(303).Longitud}'` : "44'"  : "44'"}`} */}
              {`  ${getPieNumber(301, 303, 44)}`}
            </tspan>
          </text>
          <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="TLM3"
          x="418.301"
          y="89.417"
          className='text-color-size-pies'
          display="inline"
        >
          <tspan
            id="tspan3711-3-7"
            x="418.301"
            y="89.417"
            strokeWidth="0.265"
            fontSize="5.644"
          >
            M3: 87&apos;
          </tspan>
        </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS234-236"
            x="577.327"
            y="313.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-5-51"
              x="577.327"
              y="313.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 52&apos; */}
              {/* {`${getObjectSlip(234) !== undefined ?  getObjectSlip(234).Longitud !== 0 ? `${getObjectSlip(234).Longitud}'` : getObjectSlip(236).Longitud !== 0 ? `${getObjectSlip(236).Longitud}'` : "52'"  : "52'"}`} */}
              {`  ${getPieNumber(234, 236, 52)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS233-235"
            x="524.327"
            y="313.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-8"
              x="524.327"
              y="313.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(233) !== undefined ?  getObjectSlip(233).Longitud !== 0 ? `${getObjectSlip(233).Longitud}'` : getObjectSlip(235).Longitud !== 0 ? `${getObjectSlip(235).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(233, 235, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS230-232"
            x="577.327"
            y="292.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-5-7"
              x="577.327"
              y="292.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 52&apos; */}
              {/* {`${getObjectSlip(230) !== undefined ?  getObjectSlip(230).Longitud !== 0 ? `${getObjectSlip(230).Longitud}'` : getObjectSlip(232).Longitud !== 0 ? `${getObjectSlip(232).Longitud}'` : "52'"  : "52'"}`} */}
              {`  ${getPieNumber(230, 232, 52)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS229-231"
            x="524.327"
            y="292.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-6"
              x="524.327"
              y="292.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(229) !== undefined ?  getObjectSlip(229).Longitud !== 0 ? `${getObjectSlip(229).Longitud}'` : getObjectSlip(231).Longitud !== 0 ? `${getObjectSlip(231).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(229, 231, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS226-228"
            x="577.327"
            y="270.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-5-5"
              x="577.327"
              y="270.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 52&apos; */}
              {/* {`${getObjectSlip(226) !== undefined ?  getObjectSlip(226).Longitud !== 0 ? `${getObjectSlip(226).Longitud}'` : getObjectSlip(228).Longitud !== 0 ? `${getObjectSlip(228).Longitud}'` : "52'"  : "52'"}`} */}
              {`  ${getPieNumber(226, 228, 52)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS225-227"
            x="524.327"
            y="270.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-3"
              x="524.327"
              y="270.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(225) !== undefined ?  getObjectSlip(225).Longitud !== 0 ? `${getObjectSlip(225).Longitud}'` : getObjectSlip(227).Longitud !== 0 ? `${getObjectSlip(227).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(225, 227, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS222-224"
            x="577.327"
            y="249.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-5-0"
              x="577.327"
              y="249.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 52&apos; */}
              {/* {`${getObjectSlip(222) !== undefined ?  getObjectSlip(222).Longitud !== 0 ? `${getObjectSlip(222).Longitud}'` : getObjectSlip(224).Longitud !== 0 ? `${getObjectSlip(224).Longitud}'` : "52'"  : "52'"}`} */}
              {`  ${getPieNumber(222, 224, 52)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS221-223"
            x="524.327"
            y="249.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-0"
              x="524.327"
              y="249.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(221) !== undefined ?  getObjectSlip(221).Longitud !== 0 ? `${getObjectSlip(221).Longitud}'` : getObjectSlip(223).Longitud !== 0 ? `${getObjectSlip(223).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(221, 223, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS218-220"
            x="577.327"
            y="228.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-5-1"
              x="577.327"
              y="228.057"
              strokeWidth="0.265"
              className="text-color-size-pies"
            >
              {/* 52&apos; */}
              {/* {`${getObjectSlip(218) !== undefined ?  getObjectSlip(218).Longitud !== 0 ? `${getObjectSlip(218).Longitud}'` : getObjectSlip(220).Longitud !== 0 ? `${getObjectSlip(220).Longitud}'` : "52'"  : "52'"}`} */}
              {`  ${getPieNumber(218, 220, 52)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS217-219"
            x="524.327"
            y="228.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-9"
              x="524.327"
              y="228.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(217) !== undefined ?  getObjectSlip(217).Longitud !== 0 ? `${getObjectSlip(217).Longitud}'` : getObjectSlip(219).Longitud !== 0 ? `${getObjectSlip(219).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(217, 219, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS214-216"
            x="577.327"
            y="207.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52-5"
              x="577.327"
              y="207.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 52&apos; */}
              {/* {`${getObjectSlip(214) !== undefined ?  getObjectSlip(214).Longitud !== 0 ? `${getObjectSlip(214).Longitud}'` : getObjectSlip(216).Longitud !== 0 ? `${getObjectSlip(216).Longitud}'` : "52'"  : "52'"}`} */}
              {`  ${getPieNumber(214, 216, 52)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS213-215"
            x="524.327"
            y="207.057"
            display="inline"
          >
            <tspan
              id="tspan3711-52"
              x="524.327"
              y="207.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(213) !== undefined ?  getObjectSlip(213).Longitud !== 0 ? `${getObjectSlip(213).Longitud}'` : getObjectSlip(215).Longitud !== 0 ? `${getObjectSlip(215).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(213, 215, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS210-212"
            x="580.444"
            y="186.057"
            display="inline"
          >
            <tspan
              id="tspan3711-41"
              x="580.444"
              y="186.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 58&apos; */}
              {/* {`${getObjectSlip(210) !== undefined ?  getObjectSlip(210).Longitud !== 0 ? `${getObjectSlip(210).Longitud}'` : getObjectSlip(212).Longitud !== 0 ? `${getObjectSlip(212).Longitud}'` : "58'"  : "58'"}`} */}
              {`  ${getPieNumber(210, 212, 58)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS209-211"
            x="521.327"
            y="186.057"
            display="inline"
          >
            <tspan
              id="tspan3711-89"
              x="521.327"
              y="186.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 58&apos; */}
              {/* {`${getObjectSlip(209) !== undefined ?  getObjectSlip(209).Longitud !== 0 ? `${getObjectSlip(209).Longitud}'` : getObjectSlip(211).Longitud !== 0 ? `${getObjectSlip(211).Longitud}'` : "58'"  : "58'"}`} */}
              {`  ${getPieNumber(209, 211, 58)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS206-208"
            x="580.444"
            y="165.057"
            display="inline"
          >
            <tspan
              id="tspan3711-5"
              x="580.444"
              y="165.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 58&apos; */}
              {/* {`${getObjectSlip(206) !== undefined ?  getObjectSlip(206).Longitud !== 0 ? `${getObjectSlip(206).Longitud}'` : getObjectSlip(208).Longitud !== 0 ? `${getObjectSlip(208).Longitud}'` : "58'"  : "58'"}`} */}
              {`  ${getPieNumber(206, 208, 58)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS205-207"
            x="521.327"
            y="165.057"
            display="inline"
          >
            <tspan
              id="tspan3711-4"
              x="521.327"
              y="165.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 58&apos; */}
              {/* {`${getObjectSlip(205) !== undefined ?  getObjectSlip(205).Longitud !== 0 ? `${getObjectSlip(205).Longitud}'` : getObjectSlip(207).Longitud !== 0 ? `${getObjectSlip(207).Longitud}'` : "58'"  : "58'"}`} */}
              {`  ${getPieNumber(205, 207, 58)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS202-204"
            x="580.444"
            y="144.057"
            display="inline"
          >
            <tspan
              id="tspan3711-8"
              x="580.444"
              y="144.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 58&apos; */}
              {/* {`${getObjectSlip(202) !== undefined ?  getObjectSlip(202).Longitud !== 0 ? `${getObjectSlip(202).Longitud}'` : getObjectSlip(204).Longitud !== 0 ? `${getObjectSlip(204).Longitud}'` : "58'"  : "58'"}`} */}
              {`  ${getPieNumber(202, 204, 58)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS201-203"
            x="521.327"
            y="144.057"
          >
            <tspan
              id="tspan3711"
              x="521.327"
              y="144.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 58&apos; */}
              {/* {`${getObjectSlip(201) !== undefined ?  getObjectSlip(201).Longitud !== 0 ? `${getObjectSlip(201).Longitud}'` : getObjectSlip(203).Longitud !== 0 ? `${getObjectSlip(203).Longitud}'` : "58'"  : "58'"}`} */}
              {`  ${getPieNumber(201, 203, 58)}`}
            </tspan>
          </text>
          <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="TLM2"
          x="549.846"
          y="131.877"
          // fill="navy"
          className='text-color-size-pies'
          display="inline"
        >
          <tspan
            id="tspan3711-3"
            x="549.846"
            y="131.877"
            strokeWidth="0.265"
            fontSize="5.644"
          >
            M2: 127&apos;
          </tspan>
        </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS109-110"
            x="648.509"
            y="313.057"
            display="inline"
          >
            <tspan
              id="tspan13477-2-8-8"
              x="648.509"
              y="313.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 50&apos; */}
              {/* {`${getObjectSlip(109) !== undefined ?  getObjectSlip(109).Longitud !== 0 ? `${getObjectSlip(109).Longitud}'` : getObjectSlip(110).Longitud !== 0 ? `${getObjectSlip(110).Longitud}'` : "50'"  : "50'"}`} */}
              {`  ${getPieNumber(109, 110, 50)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS107-108"
            x="645.599"
            y="292.057"
            display="inline"
          >
            <tspan
              id="tspan13477-2-8-7"
              x="645.599"
              y="292.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 58&apos; */}
              {/* {`${getObjectSlip(107) !== undefined ?  getObjectSlip(107).Longitud !== 0 ? `${getObjectSlip(107).Longitud}'` : getObjectSlip(108).Longitud !== 0 ? `${getObjectSlip(108).Longitud}'` : "58'"  : "58'"}`} */}
              {`  ${getPieNumber(107, 108, 58)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS105-106"
            x="647.186"
            y="270.057"
            display="inline"
          >
            <tspan
              id="tspan13477-2-8"
              x="647.186"
              y="270.057"
              strokeWidth="0.265"
              className='text-color-size-pies'
            >
              {/* 58&apos; */}
              {/* {`${getObjectSlip(105) !== undefined ?  getObjectSlip(105).Longitud !== 0 ? `${getObjectSlip(105).Longitud}'` : getObjectSlip(106).Longitud !== 0 ? `${getObjectSlip(106).Longitud}'` : "58'"  : "58'"}`} */}
              {`  ${getPieNumber(105, 106, 58)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS103-104"
            x="647.434"
            y="249.057"
            display="inline"
          >
            <tspan id="tspan13477-2" x="647.434" y="249.057" strokeWidth="0.265" className='text-color-size-pies'>
              {/* 58&apos; */}
              {/* {`${getObjectSlip(103) !== undefined ?  getObjectSlip(103).Longitud !== 0 ? `${getObjectSlip(103).Longitud}'` : getObjectSlip(104).Longitud !== 0 ? `${getObjectSlip(104).Longitud}'` : "58'"  : "58'"}`} */}
              {`  ${getPieNumber(103, 104, 58)}`}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TLS101-102"
            x="643.467"
            y="228.04"
          >
            <tspan id="tspan13477" x="643.467" y="228.04" strokeWidth="0.265" className='text-color-size-pies'>
              {/* 75&apos; */}
              {/* {`${getObjectSlip(803) !== undefined ? `${getObjectSlip(803).Nombre_barco.substring(0,12)} ${getObjectSlip(803).Embarcacion !== 0 ? getObjectSlip(803).Embarcacion : ""}` : ""}`} */}
              {/* {`${getObjectSlip(101) !== undefined ?  getObjectSlip(101).Longitud !== 0 ? `${getObjectSlip(101).Longitud}'` : getObjectSlip(102).Longitud !== 0 ? `${getObjectSlip(102).Longitud}'` : "75'"  : "75'"}`} */}
              {`  ${getPieNumber(101, 102, 75)}`}
            </tspan>
          </text>
        </g>
        <g
          id="layer10"
          fill="navy"
          strokeWidth="0.265"
          display="inline"
          fontFamily="Arial"
          textAnchor="middle"
          transform="translate(-5.848 -6.058)"
        >
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-7"
            x="614.52"
            y="51.259"
            display="inline"
          >
            <tspan
              id="tspan6021-5"
              x="614.52"
              y="51.259"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              814
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023"
            x="600.869"
            y="51.128"
          >
            <tspan
              id="tspan6021"
              x="600.869"
              y="51.128"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              813
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-6"
            x="600.624"
            y="59.125"
            display="inline"
          >
            <tspan
              id="tspan6021-1"
              x="600.624"
              y="59.125"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              812
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-8"
            x="615.132"
            y="59.3"
            display="inline"
          >
            <tspan
              id="tspan6021-9"
              x="615.132"
              y="59.3"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              811
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-63"
            x="615.744"
            y="71.973"
            display="inline"
          >
            <tspan
              id="tspan6021-7"
              x="615.744"
              y="71.973"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              810
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-65"
            x="615.219"
            y="80.276"
            display="inline"
          >
            <tspan
              id="tspan6021-4"
              x="615.219"
              y="80.276"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              809
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-9"
            x="600.099"
            y="72.06"
            display="inline"
          >
            <tspan
              id="tspan6021-51"
              x="600.099"
              y="72.06"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              808
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-5"
            x="600.012"
            y="80.276"
            display="inline"
          >
            <tspan
              id="tspan6021-8"
              x="600.012"
              y="80.276"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              807
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-96"
            x="614.87"
            y="93.298"
            display="inline"
          >
            <tspan
              id="tspan6021-761"
              x="614.87"
              y="93.298"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              806
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-00"
            x="614.695"
            y="102.301"
            display="inline"
          >
            <tspan
              id="tspan6021-76"
              x="614.695"
              y="102.301"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              805
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-0"
            x="600.536"
            y="102.738"
            display="inline"
          >
            <tspan
              id="tspan6021-6"
              x="600.536"
              y="102.738"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              804
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-99-6"
            x="600.038"
            y="110.693"
            display="inline"
          >
            <tspan
              id="tspan6021-46-3"
              x="600.038"
              y="110.693"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              803
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-99"
            x="614.345"
            y="114.274"
            display="inline"
          >
            <tspan
              id="tspan6021-46"
              x="614.345"
              y="114.274"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              802
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text6023-71"
            x="614.433"
            y="122.315"
            display="inline"
          >
            <tspan
              id="tspan6021-58"
              x="614.433"
              y="122.315"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              801
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-8-9"
            x="460.435"
            y="19.071"
            display="inline"
          >
            <tspan
              id="tspan1654-86-7"
              x="460.435"
              y="19.071"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              706
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-8-0"
            x="410.392"
            y="19.071"
            display="inline"
          >
            <tspan
              id="tspan1654-86-46"
              x="410.392"
              y="19.071"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              705
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-8-4"
            x="358.549"
            y="19.071"
            display="inline"
          >
            <tspan
              id="tspan1654-86-2"
              x="358.549"
              y="19.071"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              704
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-8-6"
            x="303.5"
            y="19.071"
            display="inline"
          >
            <tspan
              id="tspan1654-86-6"
              x="303.5"
              y="19.071"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              703
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-8-2"
            x="250.85"
            y="19.071"
            display="inline"
          >
            <tspan
              id="tspan1654-86-1"
              x="250.85"
              y="19.071"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              702
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-8-3"
            x="196.708"
            y="19.071"
            display="inline"
          >
            <tspan
              id="tspan1654-86-4"
              x="196.708"
              y="19.071"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              701
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-8"
            x="140.952"
            y="19.071"
            display="inline"
          >
            <tspan
              id="tspan1654-86"
              x="140.952"
              y="19.071"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              700
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-8-8-2"
            x="29.86"
            y="317.383"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.314)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-27-3-7"
              x="29.86"
              y="317.383"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              614
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-8-8"
            x="29.817"
            y="309.443"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.314)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-27-3"
              x="29.817"
              y="309.443"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              613
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-8"
            x="29.745"
            y="296.303"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.314)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-27"
              x="29.745"
              y="296.303"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              612
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-6"
            x="27.275"
            y="275.455"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.18)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-6"
              x="27.275"
              y="275.455"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              610
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-6-2"
            x="26.976"
            y="287.428"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.18)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-6-0"
              x="26.976"
              y="287.428"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              611
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-1"
            x="24.431"
            y="268.018"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.804)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-1"
              x="24.431"
              y="268.018"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              609
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-0"
            x="28.716"
            y="254.218"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.134)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-8"
              x="28.716"
              y="254.218"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              608
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-7"
            x="31.794"
            y="246.208"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.854)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-01"
              x="31.794"
              y="246.208"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              607
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-3"
            x="25.648"
            y="233.543"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.62)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-3"
              x="25.648"
              y="233.543"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              606
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0-9"
            x="25.3"
            y="224.076"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.738)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12-0"
              x="25.3"
              y="224.076"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              605
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-0"
            x="29.787"
            y="213.029"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.448)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-12"
              x="29.787"
              y="213.029"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              604
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9-2"
            x="27.746"
            y="204.794"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.108)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5-1"
              x="27.746"
              y="204.794"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              603
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2-9"
            x="25.303"
            y="191.519"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.867)"
          >
            <tspan
              id="tspan11750-8-5-29-2-5"
              x="25.303"
              y="191.519"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              602
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72-2"
            x="24.177"
            y="184.328"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-1.262)"
          >
            <tspan
              id="tspan11750-8-5-29-2"
              x="24.177"
              y="184.328"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              601
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19-4-4"
            x="161.876"
            y="316.366"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64-4-8"
              x="161.876"
              y="316.366"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              532
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19-3-1"
            x="146.446"
            y="317.123"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.154)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64-1-0"
              x="146.446"
              y="317.123"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              531
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19-6"
            x="153.802"
            y="312.639"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-1.084)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64-0"
              x="153.802"
              y="312.639"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              530
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-56-1-2"
            x="147.819"
            y="308.527"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-69-3-7"
              x="147.819"
              y="308.527"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              529
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-95-5"
            x="161.724"
            y="295.287"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-1-0"
              x="161.724"
              y="295.287"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              528
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-56-1"
            x="147.724"
            y="295.388"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-69-3"
              x="147.724"
              y="295.388"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              527
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-9-7"
            x="161.667"
            y="287.427"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-7-4"
              x="161.667"
              y="287.427"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              526
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-8-2"
            x="147.668"
            y="287.528"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-6-9"
              x="147.668"
              y="287.528"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              525
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-08-9"
            x="161.572"
            y="274.207"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-9-9"
              x="161.572"
              y="274.207"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              524
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-1-6"
            x="147.572"
            y="274.308"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-08-3"
              x="147.572"
              y="274.308"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              523
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-5-5"
            x="161.516"
            y="266.428"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-0-2"
              x="161.516"
              y="266.428"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              522
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-0-9"
            x="147.516"
            y="266.528"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-8-7"
              x="147.516"
              y="266.528"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              521
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-17"
            x="161.42"
            y="253.128"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-07"
              x="161.42"
              y="253.128"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              520
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-0-3"
            x="147.421"
            y="253.225"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-8-0"
              x="147.421"
              y="253.225"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              519
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-9"
            x="161.365"
            y="245.424"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-5"
              x="161.365"
              y="245.424"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              518
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-8-1"
            x="147.365"
            y="245.525"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-39-4"
              x="147.365"
              y="245.525"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              517
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-1-5"
            x="161.269"
            y="232.045"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-0-4"
              x="161.269"
              y="232.045"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              516
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-4-2"
            x="147.269"
            y="232.145"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-8-4"
              x="147.269"
              y="232.145"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              515
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-9-6"
            x="155.79"
            y="228.583"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.98)"
          >
            <tspan
              id="tspan11750-8-5-2-3-3-8"
              x="155.79"
              y="228.583"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              514
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-2-1"
            x="142.052"
            y="228.193"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.913)"
          >
            <tspan
              id="tspan11750-8-5-2-3-1-1"
              x="142.052"
              y="228.193"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              513
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-5-8"
            x="161.117"
            y="210.965"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-9-4"
              x="161.117"
              y="210.965"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              512
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-6-3"
            x="146.855"
            y="211.066"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-6-1"
              x="146.855"
              y="211.066"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              511
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-98"
            x="161.064"
            y="203.585"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-3-15"
              x="161.064"
              y="203.585"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              510
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-5-7"
            x="147.064"
            y="203.689"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-0-8"
              x="147.064"
              y="203.689"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              509
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-07-0"
            x="160.965"
            y="189.889"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-8-0"
              x="160.965"
              y="189.889"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              508
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-2-2"
            x="146.965"
            y="189.99"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-2-2"
              x="146.965"
              y="189.99"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              507
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-0-2"
            x="160.913"
            y="182.729"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-9-6"
              x="160.913"
              y="182.729"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              506
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-3-8"
            x="146.912"
            y="182.61"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-1-9"
              x="146.912"
              y="182.61"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              505
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-02"
            x="160.813"
            y="168.81"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-2-21"
              x="160.813"
              y="168.81"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              504
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-7-5"
            x="146.814"
            y="168.91"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-6-5"
              x="146.814"
              y="168.91"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              503
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-6-3"
            x="160.76"
            y="161.43"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-5-2"
              x="160.76"
              y="161.43"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              502
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-72"
            x="146.76"
            y="161.531"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.413)"
          >
            <tspan
              id="tspan11750-8-5-29"
              x="146.76"
              y="161.531"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              501
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19-4-1-4"
            x="295"
            y="314.989"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64-4-0-6"
              x="295"
              y="314.989"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              436
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19-4-1-2"
            x="280.6"
            y="315.115"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64-4-0-1"
              x="280.6"
              y="315.115"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              435
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19-4-1"
            x="294.931"
            y="307.05"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64-4-0"
              x="294.931"
              y="307.05"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              434
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19-4-5"
            x="280.531"
            y="307.175"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64-4-2"
              x="280.531"
              y="307.175"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              433
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19-4"
            x="294.817"
            y="293.91"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64-4"
              x="294.817"
              y="293.91"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              432
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19-3"
            x="280.417"
            y="294.035"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64-1"
              x="280.417"
              y="294.035"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              431
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-19"
            x="294.748"
            y="286.05"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-64"
              x="294.748"
              y="286.05"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              430
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-6"
            x="280.349"
            y="286.176"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-5"
              x="280.349"
              y="286.176"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              429
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-95"
            x="294.634"
            y="272.831"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-1"
              x="294.634"
              y="272.831"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              428
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-56"
            x="280.234"
            y="272.956"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-69"
              x="280.234"
              y="272.956"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              427
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-9"
            x="294.566"
            y="265.051"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-7"
              x="294.566"
              y="265.051"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              426
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-8"
            x="280.166"
            y="265.176"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-6"
              x="280.166"
              y="265.176"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              425
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-08"
            x="294.45"
            y="251.752"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-9"
              x="294.45"
              y="251.752"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              424
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-1"
            x="280.051"
            y="251.877"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-08"
              x="280.051"
              y="251.877"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              423
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-5"
            x="294.384"
            y="244.052"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-0"
              x="294.384"
              y="244.052"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              422
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7-0"
            x="279.984"
            y="244.177"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2-8"
              x="279.984"
              y="244.177"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              421
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-7"
            x="294.267"
            y="230.673"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-2"
              x="294.267"
              y="230.673"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              420
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3-0"
            x="279.868"
            y="230.798"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390-8"
              x="279.868"
              y="230.798"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              419
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-3"
            x="294.203"
            y="223.293"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-390"
              x="294.203"
              y="223.293"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              418
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-8"
            x="279.804"
            y="223.418"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-39"
              x="279.804"
              y="223.418"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              417
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-1"
            x="294.084"
            y="209.593"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-0"
              x="294.084"
              y="209.593"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              416
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-4"
            x="279.685"
            y="209.718"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-8"
              x="279.685"
              y="209.718"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              415
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-9"
            x="294.02"
            y="202.214"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-3"
              x="294.02"
              y="202.214"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              414
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-2"
            x="279.62"
            y="202.339"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-1"
              x="279.62"
              y="202.339"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              413
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-5"
            x="293.901"
            y="188.514"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-9"
              x="293.901"
              y="188.514"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              412
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57-6"
            x="279.24"
            y="188.639"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3-6"
              x="279.24"
              y="188.639"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              411
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-57"
            x="293.836"
            y="181.054"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-3"
              x="293.836"
              y="181.054"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              410
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-5"
            x="279.437"
            y="181.26"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-0"
              x="279.437"
              y="181.26"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              409
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-07"
            x="293.718"
            y="167.435"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-8"
              x="293.718"
              y="167.435"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              408
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-2"
            x="279.318"
            y="167.56"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-2"
              x="279.318"
              y="167.56"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              407
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-0"
            x="293.654"
            y="160.055"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-9"
              x="293.654"
              y="160.055"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              406
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8-3"
            x="279.254"
            y="160.18"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2-1"
              x="279.254"
              y="160.18"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              405
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-8"
            x="293.535"
            y="146.356"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-2"
              x="293.535"
              y="146.356"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              404
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-7"
            x="279.135"
            y="146.481"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-6"
              x="279.135"
              y="146.481"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              403
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2-6"
            x="293.47"
            y="138.976"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5-5"
              x="293.47"
              y="138.976"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              402
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-2"
            x="279.071"
            y="139.101"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.498)"
          >
            <tspan
              id="tspan11750-8-5"
              x="279.071"
              y="139.101"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              401
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8-4-7-0-5-9-5-5-9-0"
            x="429.295"
            y="314.265"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2-7-0-1-9-2-0-9-9-1"
              x="429.295"
              y="314.265"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              344
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8-4-7-0-5-9-5-5-9"
            x="414.296"
            y="314.397"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2-7-0-1-9-2-0-9-9"
              x="414.296"
              y="314.397"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              343
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8-4-7-0-5-9-5-5"
            x="429.23"
            y="306.885"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2-7-0-1-9-2-0-9"
              x="429.23"
              y="306.885"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              342
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8-4-7-0-5-9-5"
            x="414.231"
            y="307.017"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2-7-0-1-9-2-0"
              x="414.231"
              y="307.017"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              341
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8-4-7-0-5-9"
            x="429.109"
            y="293.185"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2-7-0-1-9-2"
              x="429.109"
              y="293.185"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              340
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8-4-7-0-5"
            x="414.11"
            y="293.318"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2-7-0-1-9"
              x="414.11"
              y="293.318"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              339
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8-4-7-0"
            x="429.044"
            y="285.806"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2-7-0-1"
              x="429.044"
              y="285.806"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              338
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8-4-7"
            x="414.044"
            y="285.938"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2-7-0"
              x="414.044"
              y="285.938"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              337
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8-4"
            x="428.923"
            y="272.106"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2-7"
              x="428.923"
              y="272.106"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              336
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4-8"
            x="413.923"
            y="272.239"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7-2"
              x="413.923"
              y="272.239"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              335
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9-4"
            x="428.858"
            y="264.726"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0-7"
              x="428.858"
              y="264.726"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              334
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7-9"
            x="413.858"
            y="264.859"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2-0"
              x="413.858"
              y="264.859"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              333
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0-7"
            x="428.737"
            y="251.027"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1-2"
              x="428.737"
              y="251.027"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              332
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5-0"
            x="413.737"
            y="251.16"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1-1"
              x="413.737"
              y="251.16"
              strokeWidth="0.265"
              className='text-color-size-slips'            >
              331
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3-5"
            x="428.671"
            y="243.647"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8-1"
              x="428.671"
              y="243.647"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              330
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1-3"
            x="413.672"
            y="243.78"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2-8"
              x="413.672"
              y="243.78"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              329
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7-1"
            x="428.55"
            y="229.948"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6-2"
              x="428.55"
              y="229.948"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              328
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9-7"
            x="413.551"
            y="230.08"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8-6"
              x="413.551"
              y="230.08"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              327
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0-9"
            x="428.485"
            y="222.568"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7-8"
              x="428.485"
              y="222.568"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              326
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7-0"
            x="413.486"
            y="222.701"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0-7"
              x="413.486"
              y="222.701"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              325
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-0"
            x="428.364"
            y="208.869"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-3"
              x="428.364"
              y="208.869"
              strokeWidth="0.265"
              className='text-color-size-slips'            >
              324
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8-7"
            x="413.365"
            y="209.001"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6-0"
              x="413.365"
              y="209.001"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              323
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-8"
            x="428.299"
            y="201.489"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-6"
              x="428.299"
              y="201.489"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              322
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8-9"
            x="413.299"
            y="201.621"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48-1"
              x="413.299"
              y="201.621"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              321
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-8"
            x="428.178"
            y="187.789"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-48"
              x="428.178"
              y="187.789"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              320
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-7"
            x="413.178"
            y="187.922"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-57"
              x="413.178"
              y="187.922"
              strokeWidth="0.265"
              className='text-color-size-slips'            >
              319
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-97"
            x="428.113"
            y="180.41"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-1"
              x="428.113"
              y="180.41"
              strokeWidth="0.265"
              className='text-color-size-slips'            >
              318
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-5"
            x="413.113"
            y="180.542"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-45"
              x="413.113"
              y="180.542"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              317
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-9"
            x="427.991"
            y="166.71"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-5"
              x="427.991"
              y="166.71"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              316
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-3"
            x="412.992"
            y="166.843"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-7"
              x="412.992"
              y="166.843"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              315
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74-0"
            x="427.926"
            y="159.331"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70-4"
              x="427.926"
              y="159.331"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              314
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-74"
            x="412.927"
            y="159.463"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-70"
              x="412.927"
              y="159.463"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              313
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-47"
            x="427.805"
            y="145.631"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-95"
              x="427.805"
              y="145.631"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              312
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-4"
            x="412.544"
            y="145.764"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-7"
              x="412.544"
              y="145.764"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              311
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-21"
            x="427.74"
            y="138.251"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-1"
              x="427.74"
              y="138.251"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              310
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-6"
            x="412.741"
            y="138.384"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-93"
              x="412.741"
              y="138.384"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              309
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-2"
            x="427.619"
            y="124.552"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-4"
              x="427.619"
              y="124.552"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              308
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-3"
            x="412.62"
            y="124.684"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-5"
              x="412.62"
              y="124.684"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              307
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-7"
            x="427.554"
            y="117.172"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-2"
              x="427.554"
              y="117.172"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              306
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-0"
            x="412.554"
            y="117.305"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-9"
              x="412.554"
              y="117.305"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              305
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1-9"
            x="427.433"
            y="103.473"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8-0"
              x="427.433"
              y="103.473"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              304
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7-1"
            x="412.433"
            y="103.605"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1-8"
              x="412.433"
              y="103.605"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              303
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1-7"
            x="427.367"
            y="96.093"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8-1"
              x="427.367"
              y="96.093"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              302
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11752-1"
            x="412.368"
            y="96.226"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(.506)"
          >
            <tspan
              id="tspan11750-8"
              x="412.368"
              y="96.226"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              301
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-0-9"
            x="557.567"
            y="317.505"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-7-6"
              x="557.567"
              y="317.505"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              236
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-0-1"
            x="544.067"
            y="317.505"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-7-1"
              x="544.067"
              y="317.505"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              235
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-0-8"
            x="557.567"
            y="309.865"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-7-9"
              x="557.567"
              y="309.865"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              234
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-0-6"
            x="544.067"
            y="309.865"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-7-8"
              x="544.067"
              y="309.865"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              233
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-0"
            x="557.567"
            y="296.425"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-7"
              x="557.567"
              y="296.425"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              232
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-63"
            x="544.067"
            y="296.425"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-8"
              x="544.067"
              y="296.425"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              231
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-9"
            x="557.567"
            y="289.045"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-44"
              x="557.567"
              y="289.045"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              230
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-53"
            x="544.067"
            y="289.045"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-4"
              x="544.067"
              y="289.045"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              229
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-6"
            x="557.567"
            y="275.346"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-38"
              x="557.567"
              y="275.346"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              228
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-8"
            x="544.067"
            y="275.346"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-3"
              x="544.067"
              y="275.346"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              227
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-1"
            x="557.567"
            y="267.966"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-04"
              x="557.567"
              y="267.966"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              226
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0-5"
            x="544.067"
            y="267.966"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3-0"
              x="544.067"
              y="267.966"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              225
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-66"
            x="557.567"
            y="254.266"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-34"
              x="557.567"
              y="254.266"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              224
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-0"
            x="544.067"
            y="254.266"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-3"
              x="544.067"
              y="254.266"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              223
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-3"
            x="557.567"
            y="246.886"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-9"
              x="557.567"
              y="246.886"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              222
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3-6"
            x="544.067"
            y="246.886"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41-8"
              x="544.067"
              y="246.886"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              221
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-3"
            x="557.567"
            y="233.186"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-41"
              x="557.567"
              y="233.186"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              220
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-15"
            x="544.067"
            y="233.186"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-8"
              x="544.067"
              y="233.186"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              219
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-0"
            x="557.567"
            y="225.807"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-6"
              x="557.567"
              y="225.807"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              218
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-74"
            x="544.067"
            y="225.807"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-49"
              x="544.067"
              y="225.807"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              217
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-9"
            x="557.567"
            y="212.107"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-3"
              x="557.567"
              y="212.107"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              216
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-14"
            x="544.067"
            y="212.107"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-4"
              x="544.067"
              y="212.107"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              215
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-1"
            x="557.567"
            y="204.727"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-9"
              x="557.567"
              y="204.727"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              214
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78-7"
            x="544.067"
            y="204.727"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13-7"
              x="544.067"
              y="204.727"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              213
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-78"
            x="557.567"
            y="191.027"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-13"
              x="557.567"
              y="191.027"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              212
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-4"
            x="543.805"
            y="191.027"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-1"
              x="543.805"
              y="191.027"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              211
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-1"
            x="557.567"
            y="183.647"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-20"
              x="557.567"
              y="183.647"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              210
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-7"
            x="544.067"
            y="183.647"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-0"
              x="544.067"
              y="183.647"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              209
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-92"
            x="557.567"
            y="169.948"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-94"
              x="557.567"
              y="169.948"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              208
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-8"
            x="544.067"
            y="169.948"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-2"
              x="544.067"
              y="169.948"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              207
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-2"
            x="557.567"
            y="162.568"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-3"
              x="557.567"
              y="162.568"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              206
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-5"
            x="544.067"
            y="162.568"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-7"
              x="544.067"
              y="162.568"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              205
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4-9"
            x="557.567"
            y="148.868"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9-9"
              x="557.567"
              y="148.868"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              204
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-4"
            x="544.067"
            y="148.868"
            display="inline"
            transform="scale(.99998 1.00002)"
          >
            <tspan
              id="tspan1654-9"
              x="544.067"
              y="148.868"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              203
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656-9"
            x="557.556"
            y="141.491"
            display="inline"
          >
            <tspan
              id="tspan1654-8"
              x="557.556"
              y="141.491"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              202
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text1656"
            x="544.056"
            y="141.491"
          >
            <tspan
              id="tspan1654"
              x="544.056"
              y="141.491"
              fill="navy"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              201
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462-5-2-6"
            x="667.308"
            y="318.688"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460-6-8-28"
              x="667.308"
              y="318.688"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              110
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462-5-2-5"
            x="667.583"
            y="311.049"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460-6-8-8"
              x="667.583"
              y="311.049"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              109
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462-5-2-78"
            x="667.607"
            y="297.609"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460-6-8-7"
              x="667.607"
              y="297.609"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              108
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462-5-2-16"
            x="667.62"
            y="290.222"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460-6-8-3"
              x="667.62"
              y="290.222"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              107
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462-5-2-1"
            x="667.644"
            y="276.272"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460-6-8-9"
              x="667.644"
              y="276.272"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              106
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462-5-2-9"
            x="667.657"
            y="269.143"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460-6-8-1"
              x="667.657"
              y="269.143"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              105
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462-5-2-7"
            x="667.681"
            y="255.443"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460-6-8-2"
              x="667.681"
              y="255.443"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              104
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462-5-2"
            x="667.694"
            y="248.069"
            fillOpacity="1"
            direction="ltr"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460-6-8"
              x="667.694"
              y="248.069"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              103
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462-5"
            x="667.718"
            y="234.369"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460-6"
              x="667.718"
              y="234.369"
              strokeWidth="0.265"
              className='text-color-size-slips'
            >
              102
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text11462"
            x="667.73"
            y="226.988"
            fillOpacity="1"
            direction="ltr"
            display="inline"
            fontStretch="normal"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight="normal"
            opacity="1"
            transform="rotate(-.1)"
          >
            <tspan
              id="tspan11460"
              x="667.73"
              y="226.988"
              strokeWidth="0.265"
              className='text-color-size-slips'
              >
              101
            </tspan>
          </text>
        </g>
        <g
          id="layer11"
          strokeWidth="0.265"
          display="inline"
          transform="translate(-5.848 -6.058)"
        >
          <path
            id="path2089"
            // fill="navy"
            className="style-muelles"
            d="M120 21h52v9h1v-9h52v9h1v-9h52v9h1v-9h52v9h1v-9h52v9h1v-9h52v9h1v-9h66v-1h-45.292.031-52.558.179H353.515h.022H120z"
            display="inline"
            opacity="1"
          ></path>
          <path
            id="path953"
            // fill="navy"
            className="style-muelles"
            d="M53 311.177v-1H21v-20h32v-1H21v-20h32v-1H21v-20h32v-1H21v-20h37v-1H21v-10h37v-1H21.1l-.1-9h37.796l-.094-1H21v-20h34v-1H20v127z"
            display="inline"
            opacity="1"
          ></path>
          <path
            id="path955"
            // fill="navy"
            className="style-muelles"
            d="M152.21 331.177h1v-20h27v-1h-27v-20h27v-1h-27v-20h27v-1h-27v-20h27v-1h-27v-20h27v-1h-27v-20h27v-1h-27v-20h27v-1h-27v-20h27v-1h-55.359v1h27.36v20h-22.36v1h22.36v20h-22.36v1h22.36v20h-22.36v1h22.36v20h-22.36v1h22.36v20h-22.36v1h22.36v20h-22.36v1h22.36v20h-22.36v1h22.36z"
            display="inline"
            opacity="1"
          ></path>
          <path
            id="path1007"
            // fill="navy"
            className="style-muelles"
            d="M284.788 331.177h1v-20H312v-1h-26.212v-20H312v-1h-26.212v-20H312v-1h-26.212v-20H312v-1h-26.212v-20H312v-1h-26.212v-20H312v-1h-26.212v-20H312v-1h-26.212v-20.011l26.212.011v-1h-26.212v-20H312v-1h-51v1h23.788s-.939.006 0 0c.07 0 0 20 0 20H261v1h23.788v20H261v1h23.788v20H261v1h23.788v19.94l-23.788.06v1h23.788v20H261v1h23.788v20H261v1h23.788v20H261v1h23.788v20H261v1h23.788z"
            display="inline"
            opacity="1"
          ></path>
          <path
            id="path1009"
            // fill="navy"
            className="style-muelles"
            d="M418.511 331.662h1v-20H438v-1h-18.489v-20H438v-1h-18.489v-20H438v-1h-18.489v-20H438v-1h-18.489v-20H438v-1h-18.489v-20H438v-1h-18.489v-20H438v-1h-18.489v-20H438v-1h-18.489v-20H438v-1h-18.489v-20H438v-1h-18.489v-20H438v-1h-40v1h20.511v20H398v1h20.511v20H398v1h20.511v20H398v1h20.511v20H398v1h20.511v20H398v1h20.511v20H398v1h20.511v20H398v1h20.511v20H400v1h18.511v20H400v1h18.511v20H400v1h18.511z"
            display="inline"
            opacity="1"
          ></path>
          <path
            id="path2404"
            // fill="navy"
            className="style-muelles"
            d="M549.883 331.177h1v-20H573v-1h-22.117v-20H573v-1h-22.117v-20H573v-1h-22.117v-20H573v-1h-22.117v-20H573v-1h-22.117v-20H573v-1h-22.117v-20H576v-1h-25.117v-20H576v-1h-25.117v-20H576v-1h-51v1h24.883v20H525v1h24.883s.13 20.017 0 20c-.13-.016-24.883 0-24.883 0v1h24.883v20H529v1h20.883v20H529v1h20.883v20H529v1h20.883v20H529v1h20.883v20H529v1h20.883v20H529v1h20.883z"
            display="inline"
            opacity="1"
          ></path>
          <path
            id="path2407"
            // fill="navy"
            className="style-muelles"
            d="M676 247.838v-67.44l-1-.126v45.905h-27v1h27v20h-22v1h22v20h-22v1h22v20h-22v1h22v20h-22v1h23V250.75z"
            display="inline"
            opacity="1"
          ></path>
          <path
            id="path2411"
            // fill="navy"
            className="style-muelles"
            d="M607 124h1v-8s22.019.19 22 0c-.019-.19 0-1 0-1h-22V95h22v-1h-22V74h22v-1h-22V53h23.5v-1H559h6c.23 0-.017.395 0 1 .002.088.982.007 2.607 0 2.388-.01 6.17.02 10.29 0 9.55-.046 20.912 0 20.912 0H607v20h-22v1h22v30h-22v1h22z"
            display="inline"
            opacity="1"
          ></path>
          <path
            id="path2414"
            // fill="#003344"
            className='style-marco-contorno'
            d="M650.54 11.058l.126-5H5.848V336.177h641.019v-5h-66.775v-61.055 61.055H10.848V11.058H221.65z"
            display="inline"
            opacity="1"
          ></path>
        </g>
        <g
          id="layer2"
          fill="navy"
          strokeWidth="0.265"
          display="inline"
          fontFamily="Arial"
          fontSize="4.939"
          textAnchor="middle"
          transform="translate(-5.848 -6.058)"
        >
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb814"
            x={PosicionX_Slips800Der}
            y="51.794"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="53.794"
            // x="620.799"
            // x={getBulletPosition(814, PosicionX_Slips800Der, rightPosition)}
            className={getColorEnBaseFactura(814,rightPosition)}
            >•</tspan>
            <tspan
              id="TE814"
              x={PosicionX_Slips800Der}
              y="51.794"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(814) !== undefined ? getObjectSlip(814).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(814, 'right')}
            >
              {/* EMBARCACION 814 */}
              {/* {`${getObjectSlip(814) !== undefined ? `${getObjectSlip(814).Nombre_barco.substring(0,12)} ${getObjectSlip(814).Embarcacion !== 0 ? getObjectSlip(814).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(814, rightPosition)}
              
            </tspan>
            <tspan className={getColorLongitudCSS(814)}>{getTamañoEmbarcacion(814)}</tspan>
            
          </text>
          
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb813"
            x={PosicionX_Slips800Izq}
            y="51.331"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="53.331" //51.331
            strokeWidth="0.265"
            className={getColorEnBaseFactura(813,leftPosition)}
            >•</tspan>
            <tspan
              id="TE813"
              x={PosicionX_Slips800Izq}
              y="51.331"
              strokeWidth="0.265"
              fontSize="4.939"
              className={getNombreClaseCSS(813, 'left')}
              >
              {/* EMBARCACION 813 */}
              {/* {`${getObjectSlip(813) !== undefined ? `${getObjectSlip(813).Nombre_barco.substring(0,12)} ${getObjectSlip(813).Embarcacion !== 0 ? getObjectSlip(813).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(813, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(813)}>{getTamañoEmbarcacion(813, leftPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb812"
            x={PosicionX_Slips800Izq}
            y="66.212"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="68.212"
            // x={getBulletPosition(812, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(812,leftPosition)}
            >•</tspan>
            <tspan
              id="TE812"
              x={PosicionX_Slips800Izq}
              y="66.212"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(812) !== undefined ? getObjectSlip(812).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(812, 'left')}
              >
              {/* EMBARCACION 812 */}
              {/* {`${getObjectSlip(812) !== undefined ? `${getObjectSlip(812).Nombre_barco.substring(0,12)} ${getObjectSlip(812).Embarcacion !== 0 ? getObjectSlip(812).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(812, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(812)}>{getTamañoEmbarcacion(812, leftPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb811"
            x={PosicionX_Slips800Der}
            y="63.543"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="65.543"
            // x={getBulletPosition(811, PosicionX_Slips800Der, rightPosition)}
            className={getColorEnBaseFactura(811,rightPosition)}
            >•</tspan>
            <tspan
              id="TE811"
              x={PosicionX_Slips800Der}
              y="63.543"
              strokeWidth="0.265"
              color="red"
              fontSize="4.939"
              // className={getObjectSlip(811) !== undefined ? getObjectSlip(811).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(811, 'right')}
              >
              {/* EMBARCACION 811 */}
              {/* {`${getObjectSlip(811) !== undefined ? `${getObjectSlip(811).Nombre_barco.substring(0,12)} ${getObjectSlip(811).Embarcacion !== 0 ? getObjectSlip(811).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(811, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(811)}>{getTamañoEmbarcacion(811, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb810"
            x={PosicionX_Slips800Der}
            y="73.979"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="75.979"
            // x={getBulletPosition(810, PosicionX_Slips800Der, rightPosition)}
            className={getColorEnBaseFactura(810,rightPosition)}
            >•</tspan>
            <tspan
              id="TE810"
              x={PosicionX_Slips800Der}
              y="73.979"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(810) !== undefined ? getObjectSlip(810).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(810, 'right')}
            >
              {/* EMBARCACION 810 */}
              {/* {`${getObjectSlip(810) !== undefined ? `${getObjectSlip(810).Nombre_barco.substring(0,12)} ${getObjectSlip(810).Embarcacion !== 0 ? getObjectSlip(810).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(810, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(810)}>{getTamañoEmbarcacion(810, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb809"
            x={PosicionX_Slips800Der}
            y="85.198"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="87.198"
            // x={getBulletPosition(809, PosicionX_Slips800Der, rightPosition)}
            className={getColorEnBaseFactura(809,rightPosition)}
            >•</tspan>
            <tspan
              id="TE809"
              x={PosicionX_Slips800Der}
              y="85.198"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(809) !== undefined ? getObjectSlip(809).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(809, 'right')}
            >
              {/* EMBARCACION 809 */}
              {/* {`${getObjectSlip(809) !== undefined ? `${getObjectSlip(809).Nombre_barco.substring(0,12)} ${getObjectSlip(809).Embarcacion !== 0 ? getObjectSlip(809).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(809, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(809)}>{getTamañoEmbarcacion(809, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb808"
            x={PosicionX_Slips800Izq}
            y="73.882"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="75.882"
            // x={getBulletPosition(808, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(808,leftPosition)}
            >•</tspan>
            <tspan
              id="TE808"
              x={PosicionX_Slips800Izq}
              y="73.882"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(808) !== undefined ? getObjectSlip(808).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(808, 'left')}
            >
              {/* EMBARCACION 808 */}
              {/* {`${getObjectSlip(808) !== undefined ? `${getObjectSlip(808).Nombre_barco.substring(0,12)} ${getObjectSlip(808).Embarcacion !== 0 ? getObjectSlip(808).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(808, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(808)}>{getTamañoEmbarcacion(808, leftPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb807"
            x={PosicionX_Slips800Izq}
            y="88.849"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="90.849"
            // x={getBulletPosition(807, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(807,leftPosition)}
            >•</tspan>
            <tspan
              id="TE807"
              x={PosicionX_Slips800Izq}
              y="88.849"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(807) !== undefined ? getObjectSlip(807).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(807, 'left')}
            >
              {/* EMBARCACION 807 */}
              {/* {`${getObjectSlip(807) !== undefined ? `${getObjectSlip(807).Nombre_barco.substring(0,12)} ${getObjectSlip(807).Embarcacion !== 0 ? getObjectSlip(807).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(807, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(807)}>{getTamañoEmbarcacion(807, leftPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb806"
            x={PosicionX_Slips800Der}
            y="94.504"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="96.504"
            // x={getBulletPosition(806, PosicionX_Slips800Der, rightPosition)}
            className={getColorEnBaseFactura(806,rightPosition)}
            >•</tspan>
            <tspan
              id="TE806"
              x={PosicionX_Slips800Der}
              y="94.504"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(806) !== undefined ? getObjectSlip(806).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(806, 'right')}
            >
              {/* EMBARCACION 806 */}
              {/* {`${getObjectSlip(806) !== undefined ? `${getObjectSlip(806).Nombre_barco.substring(0,12)} ${getObjectSlip(806).Embarcacion !== 0 ? getObjectSlip(806).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(806, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(806)}>{getTamañoEmbarcacion(806, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb805"
            x={PosicionX_Slips800Der}
            y="106.135"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="108.135"
            // x={getBulletPosition(805, PosicionX_Slips800Der, rightPosition)}
            className={getColorEnBaseFactura(805,rightPosition)}
            >•</tspan>
            <tspan
              id="TE805"
              x={PosicionX_Slips800Der}
              y="106.135"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(805) !== undefined ? getObjectSlip(805).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(805, 'right')}
            >
              {/* EMBARCACION 805 */}
              {/* {`${getObjectSlip(805) !== undefined ? `${getObjectSlip(805).Nombre_barco.substring(0,12)} ${getObjectSlip(805).Embarcacion !== 0 ? getObjectSlip(805).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(805, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(805)}>{getTamañoEmbarcacion(805, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb804"
            x={PosicionX_Slips800Izq}
            y="100.261"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="102.261"
            // x={getBulletPosition(804, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(804, leftPosition)}
            >•</tspan>
            <tspan
              id="TE804"
              x={PosicionX_Slips800Izq}
              y="100.261"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(804) !== undefined ? getObjectSlip(804).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(804, 'left')}
            >
              {/* EMBARCACION 804 */}
              {/* {`${getObjectSlip(804) !== undefined ? `${getObjectSlip(804).Nombre_barco.substring(0,12)} ${getObjectSlip(804).Embarcacion !== 0 ? getObjectSlip(804).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(804, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(804)}>{getTamañoEmbarcacion(804, leftPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb803"
            x={PosicionX_Slips800Izq}
            y="121.33"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="123.33"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(803, leftPosition)}
            >•</tspan>
            <tspan
              id="TE803"
              x={PosicionX_Slips800Izq}
              y="121.33"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(803) !== undefined ? getObjectSlip(803).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(803, 'left')}
            >
              {/* EMBARCACION 803 */}
              {/* {`${getObjectSlip(803) !== undefined ? `${getObjectSlip(803).Nombre_barco.substring(0,12)} ${getObjectSlip(803).Embarcacion !== 0 ? getObjectSlip(803).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(803, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(803)}>{getTamañoEmbarcacion(803, leftPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb802"
            x={PosicionX_Slips800Der}
            y="115.706"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="117.706"
            // x={getBulletPosition(802, PosicionX_Slips800Der, rightPosition)}
            className={getColorEnBaseFactura(802, rightPosition)}
            >•</tspan>
            <tspan
              id="TE802"
              x={PosicionX_Slips800Der}
              y="115.706"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(802) !== undefined ? getObjectSlip(802).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(802, 'right')}
            >
              {/* EMBARCACION 802 */}
              {/* {`${getObjectSlip(802) !== undefined ? `${getObjectSlip(802).Nombre_barco.substring(0,12)} ${getObjectSlip(802).Embarcacion !== 0 ? getObjectSlip(802).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(802, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(802)}>{getTamañoEmbarcacion(802, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb801"
            x={PosicionX_Slips800Der}
            y="127.903"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="129.903"
            // x={getBulletPosition(801, PosicionX_Slips800Der, rightPosition)}
            className={getColorEnBaseFactura(801, rightPosition)}
            >•</tspan>
            <tspan
              id="TE801"
              x={PosicionX_Slips800Der}
              y="127.903"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(801) !== undefined ? getObjectSlip(801).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(801, 'right')}
            >
              {/* EMBARCACION 801 */}
              {/* {`${getObjectSlip(801) !== undefined ? `${getObjectSlip(801).Nombre_barco.substring(0,12)} ${getObjectSlip(801).Embarcacion !== 0 ? getObjectSlip(801).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(801, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(801)}>{getTamañoEmbarcacion(801, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb706"
            x={PosicionX_Slips706}
            y="26.961"//37.218 
            display="inline"
            imageRendering="auto"
            transform="rotate(.606)"
          >
            <tspan 
            // y={PosicionY_Slips700}
            y="34.505"
            className={getColorEnBaseFactura(706, centerPosition)}
            >•</tspan>
            <tspan
              id="TE706"
              x={PosicionX_Slips706}
              y="26.961"//37.218 
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(706) !== undefined ? getObjectSlip(706).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(706, 'center')}
            >
              {/* EMBARCACION 706 */}
              {/* {`${getObjectSlip(706) !== undefined ? `${getObjectSlip(706).Nombre_barco.substring(0,12)} ${getObjectSlip(706).Embarcacion !== 0 ? getObjectSlip(706).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(706)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(706)}>{getTamañoEmbarcacion(706)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb705"
            x={PosicionX_Slips705}
            y="27.505"//44.88
            display="inline"
            imageRendering="auto"
            transform="rotate(.606)"
          >
            <tspan 
            y={PosicionY_Slips700}
            className={getColorEnBaseFactura(705, centerPosition)}
            >•</tspan>
            <tspan
              id="TE705"
              x={PosicionX_Slips705}
              y="27.505"//44.88
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(705) !== undefined ? getObjectSlip(705).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(705, 'center')}
            >
              {/* EMBARCACION 705 */}
              {/* {`${getObjectSlip(705) !== undefined ? `${getObjectSlip(705).Nombre_barco.substring(0,12)} ${getObjectSlip(705).Embarcacion !== 0 ? getObjectSlip(705).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(705)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(705)}>{getTamañoEmbarcacion(705)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb704"
            x={PosicionX_Slips704}
            y="27.969"//38.376
            display="inline"
            imageRendering="auto"
            transform="rotate(.606)"
          >
            <tspan 
            y={PosicionY_Slips700}
            className={getColorEnBaseFactura(704, centerPosition)}
            >•</tspan>
            <tspan
              id="TE704"
              x={PosicionX_Slips704}
              y="27.969"//38.376
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(704) !== undefined ? getObjectSlip(704).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(704, 'center')}
           >
              {/* EMBARCACION 704 */}
              {/* {`${getObjectSlip(704) !== undefined ? `${getObjectSlip(704).Nombre_barco.substring(0,12)} ${getObjectSlip(704).Embarcacion !== 0 ? getObjectSlip(704).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(704)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(704)}>{getTamañoEmbarcacion(704)}</tspan>
            {/* <tspan y="31.969" x={getBulletPosition(704, PosicionX_Slips704, centerPosition)} className={getColorEnBaseFactura(704, centerPosition)} >•</tspan> */}
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb703"
            x={PosicionX_Slips703}
            y="28.629"//45.984
            display="inline"
            imageRendering="auto"
            transform="rotate(.606)"
          >
            <tspan 
            y={PosicionY_Slips700}
            className={getColorEnBaseFactura(703, centerPosition)}
            >•</tspan>
            <tspan
              id="TE703"
              x={PosicionX_Slips703}
              y="28.629"//45.984
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(703) !== undefined ? getObjectSlip(703).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(703, 'center')}
            >
              {/* EMBARCACION 703 */}
              {/* {`${getObjectSlip(703) !== undefined ? `${getObjectSlip(703).Nombre_barco.substring(0,12)} ${getObjectSlip(703).Embarcacion !== 0 ? getObjectSlip(703).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(703)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(703)}>{getTamañoEmbarcacion(703)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb702"
            x={PosicionX_Slips702}
            y="29.086"//39.397
            display="inline"
            imageRendering="auto"
            transform="rotate(0.606)"
          >
            <tspan 
            // y={PosicionY_Slips700}
            y="36.205"
            className={getColorEnBaseFactura(702, centerPosition)}
            >•</tspan>
            <tspan
              id="TE702"
              x={PosicionX_Slips702}
              y="29.086"//39.397
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(702) !== undefined ? getObjectSlip(702).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(702, 'center')}
            >
              {/* EMBARCACION 702 */}
              {/* {`${getObjectSlip(702) !== undefined ? `${getObjectSlip(702).Nombre_barco.substring(0,12)} ${getObjectSlip(702).Embarcacion !== 0 ? getObjectSlip(702).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(702)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(702)}>{getTamañoEmbarcacion(702)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb701"
            x={PosicionX_Slips701}
            y="29.651"//47.072
            display="inline"
            imageRendering="auto"
            transform="rotate(.606)"
          >
            <tspan 
            // y={PosicionY_Slips700}
            y="36.30"
            className={getColorEnBaseFactura(701, centerPosition)}
            >•</tspan>
            <tspan
              id="TE701"
              x={PosicionX_Slips701}
              y="29.651"//47.072
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(701) !== undefined ? getObjectSlip(701).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(701, 'center')}
            >
              {/* EMBARCACION 701 */}
              {/* {`${getObjectSlip(701) !== undefined ? `${getObjectSlip(701).Nombre_barco.substring(0,12)} ${getObjectSlip(701).Embarcacion !== 0 ? getObjectSlip(701).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(701)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(701)}>{getTamañoEmbarcacion(701)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text834-3-1-4-1-5-1-2-3-3-1-7-9-3-0-78-2"
            x={PosicionX_Slips700}
            y="30.318"//40.376
            display="inline"
            imageRendering="auto"
            transform="rotate(.606)"
          >
            <tspan 
            // y={PosicionY_Slips700}
            y="36.90"
            className={getColorEnBaseFactura(700, centerPosition)}
            >•</tspan>
            <tspan
              id="TE700"
              x={PosicionX_Slips700}
              y="30.318"//40.376
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(700) !== undefined ? getObjectSlip(700).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(700, 'center')}
            >
              {/* EMBARCACION 700 */}
              {/* {`${getObjectSlip(700) !== undefined ? `${getObjectSlip(700).Nombre_barco.substring(0,12)} ${getObjectSlip(700).Embarcacion !== 0 ? getObjectSlip(700).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(700)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(700)}>{getTamañoEmbarcacion(700)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb612-6-0"
            x={PosicionX_Slips600Der}
            y="318.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="320.785"
            // x={getBulletPosition(614, PosicionX_Slips600Der, rightPosition)}
            className={getColorEnBaseFactura(614, rightPosition)}
            >•</tspan>
            <tspan
              id="TE614"
              x={PosicionX_Slips600Der}
              y="318.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(614) !== undefined ? getObjectSlip(614).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(614, 'right')}
            >
              {/* EMBARCACION 614 */}
              {/* {`${getObjectSlip(614) !== undefined ? `${getObjectSlip(614).Nombre_barco.substring(0,12)} ${getObjectSlip(614).Embarcacion !== 0 ? getObjectSlip(614).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(614, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(614)}>{getTamañoEmbarcacion(614, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb612-6"
            x={PosicionX_Slips600Der}
            y="307.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="309.785"
            // x={getBulletPosition(613, PosicionX_Slips600Der, rightPosition)}
            className={getColorEnBaseFactura(613, rightPosition)}
            >•</tspan>
            <tspan
              id="TE613"
              x={PosicionX_Slips600Der}
              y="307.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(613) !== undefined ? getObjectSlip(613).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(613, 'right')}
            >
              {/* EMBARCACION 613 */}
              {/* {`${getObjectSlip(613) !== undefined ? `${getObjectSlip(613).Nombre_barco.substring(0,12)} ${getObjectSlip(613).Embarcacion !== 0 ? getObjectSlip(613).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(613, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(613)}>{getTamañoEmbarcacion(613, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb612"
            x={PosicionX_Slips600Der}
            y="298.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="300.785"
            // x={getBulletPosition(612, PosicionX_Slips600Der, rightPosition)}
            className={getColorEnBaseFactura(612, rightPosition)}
            >•</tspan>
            <tspan
              id="TE612"
              x={PosicionX_Slips600Der}
              y="298.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(612) !== undefined ? getObjectSlip(612).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(612, 'right')}
            >
              {/* EMBARCACION 612 */}
              {/* {`${getObjectSlip(612) !== undefined ? `${getObjectSlip(612).Nombre_barco.substring(0,12)} ${getObjectSlip(612).Embarcacion !== 0 ? getObjectSlip(612).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(612, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(612)}>{getTamañoEmbarcacion(612, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb611"
            x={PosicionX_Slips600Der}
            y="286.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="288.785"
            // x={getBulletPosition(611, PosicionX_Slips600Der, rightPosition)}
            className={getColorEnBaseFactura(611, rightPosition)}
            >•</tspan>
            <tspan
              id="TE611"
              x={PosicionX_Slips600Der}
              y="286.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(611) !== undefined ? getObjectSlip(611).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(611, 'right')}
            >
              {/* EMBARCACION 611 */}
              {/* {`${getObjectSlip(611) !== undefined ? `${getObjectSlip(611).Nombre_barco.substring(0,12)} ${getObjectSlip(611).Embarcacion !== 0 ? getObjectSlip(611).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(611, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(611)}>{getTamañoEmbarcacion(611, rightPosition)}</tspan>
            
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb610"
            x={PosicionX_Slips600Der}
            y="276.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="278.785"
            className={getColorEnBaseFactura(610, rightPosition)}
            >•</tspan>
            <tspan
              id="TE610"
              x={PosicionX_Slips600Der}
              y="276.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(610) !== undefined ? getObjectSlip(610).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(610, 'right')}
            >
              {/* EMBARCACION 610 */}
              {/* {`${getObjectSlip(610) !== undefined ? `${getObjectSlip(610).Nombre_barco.substring(0,12)} ${getObjectSlip(610).Embarcacion !== 0 ? getObjectSlip(610).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 610, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(610)}>{getTamañoEmbarcacion(610, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb609"
            x={PosicionX_Slips600Der}
            y="264.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="266.785"
            className={getColorEnBaseFactura(609, rightPosition)}
            >•</tspan>
            <tspan
              id="TE609"
              x={PosicionX_Slips600Der}
              y="264.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(609) !== undefined ? getObjectSlip(609).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(609, 'right')}
            >
              {/* EMBARCACION 609 */}
              {/* {`${getObjectSlip(609) !== undefined ? `${getObjectSlip(609).Nombre_barco.substring(0,12)} ${getObjectSlip(609).Embarcacion !== 0 ? getObjectSlip(609).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 609, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(609)}>{getTamañoEmbarcacion(609, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb608"
            x={PosicionX_Slips600Der}
            y="255.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="257.785"
            className={getColorEnBaseFactura(608, rightPosition)}
            >•</tspan>
            <tspan
              id="TE608"
              x={PosicionX_Slips600Der}
              y="255.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(608) !== undefined ? getObjectSlip(608).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(608, 'right')}
            >
              {/* EMBARCACION 608 */}
              {/* {`${getObjectSlip(608) !== undefined ? `${getObjectSlip(608).Nombre_barco.substring(0,12)} ${getObjectSlip(608).Embarcacion !== 0 ? getObjectSlip(608).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 608, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(608)}>{getTamañoEmbarcacion(608, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="Txt606"
            x={PosicionX_Slips600Der}
            y="243.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="245.785"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(607, rightPosition)}
            >•</tspan>
            <tspan
              id="TE607"
              x={PosicionX_Slips600Der}
              y="243.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(607) !== undefined ? getObjectSlip(607).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(607, 'right')}
            >
              {/* EMBARCACION 607 */}
              {/* {`${getObjectSlip(607) !== undefined ? `${getObjectSlip(607).Nombre_barco.substring(0,12)} ${getObjectSlip(607).Embarcacion !== 0 ? getObjectSlip(607).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 607, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(607)}>{getTamañoEmbarcacion(607, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb606"
            x={PosicionX_Slips600Der}
            y="234.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="236.785"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(606, rightPosition)}
            >•</tspan>
            <tspan
              id="TE606"
              x={PosicionX_Slips600Der}
              y="234.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(606) !== undefined ? getObjectSlip(606).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(606, 'right')}
            >
              {/* EMBARCACION 606 */}
              {/* {`${getObjectSlip(606) !== undefined ? `${getObjectSlip(606).Nombre_barco.substring(0,12)} ${getObjectSlip(606).Embarcacion !== 0 ? getObjectSlip(606).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 606, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(606)}>{getTamañoEmbarcacion(606, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb605"
            x={PosicionX_Slips600Der}
            y="222.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="224.785"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(605, rightPosition)}
            >•</tspan>
            <tspan
              id="TE605"
              x={PosicionX_Slips600Der}
              y="222.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(605) !== undefined ? getObjectSlip(605).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(605, 'right')}
            >
              {/* EMBARCACION 605 */}
              {/* {`${getObjectSlip(605) !== undefined ? `${getObjectSlip(605).Nombre_barco.substring(0,12)} ${getObjectSlip(605).Embarcacion !== 0 ? getObjectSlip(605).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 605, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(605)}>{getTamañoEmbarcacion(605, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb604"
            x={PosicionX_Slips600Der}
            y="212.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="214.785"
            className={getColorEnBaseFactura(604, rightPosition)}
            >•</tspan>
            <tspan
              id="TE604"
              x={PosicionX_Slips600Der}
              y="212.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(604) !== undefined ? getObjectSlip(604).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(604, 'right')}
            >
              {/* EMBARCACION 604 */}
              {/* {`${getObjectSlip(604) !== undefined ? `${getObjectSlip(604).Nombre_barco.substring(0,12)} ${getObjectSlip(604).Embarcacion !== 0 ? getObjectSlip(604).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 604, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(604)}>{getTamañoEmbarcacion(604, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb603"
            x={PosicionX_Slips600Der}
            y="201.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="203.785"
            className={getColorEnBaseFactura(603, rightPosition)}
            >•</tspan>
            <tspan
              id="TE603"
              x={PosicionX_Slips600Der}
              y="201.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(603) !== undefined ? getObjectSlip(603).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(603, 'right')}
            >
              {/* EMBARCACION 603 */}
              {/* {`${getObjectSlip(603) !== undefined ? `${getObjectSlip(603).Nombre_barco.substring(0,12)} ${getObjectSlip(603).Embarcacion !== 0 ? getObjectSlip(603).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 603, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(603)}>{getTamañoEmbarcacion(603, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="text834-3-1-4-1-5-1-2-3-3-1-7-9-0-3-5-2"
            x={PosicionX_Slips600Der}
            y="191.785"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="193.785"
            className={getColorEnBaseFactura(602, rightPosition)}
            >•</tspan>
            <tspan
              id="TE602"
              x={PosicionX_Slips600Der}
              y="191.785"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(602) !== undefined ? getObjectSlip(602).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(602, 'right')}
            >
              {/* EMBARCACION 602 */}
              {/* {`${getObjectSlip(602) !== undefined ? `${getObjectSlip(602).Nombre_barco.substring(0,12)} ${getObjectSlip(602).Embarcacion !== 0 ? getObjectSlip(602).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 602, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(602)}>{getTamañoEmbarcacion(602, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb601"
            x={PosicionX_Slips600Der}
            y="179.786"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.155)"
          >
            <tspan 
            y="181.786"
            className={getColorEnBaseFactura(601, rightPosition)}
            >•</tspan>
            <tspan
              id="TE601"
              x={PosicionX_Slips600Der}
              y="179.786"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(601) !== undefined ? getObjectSlip(601).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
              className={getNombreClaseCSS(601, 'right')}
            >
              {/* EMBARCACION 601 */}
              {/* {`${getObjectSlip(601) !== undefined ? `${getObjectSlip(601).Nombre_barco.substring(0,12)} ${getObjectSlip(601).Embarcacion !== 0 ? getObjectSlip(601).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 601, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(601)}>{getTamañoEmbarcacion(601, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb532"
            x={PosicionX_Slips500Der}
            y="318.598"
            display="inline"
          >
            <tspan 
            y="320.598"
            className={getColorEnBaseFactura(532, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE532" 
            x={PosicionX_Slips500Der} 
            y="318.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(532) !== undefined ? getObjectSlip(532).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(532, 'right')}
            >
              {/* EMBARCACION 532 */}
              {/* {`${getObjectSlip(532) !== undefined ? `${getObjectSlip(532).Nombre_barco.substring(0,12)} ${getObjectSlip(532).Embarcacion !== 0 ? getObjectSlip(532).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 532, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(532)}>{getTamañoEmbarcacion(532, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb531"
            x={PosicionX_Slips500Izq}
            y="318.598"
            display="inline"
          >
            <tspan 
            y="320.598"
            className={getColorEnBaseFactura(531, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE531" 
            x={PosicionX_Slips500Izq} 
            y="318.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(531) !== undefined ? getObjectSlip(531).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(531, 'left')}
            >
              {/* EMBARCACION 531 */}
              {/* {`${getObjectSlip(531) !== undefined ? `${getObjectSlip(531).Nombre_barco.substring(0,12)} ${getObjectSlip(531).Embarcacion !== 0 ? getObjectSlip(531).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 531, leftPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(531)}>{getTamañoEmbarcacion(531, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtxEmb530"
            x={PosicionX_Slips500Der}
            y="307.598"
            display="inline"
          >
            <tspan 
            y="309.598"
            className={getColorEnBaseFactura(530, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE530" 
            x={PosicionX_Slips500Der} 
            y="307.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(530) !== undefined ? getObjectSlip(530).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(530, 'right')}
            >
              {/* EMBARCACION 530 */}
              {/* {`${getObjectSlip(530) !== undefined ? `${getObjectSlip(530).Nombre_barco.substring(0,12)} ${getObjectSlip(530).Embarcacion !== 0 ? getObjectSlip(530).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 530, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(530)}>{getTamañoEmbarcacion(530, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb529"
            x={PosicionX_Slips500Izq}
            y="307.598"
            display="inline"
          >
            <tspan 
            y="309.598"
            className={getColorEnBaseFactura(529, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE529" 
            x={PosicionX_Slips500Izq} 
            y="307.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(529) !== undefined ? getObjectSlip(529).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(529, 'left')}
            >
              {/* EMBARCACION 529 */}
              {/* {`${getObjectSlip(529) !== undefined ? `${getObjectSlip(529).Nombre_barco.substring(0,12)} ${getObjectSlip(529).Embarcacion !== 0 ? getObjectSlip(529).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 529, leftPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(529)}>{getTamañoEmbarcacion(529, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb528"
            x={PosicionX_Slips500Der}
            y="298.598"
            display="inline"
          >
            <tspan 
            y="300.598"
            className={getColorEnBaseFactura(528, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE528" 
            x={PosicionX_Slips500Der} 
            y="298.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(528) !== undefined ? getObjectSlip(528).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(528, 'right')}
            >
              {/* EMBARCACION 528 */}
              {/* {`${getObjectSlip(528) !== undefined ? `${getObjectSlip(528).Nombre_barco.substring(0,12)} ${getObjectSlip(528).Embarcacion !== 0 ? getObjectSlip(528).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 528, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(528)}>{getTamañoEmbarcacion(528, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb527"
            x={PosicionX_Slips500Izq}
            y="298.598"
            display="inline"
          >
            <tspan 
            y="300.598"
            className={getColorEnBaseFactura(527, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE527" 
            x={PosicionX_Slips500Izq} 
            y="298.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(527) !== undefined ? getObjectSlip(527).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(527, 'left')}
            >
              {/* EMBARCACION 527 */}
              {/* {`${getObjectSlip(527) !== undefined ? `${getObjectSlip(527).Nombre_barco.substring(0,12)} ${getObjectSlip(527).Embarcacion !== 0 ? getObjectSlip(527).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 527, leftPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(527)}>{getTamañoEmbarcacion(527, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb526"
            x={PosicionX_Slips500Der}
            y="286.598"
            display="inline"
          >
           <tspan 
            y="288.598"
            className={getColorEnBaseFactura(526, rightPosition)}
            >•</tspan> 
            <tspan 
            id="TE526" 
            x={PosicionX_Slips500Der} 
            y="286.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(526) !== undefined ? getObjectSlip(526).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(526, 'right')}
            >
              {/* EMBARCACION 526 */}
              {/* {`${getObjectSlip(526) !== undefined ? `${getObjectSlip(526).Nombre_barco.substring(0,12)} ${getObjectSlip(526).Embarcacion !== 0 ? getObjectSlip(526).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 526, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(526)}>{getTamañoEmbarcacion(526, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb525"
            x={PosicionX_Slips500Izq}
            y="286.598"
            display="inline"
          >
            <tspan 
            y="288.598"
            className={getColorEnBaseFactura(525, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE525" 
            x={PosicionX_Slips500Izq} 
            y="286.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(525) !== undefined ? getObjectSlip(525).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(525, 'left')}
            >
              {/* EMBARCACION 525 */}
              {/* {`${getObjectSlip(525) !== undefined ? `${getObjectSlip(525).Nombre_barco.substring(0,12)} ${getObjectSlip(525).Embarcacion !== 0 ? getObjectSlip(525).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 525, leftPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(525)}>{getTamañoEmbarcacion(525, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb524"
            x={PosicionX_Slips500Der}
            y="276.598"
            display="inline"
          >
            <tspan 
            y="278.598"
            className={getColorEnBaseFactura(524, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE524" 
            x={PosicionX_Slips500Der} 
            y="276.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(524) !== undefined ? getObjectSlip(524).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(524, 'right')}
            >
              {/* EMBARCACION 524 */}
              {/* {`${getObjectSlip(524) !== undefined ? `${getObjectSlip(524).Nombre_barco.substring(0,12)} ${getObjectSlip(524).Embarcacion !== 0 ? getObjectSlip(524).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 524, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(524)}>{getTamañoEmbarcacion(524, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb523"
            x={PosicionX_Slips500Izq}
            y="276.598"
            display="inline"
          >
            <tspan 
            y="278.598"
            className={getColorEnBaseFactura(523, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE523" 
            x={PosicionX_Slips500Izq} 
            y="276.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(523) !== undefined ? getObjectSlip(523).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(523, 'left')}
            >
              {/* EMBARCACION 523 */}
              {/* {`${getObjectSlip(523) !== undefined ? `${getObjectSlip(523).Nombre_barco.substring(0,12)} ${getObjectSlip(523).Embarcacion !== 0 ? getObjectSlip(523).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 523, leftPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(523)}>{getTamañoEmbarcacion(523, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb522"
            x={PosicionX_Slips500Der}
            y="264.598"
            display="inline"
          >
            <tspan 
            y="266.598"
            className={getColorEnBaseFactura(522, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE522" 
            x={PosicionX_Slips500Der} 
            y="264.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(522) !== undefined ? getObjectSlip(522).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(522, 'right')}
            >
              {/* EMBARCACION 522 */}
              {/* {`${getObjectSlip(522) !== undefined ? `${getObjectSlip(522).Nombre_barco.substring(0,12)} ${getObjectSlip(522).Embarcacion !== 0 ? getObjectSlip(522).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 522, rightPosition )}`}
            
            </tspan>
            <tspan className={getColorLongitudCSS(522)}>{getTamañoEmbarcacion(522, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb521"
            x={PosicionX_Slips500Izq}
            y="264.598"
            display="inline"
          >
            <tspan 
            y="266.598"
            className={getColorEnBaseFactura(521, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE521" 
            x={PosicionX_Slips500Izq} 
            y="264.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(521) !== undefined ? getObjectSlip(521).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(521, 'left')}
            >
              {/* EMBARCACION 521 */}
              {/* {`${getObjectSlip(521) !== undefined ? `${getObjectSlip(521).Nombre_barco.substring(0,12)} ${getObjectSlip(521).Embarcacion !== 0 ? getObjectSlip(521).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 521, leftPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(521)}>{getTamañoEmbarcacion(521, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb520"
            x={PosicionX_Slips500Der}
            y="255.598"
            display="inline"
          >
            <tspan 
            y="257.598"
            className={getColorEnBaseFactura(520, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE520" 
            x={PosicionX_Slips500Der} 
            y="255.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(520) !== undefined ? getObjectSlip(520).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(520, 'right')}
            >
              {/* EMBARCACION 520 */}
              {/* {`${getObjectSlip(520) !== undefined ? `${getObjectSlip(520).Nombre_barco.substring(0,12)} ${getObjectSlip(520).Embarcacion !== 0 ? getObjectSlip(520).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 520, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(520)}>{getTamañoEmbarcacion(520, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb519"
            x={PosicionX_Slips500Izq}
            y="255.598"
            display="inline"
          >
            <tspan 
            y="257.598"
            className={getColorEnBaseFactura(519, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE519" 
            x={PosicionX_Slips500Izq} 
            y="255.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(519) !== undefined ? getObjectSlip(519).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(519, 'left')}
            >
              {/* EMBARCACION 519 */}
              {/* {`${getObjectSlip(519) !== undefined ? `${getObjectSlip(519).Nombre_barco.substring(0,12)} ${getObjectSlip(519).Embarcacion !== 0 ? getObjectSlip(519).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(519, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(519)}>{getTamañoEmbarcacion(519, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb518"
            x={PosicionX_Slips500Der}
            y="243.598"
            display="inline"
          >
            <tspan 
            y="245.598"
            className={getColorEnBaseFactura(518, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE518" 
            x={PosicionX_Slips500Der} 
            y="243.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(518) !== undefined ? getObjectSlip(518).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(518, 'right')}
            >
              {/* EMBARCACION 518 */}
              {/* {`${getObjectSlip(518) !== undefined ? `${getObjectSlip(518).Nombre_barco.substring(0,12)} ${getObjectSlip(518).Embarcacion !== 0 ? getObjectSlip(518).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(518, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(518)}>{getTamañoEmbarcacion(518, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb517"
            x={PosicionX_Slips500Izq}
            y="243.598"
            display="inline"
          >
            <tspan 
            y="245.598"
            className={getColorEnBaseFactura(517, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE517" 
            x={PosicionX_Slips500Izq} 
            y="243.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(517) !== undefined ? getObjectSlip(517).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(517, 'left')}
            >
              {/* EMBARCACION 517 */}
              {/* {`${getObjectSlip(517) !== undefined ? `${getObjectSlip(517).Nombre_barco.substring(0,12)} ${getObjectSlip(517).Embarcacion !== 0 ? getObjectSlip(517).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(517, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(517)}>{getTamañoEmbarcacion(517, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb516"
            x={PosicionX_Slips500Der}
            y="234.598"
            display="inline"
          >
            <tspan 
            y="236.598"
            className={getColorEnBaseFactura(516, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE516" 
            x={PosicionX_Slips500Der} 
            y="234.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(516) !== undefined ? getObjectSlip(516).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(516, 'right')}
            >
              {/* EMBARCACION 516 */}
              {/* {`${getObjectSlip(516) !== undefined ? `${getObjectSlip(516).Nombre_barco.substring(0,12)} ${getObjectSlip(516).Embarcacion !== 0 ? getObjectSlip(516).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(516, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(516)}>{getTamañoEmbarcacion(516, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb515"
            x={PosicionX_Slips500Izq}
            y="234.598"
            display="inline"
          >
            <tspan 
            y="236.598"
            className={getColorEnBaseFactura(515, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE515" 
            x={PosicionX_Slips500Izq} 
            y="234.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(515) !== undefined ? getObjectSlip(515).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(515, 'left')}
            >
              {/* EMBARCACION 515 */}
              {/* {`${getObjectSlip(515) !== undefined ? `${getObjectSlip(515).Nombre_barco.substring(0,12)} ${getObjectSlip(515).Embarcacion !== 0 ? getObjectSlip(515).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(515, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(515)}>{getTamañoEmbarcacion(515, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb514"
            x={PosicionX_Slips500Der}
            y="221.598"
            display="inline"
          >
            <tspan 
            y="223.598"
            className={getColorEnBaseFactura(514, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE514" 
            x={PosicionX_Slips500Der} 
            y="221.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(514) !== undefined ? getObjectSlip(514).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(514, 'right')}
            >
              {/* EMBARCACION 514 */}
              {/* {`${getObjectSlip(514) !== undefined ? `${getObjectSlip(514).Nombre_barco.substring(0,12)} ${getObjectSlip(514).Embarcacion !== 0 ? getObjectSlip(514).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(514, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(514)}>{getTamañoEmbarcacion(514, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb513"
            x={PosicionX_Slips500Izq}
            y="221.598"
            display="inline"
          >
            <tspan 
            y="223.598"
            className={getColorEnBaseFactura(513, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE513" 
            x={PosicionX_Slips500Izq} 
            y="221.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(513) !== undefined ? getObjectSlip(513).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(513, 'left')}
            >
              {/* EMBARCACION 513 */}
              {/* {`${getObjectSlip(513) !== undefined ? `${getObjectSlip(513).Nombre_barco.substring(0,12)} ${getObjectSlip(513).Embarcacion !== 0 ? getObjectSlip(513).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(513, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(513)}>{getTamañoEmbarcacion(513, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb512"
            x={PosicionX_Slips500Der}
            y="212.598"
            display="inline"
          >
            <tspan 
            y="214.598"
            className={getColorEnBaseFactura(512, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE512" 
            x={PosicionX_Slips500Der} 
            y="212.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(512) !== undefined ? getObjectSlip(512).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(512, 'right')}
            >
              {/* EMBARCACION 512 */}
              {/* {`${getObjectSlip(512) !== undefined ? `${getObjectSlip(512).Nombre_barco.substring(0,12)} ${getObjectSlip(512).Embarcacion !== 0 ? getObjectSlip(512).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(512, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(512)}>{getTamañoEmbarcacion(512, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb511"
            x={PosicionX_Slips500Izq}
            y="212.598"
            display="inline"
          >
            <tspan 
            y="214.598"
            className={getColorEnBaseFactura(511, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE511" 
            x={PosicionX_Slips500Izq} 
            y="212.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(511) !== undefined ? getObjectSlip(511).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(511, 'left')}
            >
              {/* EMBARCACION 511 */}
              {/* {`${getObjectSlip(511) !== undefined ? `${getObjectSlip(511).Nombre_barco.substring(0,12)} ${getObjectSlip(511).Embarcacion !== 0 ? getObjectSlip(511).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(511, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(511)}>{getTamañoEmbarcacion(511, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb510"
            x={PosicionX_Slips500Der}
            y="201.598"
            display="inline"
          >
            <tspan 
            y="203.598"
            className={getColorEnBaseFactura(510, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE510" 
            x={PosicionX_Slips500Der} 
            y="201.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(510) !== undefined ? getObjectSlip(510).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(510, 'right')}
            >
              {/* EMBARCACION 510 */}
              {/* {`${getObjectSlip(510) !== undefined ? `${getObjectSlip(510).Nombre_barco.substring(0,12)} ${getObjectSlip(510).Embarcacion !== 0 ? getObjectSlip(510).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(510, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(510)}>{getTamañoEmbarcacion(510, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb509"
            x={PosicionX_Slips500Izq}
            y="201.598"
            display="inline"
          >
            <tspan 
            y="203.598"
            className={getColorEnBaseFactura(509, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE509" 
            x={PosicionX_Slips500Izq} 
            y="201.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(509) !== undefined ? getObjectSlip(509).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(509, 'left')}
            >
              {/* EMBARCACION 509 */}
              {/* {`${getObjectSlip(509) !== undefined ? `${getObjectSlip(509).Nombre_barco.substring(0,12)} ${getObjectSlip(509).Embarcacion !== 0 ? getObjectSlip(509).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(509, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(509)}>{getTamañoEmbarcacion(509, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb508"
            x={PosicionX_Slips500Der}
            y="191.598"
            display="inline"
          >
            <tspan 
            y="193.598"
            className={getColorEnBaseFactura(508, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE508" 
            x={PosicionX_Slips500Der} 
            y="191.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(508) !== undefined ? getObjectSlip(508).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(508, 'right')}
            >
              {/* EMBARCACION 508 */}
              {/* {`${getObjectSlip(508) !== undefined ? `${getObjectSlip(508).Nombre_barco.substring(0,12)} ${getObjectSlip(508).Embarcacion !== 0 ? getObjectSlip(508).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(508, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(508)}>{getTamañoEmbarcacion(508, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb507"
            x={PosicionX_Slips500Izq}
            y="191.598"
            display="inline"
          >
            <tspan 
            y="193.598"
            className={getColorEnBaseFactura(507, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE507" 
            x={PosicionX_Slips500Izq} 
            y="191.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(507) !== undefined ? getObjectSlip(507).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(507, 'left')}
            >
              {/* EMBARCACION 507 */}
              {/* {`${getObjectSlip(507) !== undefined ? `${getObjectSlip(507).Nombre_barco.substring(0,12)} ${getObjectSlip(507).Embarcacion !== 0 ? getObjectSlip(507).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(507, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(507)}>{getTamañoEmbarcacion(507, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb506"
            x={PosicionX_Slips500Der}
            y="179.598"
            display="inline"
          >
            <tspan 
            y="181.598"
            className={getColorEnBaseFactura(506, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE506" 
            x={PosicionX_Slips500Der} 
            y="179.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(506) !== undefined ? getObjectSlip(506).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(506, 'right')}
            >
              {/* EMBARCACION 506 */}
              {/* {`${getObjectSlip(506) !== undefined ? `${getObjectSlip(506).Nombre_barco.substring(0,12)} ${getObjectSlip(506).Embarcacion !== 0 ? getObjectSlip(506).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(506, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(506)}>{getTamañoEmbarcacion(506, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb505"
            x={PosicionX_Slips500Izq}
            y="179.598"
            display="inline"
          >
            <tspan 
            y="181.598"
            className={getColorEnBaseFactura(505, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE505" 
            x={PosicionX_Slips500Izq} 
            y="179.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(505) !== undefined ? getObjectSlip(505).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(505, 'left')}
            >
              {/* EMBARCACION 505 */}
              {/* {`${getObjectSlip(505) !== undefined ? `${getObjectSlip(505).Nombre_barco.substring(0,12)} ${getObjectSlip(505).Embarcacion !== 0 ? getObjectSlip(505).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(505, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(505)}>{getTamañoEmbarcacion(505, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb504"
            x={PosicionX_Slips500Der}
            y="171.598"
            display="inline"
          >
            <tspan 
            y="173.598"
            className={getColorEnBaseFactura(504, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE504" 
            x={PosicionX_Slips500Der} 
            y="171.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(504) !== undefined ? getObjectSlip(504).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(504, 'right')}
            >
              {/* EMBARCACION 504 */}
              {/* {`${getObjectSlip(504) !== undefined ? `${getObjectSlip(504).Nombre_barco.substring(0,12)} ${getObjectSlip(504).Embarcacion !== 0 ? getObjectSlip(504).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(504, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(504)}>{getTamañoEmbarcacion(504, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb503"
            x={PosicionX_Slips500Izq}
            y="171.598"
            display="inline"
          >
            <tspan 
            y="173.598"
            className={getColorEnBaseFactura(503, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE503" 
            x={PosicionX_Slips500Izq} 
            y="171.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(503) !== undefined ? getObjectSlip(503).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(503, 'left')}
            >
              {/* EMBARCACION 503 */}
              {/* {`${getObjectSlip(503) !== undefined ? `${getObjectSlip(503).Nombre_barco.substring(0,12)} ${getObjectSlip(503).Embarcacion !== 0 ? getObjectSlip(503).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(503, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(503)}>{getTamañoEmbarcacion(503, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb502"
            x={PosicionX_Slips500Der}
            y="159.598"
            display="inline"
          >
            <tspan 
            y="161.598"
            className={getColorEnBaseFactura(502, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE502" 
            x={PosicionX_Slips500Der} 
            y="159.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(502) !== undefined ? getObjectSlip(502).Embarcacion !== 0 ? "text-color-normal" : getObjectSlip(502).Compartido === "S" ? "text-color-compartido" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(502,'right')}
            >
              {/* EMBARCACION 502 */}
              {/* {`${getObjectSlip(502) !== undefined ? `${getObjectSlip(502).Nombre_barco.substring(0,12)} ${getObjectSlip(502).Embarcacion !== 0 ? getObjectSlip(502).Embarcacion : getObjectSlip(502).Compartido === "S" ? getObjectSlip(502).Observacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(502, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(502)}>{getTamañoEmbarcacion(502, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb501"
            x={PosicionX_Slips500Izq}
            y="159.598"
            display="inline"
          >
            <tspan 
            y="161.598"
            className={getColorEnBaseFactura(501, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE501" 
            x={PosicionX_Slips500Izq} 
            y="159.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(501) !== undefined ? getObjectSlip(501).Embarcacion !== 0 ? "text-color-normal" : getObjectSlip(501).Compartido === "S" ? "text-color-compartido" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(501, 'left')}
            >
              {/* EMBARCACION 501 */}
              {/* {`${getObjectSlip(501) !== undefined ? `${getObjectSlip(501).Nombre_barco.substring(0,12)} ${getObjectSlip(501).Embarcacion !== 0 ? getObjectSlip(501).Embarcacion : getObjectSlip(501).Compartido === "S" ? getObjectSlip(501).Observacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(501, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(501)}>{getTamañoEmbarcacion(501, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb436"
            x={PosicionX_Slips400Der}
            y="318.598"
            display="inline"
          >
            <tspan 
            y="320.598"
            className={getColorEnBaseFactura(436, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE436" 
            x={PosicionX_Slips400Der} 
            y="318.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(436) !== undefined ? getObjectSlip(436).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(436, 'right')}
            >
              {/* EMBARCACION 436 */}
              {/* {`${getObjectSlip(436) !== undefined ? `${getObjectSlip(436).Nombre_barco.substring(0,12)} ${getObjectSlip(436).Embarcacion !== 0 ? getObjectSlip(436).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(436, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(436)}>{getTamañoEmbarcacion(436, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb435"
            x={PosicionX_Slips400Izq}
            y="318.598"
            display="inline"
          >
            <tspan 
            y="320.598"
            className={getColorEnBaseFactura(435, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE435" 
            x={PosicionX_Slips400Izq} 
            y="318.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(435) !== undefined ? getObjectSlip(435).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(435, 'left')}
            >
              {/* EMBARCACION 435 */}
              {/* {`${getObjectSlip(435) !== undefined ? `${getObjectSlip(435).Nombre_barco.substring(0,12)} ${getObjectSlip(435).Embarcacion !== 0 ? getObjectSlip(435).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(435, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(435)}>{getTamañoEmbarcacion(435, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb434"
            x={PosicionX_Slips400Der}
            y="307.598"
            display="inline"
          >
            <tspan 
            y="309.598"
            className={getColorEnBaseFactura(434, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE434" 
            x={PosicionX_Slips400Der} 
            y="307.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(434) !== undefined ? getObjectSlip(434).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(434, 'right')}
            >
              {/* EMBARCACION 434 */}
              {/* {`${getObjectSlip(434) !== undefined ? `${getObjectSlip(434).Nombre_barco.substring(0,12)} ${getObjectSlip(434).Embarcacion !== 0 ? getObjectSlip(434).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(434, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(434)}>{getTamañoEmbarcacion(434, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb433"
            x={PosicionX_Slips400Izq}
            y="307.598"
            display="inline"
          >
            <tspan 
            y="309.598"
            className={getColorEnBaseFactura(433, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE433" 
            x={PosicionX_Slips400Izq} 
            y="307.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(433) !== undefined ? getObjectSlip(433).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(433, 'left')}
            >
              {/* EMBARCACION 433 */}
              {/* {`${getObjectSlip(433) !== undefined ? `${getObjectSlip(433).Nombre_barco.substring(0,12)} ${getObjectSlip(433).Embarcacion !== 0 ? getObjectSlip(433).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(433, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(433)}>{getTamañoEmbarcacion(433, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb432"
            x={PosicionX_Slips400Der}
            y="298.598"
            display="inline"
          >
            <tspan 
            y="300.598"
            className={getColorEnBaseFactura(432, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE432" 
            x={PosicionX_Slips400Der} 
            y="298.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(432) !== undefined ? getObjectSlip(432).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(432, 'right')}
            >
              {/* EMBARCACION 432 */}
              {/* {`${getObjectSlip(432) !== undefined ? `${getObjectSlip(432).Nombre_barco.substring(0,12)} ${getObjectSlip(432).Embarcacion !== 0 ? getObjectSlip(432).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(432, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(432)}>{getTamañoEmbarcacion(432, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb431"
            x={PosicionX_Slips400Izq}
            y="298.598"
            display="inline"
          >
            <tspan 
            y="300.598"
            className={getColorEnBaseFactura(431, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE431" 
            x={PosicionX_Slips400Izq} 
            y="298.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(431) !== undefined ? getObjectSlip(431).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(431, 'left')}
            >
              {/* EMBARCACION 431 */}
              {/* {`${getObjectSlip(431) !== undefined ? `${getObjectSlip(431).Nombre_barco.substring(0,12)} ${getObjectSlip(431).Embarcacion !== 0 ? getObjectSlip(431).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(431, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(431)}>{getTamañoEmbarcacion(431, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb430"
            x={PosicionX_Slips400Der}
            y="286.598"
            display="inline"
          >
            <tspan 
            y="288.598"
            className={getColorEnBaseFactura(430, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE430" 
            x={PosicionX_Slips400Der} 
            y="286.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(430) !== undefined ? getObjectSlip(430).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(430, 'right')}
            >
              {/* EMBARCACION 430 */}
              {/* {`${getObjectSlip(430) !== undefined ? `${getObjectSlip(430).Nombre_barco.substring(0,12)} ${getObjectSlip(430).Embarcacion !== 0 ? getObjectSlip(430).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(430, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(430)}>{getTamañoEmbarcacion(430, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb429"
            x={PosicionX_Slips400Izq}
            y="286.598"
            display="inline"
          >
            <tspan 
            y="288.598"
            className={getColorEnBaseFactura(429, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE429" 
            x={PosicionX_Slips400Izq} 
            y="286.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(429) !== undefined ? getObjectSlip(429).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(429, 'left')}
            >
              {/* EMBARCACION 429 */}
              {/* {`${getObjectSlip(429) !== undefined ? `${getObjectSlip(429).Nombre_barco.substring(0,12)} ${getObjectSlip(429).Embarcacion !== 0 ? getObjectSlip(429).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(429, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(429)}>{getTamañoEmbarcacion(429, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb428"
            x={PosicionX_Slips400Der}
            y="276.598"
            display="inline"
          >
            <tspan 
            y="278.598"
            className={getColorEnBaseFactura(428, rightPosition)}
            >•</tspan>
            <tspan 
            id="tTE428" 
            x={PosicionX_Slips400Der} 
            y="276.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(428) !== undefined ? getObjectSlip(428).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(428, 'right')}
            >
              {/* EMBARCACION 428 */}
              {/* {`${getObjectSlip(428) !== undefined ? `${getObjectSlip(428).Nombre_barco.substring(0,12)} ${getObjectSlip(428).Embarcacion !== 0 ? getObjectSlip(428).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(428, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(428)}>{getTamañoEmbarcacion(428, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb427"
            x={PosicionX_Slips400Izq}
            y="276.598"
            display="inline"
          >
            <tspan 
            y="278.598"
            className={getColorEnBaseFactura(427, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE427" 
            x={PosicionX_Slips400Izq} 
            y="276.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(427) !== undefined ? getObjectSlip(427).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(427, 'left')}
            >
              {/* EMBARCACION 427 */}
              {/* {`${getObjectSlip(427) !== undefined ? `${getObjectSlip(427).Nombre_barco.substring(0,12)} ${getObjectSlip(427).Embarcacion !== 0 ? getObjectSlip(427).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(427, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(427)}>{getTamañoEmbarcacion(427, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb426"
            x={PosicionX_Slips400Der}
            y="264.598"
            display="inline"
          >
            <tspan 
            y="266.598"
            className={getColorEnBaseFactura(426, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE426" x={PosicionX_Slips400Der} 
            y="264.598" 
            strokeWidth="0.265" 
            // className={getObjectSlip(426) !== undefined ? getObjectSlip(426).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(426, 'right')}
            >
              {/* EMBARCACION 426 */}
              {/* {`${getObjectSlip(426) !== undefined ? `${getObjectSlip(426).Nombre_barco.substring(0,12)} ${getObjectSlip(426).Embarcacion !== 0 ? getObjectSlip(426).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(426, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(426)}>{getTamañoEmbarcacion(426, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb425"
            x={PosicionX_Slips400Izq}
            y="265.127"
            display="inline"
          >
            <tspan 
            y="267.127"
            className={getColorEnBaseFactura(425, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE425" 
            x={PosicionX_Slips400Izq} 
            y="265.127" 
            strokeWidth="0.265"
            // className={getObjectSlip(425) !== undefined ? getObjectSlip(425).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(425, 'left')}
            >
              {/* EMBARCACION 425 */}
              {/* {`${getObjectSlip(425) !== undefined ? `${getObjectSlip(425).Nombre_barco.substring(0,12)} ${getObjectSlip(425).Embarcacion !== 0 ? getObjectSlip(425).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(425, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(425)}>{getTamañoEmbarcacion(425, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb424"
            x={PosicionX_Slips400Der}
            y="255.598"
            display="inline"
          >
            <tspan 
            y="257.598"
            className={getColorEnBaseFactura(424, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE424" 
            x={PosicionX_Slips400Der} 
            y="255.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(424) !== undefined ? getObjectSlip(424).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(424, 'right')}
            >
              {/* EMBARCACION 424 */}
              {/* {`${getObjectSlip(424) !== undefined ? `${getObjectSlip(424).Nombre_barco.substring(0,12)} ${getObjectSlip(424).Embarcacion !== 0 ? getObjectSlip(424).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(424, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(424)}>{getTamañoEmbarcacion(424, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb423"
            x={PosicionX_Slips400Izq}
            y="255.598"
            display="inline"
          >
            <tspan 
            y="257.598"
            className={getColorEnBaseFactura(423, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE423" 
            x={PosicionX_Slips400Izq} 
            y="255.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(423) !== undefined ? getObjectSlip(423).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(423, 'left')}
            >
              {/* EMBARCACION 423 */}
              {/* {`${getObjectSlip(423) !== undefined ? `${getObjectSlip(423).Nombre_barco.substring(0,12)} ${getObjectSlip(423).Embarcacion !== 0 ? getObjectSlip(423).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(423, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(423)}>{getTamañoEmbarcacion(423, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb422"
            x={PosicionX_Slips400Der}
            y="243.598"
            display="inline"
          >
            <tspan 
            y="245.598"
            className={getColorEnBaseFactura(422, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE422" 
            x={PosicionX_Slips400Der} 
            y="243.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(422) !== undefined ? getObjectSlip(422).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(422, 'right')}
            >
              {/* EMBARCACION 422 */}
              {/* {`${getObjectSlip(422) !== undefined ? `${getObjectSlip(422).Nombre_barco.substring(0,12)} ${getObjectSlip(422).Embarcacion !== 0 ? getObjectSlip(422).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(422, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(422)}>{getTamañoEmbarcacion(422, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb421"
            x={PosicionX_Slips400Izq}
            y="243.598"
            display="inline"
          >
            <tspan 
            y="245.598"
            className={getColorEnBaseFactura(421, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE421" 
            x={PosicionX_Slips400Izq} 
            y="243.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(421) !== undefined ? getObjectSlip(421).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(421, 'left')}
            >
              {/* EMBARCACION 421 */}
              {/* {`${getObjectSlip(421) !== undefined ? `${getObjectSlip(421).Nombre_barco.substring(0,12)} ${getObjectSlip(421).Embarcacion !== 0 ? getObjectSlip(421).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(421, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(421)}>{getTamañoEmbarcacion(421, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb420"
            x={PosicionX_Slips400Der}
            y="234.598"
            display="inline"
          >
            <tspan 
            y="236.598"
            className={getColorEnBaseFactura(420, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE420" 
            x={PosicionX_Slips400Der} 
            y="234.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(420) !== undefined ? getObjectSlip(420).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(420, 'right')}
            >
              {/* EMBARCACION 420 */}
              {/* {`${getObjectSlip(420) !== undefined ? `${getObjectSlip(420).Nombre_barco.substring(0,12)} ${getObjectSlip(420).Embarcacion !== 0 ? getObjectSlip(420).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(420, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(420)}>{getTamañoEmbarcacion(420, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb419"
            x={PosicionX_Slips400Izq}
            y="234.598"
            display="inline"
          >
            <tspan 
            y="234.598"
            className={getColorEnBaseFactura(419, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE419" 
            x={PosicionX_Slips400Izq} 
            y="234.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(419) !== undefined ? getObjectSlip(419).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(419, 'left')}
            >
              {/* EMBARCACION 419 */}
              {/* {`${getObjectSlip(419) !== undefined ? `${getObjectSlip(419).Nombre_barco.substring(0,12)} ${getObjectSlip(419).Embarcacion !== 0 ? getObjectSlip(419).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(419, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(419)}>{getTamañoEmbarcacion(419, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb418"
            x={PosicionX_Slips400Der}
            y="221.598"
            display="inline"
          >
            <tspan 
            y="223.598"
            className={getColorEnBaseFactura(418, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE418" 
            x={PosicionX_Slips400Der} 
            y="221.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(418) !== undefined ? getObjectSlip(418).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(418, 'right')}
            >
              {/* EMBARCACION 418 */}
              {/* {`${getObjectSlip(418) !== undefined ? `${getObjectSlip(418).Nombre_barco.substring(0,12)} ${getObjectSlip(418).Embarcacion !== 0 ? getObjectSlip(418).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(418, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(418)}>{getTamañoEmbarcacion(418, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb417"
            x={PosicionX_Slips400Izq}
            y="221.598"
            display="inline"
          >
            <tspan 
            y="223.598"
            className={getColorEnBaseFactura(417, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE417" 
            x={PosicionX_Slips400Izq} 
            y="221.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(417) !== undefined ? getObjectSlip(417).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(417, 'left')}
            >
              {/* EMBARCACION 417 */}
              {/* {`${getObjectSlip(417) !== undefined ? `${getObjectSlip(417).Nombre_barco.substring(0,12)} ${getObjectSlip(417).Embarcacion !== 0 ? getObjectSlip(417).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(417, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(417)}>{getTamañoEmbarcacion(417, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb416"
            x={PosicionX_Slips400Der}
            y="212.598"
            display="inline"
          >
            <tspan 
            y="214.598"
            className={getColorEnBaseFactura(416, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE416" 
            x={PosicionX_Slips400Der} 
            y="212.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(416) !== undefined ? getObjectSlip(416).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(416, 'right')}
            >
              {/* EMBARCACION 416 */}
              {/* {`${getObjectSlip(416) !== undefined ? `${getObjectSlip(416).Nombre_barco.substring(0,12)} ${getObjectSlip(416).Embarcacion !== 0 ? getObjectSlip(416).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(416, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(416)}>{getTamañoEmbarcacion(416, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb415"
            x={PosicionX_Slips400Izq}
            y="212.598"
            display="inline"
          >
            <tspan 
            y="214.598"
            className={getColorEnBaseFactura(415, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE415" 
            x={PosicionX_Slips400Izq} 
            y="212.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(415) !== undefined ? getObjectSlip(415).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(415, 'left')}
            >
              {/* EMBARCACION 415 */}
              {/* {`${getObjectSlip(415) !== undefined ? `${getObjectSlip(415).Nombre_barco.substring(0,12)} ${getObjectSlip(415).Embarcacion !== 0 ? getObjectSlip(415).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(415, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(415)}>{getTamañoEmbarcacion(415, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb414"
            x={PosicionX_Slips400Der}
            y="201.598"
            display="inline"
          >
            <tspan 
            y="203.598"
            className={getColorEnBaseFactura(414, rightPosition)}
            >•</tspan>
            <tspan 
            id="tTE414" 
            x={PosicionX_Slips400Der} 
            y="201.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(414) !== undefined ? getObjectSlip(414).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(414, 'right')}
            >
              {/* EMBARCACION 414 */}
              {/* {`${getObjectSlip(414) !== undefined ? `${getObjectSlip(414).Nombre_barco.substring(0,12)} ${getObjectSlip(414).Embarcacion !== 0 ? getObjectSlip(414).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(414, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(414)}>{getTamañoEmbarcacion(414, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb413"
            x={PosicionX_Slips400Izq}
            y="201.598"
            display="inline"
          >
            <tspan 
            y="203.598"
            className={getColorEnBaseFactura(413, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE413" 
            x={PosicionX_Slips400Izq} 
            y="201.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(413) !== undefined ? getObjectSlip(413).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(413, 'left')}
            >
              {/* EMBARCACION 413 */}
              {/* {`${getObjectSlip(413) !== undefined ? `${getObjectSlip(413).Nombre_barco.substring(0,12)} ${getObjectSlip(413).Embarcacion !== 0 ? getObjectSlip(413).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(413, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(413)}>{getTamañoEmbarcacion(413, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb412"
            x={PosicionX_Slips400Der}
            y="191.598"
            display="inline"
          >
            <tspan 
            y="193.598"
            className={getColorEnBaseFactura(412, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE412" 
            x={PosicionX_Slips400Der} 
            y="191.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(412) !== undefined ? getObjectSlip(412).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(412, 'right')}
            >
              {/* EMBARCACION 412 */}
              {/* {`${getObjectSlip(412) !== undefined ? `${getObjectSlip(412).Nombre_barco.substring(0,12)} ${getObjectSlip(412).Embarcacion !== 0 ? getObjectSlip(412).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(412, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(412)}>{getTamañoEmbarcacion(412, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb411"
            x={PosicionX_Slips400Izq}
            y="191.598"
            display="inline"
          >
            <tspan 
            y="193.598"
            className={getColorEnBaseFactura(411, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE411" 
            x={PosicionX_Slips400Izq} 
            y="191.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(411) !== undefined ? getObjectSlip(411).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(411, 'left')}
            >
              {/* EMBARCACION 411 */}
              {/* {`${getObjectSlip(411) !== undefined ? `${getObjectSlip(411).Nombre_barco.substring(0,12)} ${getObjectSlip(411).Embarcacion !== 0 ? getObjectSlip(411).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(411, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(411)}>{getTamañoEmbarcacion(411, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb410"
            x={PosicionX_Slips400Der}
            y="179.598"
            display="inline"
          >
            <tspan 
            y="181.598"
            className={getColorEnBaseFactura(410, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE410" 
            x={PosicionX_Slips400Der} 
            y="179.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(410) !== undefined ? getObjectSlip(410).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(410, 'right')}
            >
              {/* EMBARCACION 410 */}
              {/* {`${getObjectSlip(410) !== undefined ? `${getObjectSlip(410).Nombre_barco.substring(0,12)} ${getObjectSlip(410).Embarcacion !== 0 ? getObjectSlip(410).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(410, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(410)}>{getTamañoEmbarcacion(410, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb409"
            x={PosicionX_Slips400Izq}
            y="179.598"
            display="inline"
          >
            <tspan 
            y="181.598"
            className={getColorEnBaseFactura(409, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE409" 
            x={PosicionX_Slips400Izq} 
            y="179.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(409) !== undefined ? getObjectSlip(409).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(409, 'left')}
            >
              {/* EMBARCACION 409 */}
              {/* {`${getObjectSlip(409) !== undefined ? `${getObjectSlip(409).Nombre_barco.substring(0,12)} ${getObjectSlip(409).Embarcacion !== 0 ? getObjectSlip(409).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(409, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(409)}>{getTamañoEmbarcacion(409, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb408"
            x={PosicionX_Slips400Der}
            y="171.598"
            display="inline"
          >
            <tspan 
            y="173.598"
            className={getColorEnBaseFactura(408, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE408" 
            x={PosicionX_Slips400Der} 
            y="171.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(408) !== undefined ? getObjectSlip(408).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(408, 'right')}
            >
              {/* EMBARCACION 408 */}
              {/* {`${getObjectSlip(408) !== undefined ? `${getObjectSlip(408).Nombre_barco.substring(0,12)} ${getObjectSlip(408).Embarcacion !== 0 ? getObjectSlip(408).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(408, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(408)}>{getTamañoEmbarcacion(408, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb407"
            x={PosicionX_Slips400Izq}
            y="171.598"
            display="inline"
          >
            <tspan 
            y="173.598"
            className={getColorEnBaseFactura(407, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE407" 
            x={PosicionX_Slips400Izq} 
            y="171.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(407) !== undefined ? getObjectSlip(407).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(407, 'left')}
            >
              {/* EMBARCACION 407 */}
              {/* {`${getObjectSlip(407) !== undefined ? `${getObjectSlip(407).Nombre_barco.substring(0,12)} ${getObjectSlip(407).Embarcacion !== 0 ? getObjectSlip(407).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(407, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(407)}>{getTamañoEmbarcacion(407, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb406"
            x={PosicionX_Slips400Der}
            y="159.598"
            display="inline"
          >
            <tspan 
            y="161.598"
            className={getColorEnBaseFactura(406, rightPosition)}
            >•</tspan>
            <tspan 
            id="tspan9351-0-3-28-4" 
            x={PosicionX_Slips400Der} 
            y="159.598"
            // className={getObjectSlip(406) !== undefined ? getObjectSlip(406).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(406, 'right')}
            >
              {/* EMBARCACION 406 */}
              {/* {`${getObjectSlip(406) !== undefined ? `${getObjectSlip(406).Nombre_barco.substring(0,12)} ${getObjectSlip(406).Embarcacion !== 0 ? getObjectSlip(406).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(406, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(406)}>{getTamañoEmbarcacion(406, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb405"
            x={PosicionX_Slips400Izq}
            y="159.598"
            display="inline"
          >
            <tspan 
            y="161.598"
            className={getColorEnBaseFactura(405, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE405" 
            x={PosicionX_Slips400Izq} 
            y="159.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(405) !== undefined ? getObjectSlip(405).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(405, 'left')}
            >
              {/* EMBARCACION 405 */}
              {/* {`${getObjectSlip(405) !== undefined ? `${getObjectSlip(405).Nombre_barco.substring(0,12)} ${getObjectSlip(405).Embarcacion !== 0 ? getObjectSlip(405).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(405, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(405)}>{getTamañoEmbarcacion(405, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb404"
            x={PosicionX_Slips400Der}
            y="149.598"
            display="inline"
          >
            <tspan 
            y="151.598"
            className={getColorEnBaseFactura(404, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE404" 
            x={PosicionX_Slips400Der} 
            y="149.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(404) !== undefined ? getObjectSlip(404).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={getNombreClaseCSS(404, 'right')}
            >
              {/* EMBARCACION 404 */}
              {/* {`${getObjectSlip(404) !== undefined ? `${getObjectSlip(404).Nombre_barco.substring(0,12)} ${getObjectSlip(404).Embarcacion !== 0 ? getObjectSlip(404).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(404, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(404)}>{getTamañoEmbarcacion(404, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb403"
            x={PosicionX_Slips400Izq}
            y="149.598"
            display="inline"
          >
            <tspan 
            y="151.598"
            className={getColorEnBaseFactura(403, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE403" 
            x={PosicionX_Slips400Izq} 
            y="149.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(403) !== undefined ? getObjectSlip(403).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(403, 'left')}
            >
              {/* EMBARCACION 403 */}
              {/* {`${getObjectSlip(403) !== undefined ? `${getObjectSlip(403).Nombre_barco.substring(0,12)} ${getObjectSlip(403).Embarcacion !== 0 ? getObjectSlip(403).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(403, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(403)}>{getTamañoEmbarcacion(403, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb402"
            x={PosicionX_Slips400Der}
            y="138.598"
            display="inline"
          >
            <tspan 
            y="140.598"
            className={getColorEnBaseFactura(402, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE402" 
            x={PosicionX_Slips400Der} 
            y="138.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(402) !== undefined ? getObjectSlip(402).Embarcacion !== 0 ? "text-color-normal" : getObjectSlip(402).Compartido === "S" ? "text-color-compartido" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(402, 'right')}
            >
              {/* EMBARCACION 402 */}
              {/* {`${getObjectSlip(402) !== undefined ? `${getObjectSlip(402).Nombre_barco.substring(0,12)} ${getObjectSlip(402).Embarcacion !== 0 ? getObjectSlip(402).Embarcacion : getObjectSlip(402).Compartido === "S" ? getObjectSlip(402).Observacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(402, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(402)}>{getTamañoEmbarcacion(402, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb401"
            x={PosicionX_Slips400Izq}
            y="138.598"
            display="inline"
          >
            <tspan 
            y="140.598"
            className={getColorEnBaseFactura(401, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE401" 
            x={PosicionX_Slips400Izq} 
            y="138.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(401) !== undefined ? getObjectSlip(401).Embarcacion !== 0 ? "text-color-normal" : getObjectSlip(401).Compartido === "S" ? "text-color-compartido" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(401,'left')}
            >
              {/* EMBARCACION 401 */}
              {/* {`${getObjectSlip(401) !== undefined ? `${getObjectSlip(401).Nombre_barco.substring(0,12)} ${getObjectSlip(401).Embarcacion !== 0 ? getObjectSlip(401).Embarcacion : getObjectSlip(401).Compartido === "S" ? getObjectSlip(401).Observacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(401, leftPosition)}
             </tspan>
             <tspan className={getColorLongitudCSS(401)}>{getTamañoEmbarcacion(401, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb344"
            x={PosicionX_Slips300Der}
            y="318.598"
            display="inline"
          >
            <tspan 
            y="320.598"
            className={getColorEnBaseFactura(344, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE344" 
            x={PosicionX_Slips300Der} 
            y="318.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(344) !== undefined ? getObjectSlip(344).Embarcacion !== 0 ? "text-color-normal slips-names-align-right" : "text-color-libre slips-names-align-right" : "text-color-normal slips-names-align-right"}
            className={ getNombreClaseCSS( 344, 'right' ) }
            >
              {/* EMBARCACION 344 */}
              {/* {`${getObjectSlip(344) !== undefined ? `${getObjectSlip(344).Nombre_barco.substring(0,12)} ${getObjectSlip(344).Embarcacion !== 0 ? getObjectSlip(344).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 344, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(344)}>{getTamañoEmbarcacion(344, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb343"
            x={PosicionX_Slips300Izq}
            y="318.598"
            display="inline"
          >
            <tspan 
            y="320.598"
            className={getColorEnBaseFactura(343, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE343" 
            x={PosicionX_Slips300Izq} 
            y="318.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(343) !== undefined ? getObjectSlip(343).Embarcacion !== 0 ? "text-color-normal slips-names-align-left" : "text-color-libre slips-names-align-left" : "text-color-normal slips-names-align-left"}
            className={getNombreClaseCSS(343, 'left')}
            >
              {/* EMBARCACION 343 */}
              {/* {`${getObjectSlip(343) !== undefined ? `${getObjectSlip(343).Nombre_barco.substring(0,12)} ${getObjectSlip(343).Embarcacion !== 0 ? getObjectSlip(343).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${ getNombreEmbarcacion(343, leftPosition) }`}
            </tspan>
            <tspan className={getColorLongitudCSS(343)}>{getTamañoEmbarcacion(343, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb342"
            x={PosicionX_Slips300Der} 
            y="307.598"
            display="inline"
          >
            <tspan 
            y="309.598"
            className={getColorEnBaseFactura(342, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE342" 
            x={PosicionX_Slips300Der}  
            y="307.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(342) !== undefined ? getObjectSlip(342).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(342, 'right')}
            >
              {/* EMBARCACION 342 */}
              {/* {`${getObjectSlip(342) !== undefined ? `${getObjectSlip(342).Nombre_barco.substring(0,12)} ${getObjectSlip(342).Embarcacion !== 0 ? getObjectSlip(342).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(342, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(342)}>{getTamañoEmbarcacion(342, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb341"
            x={PosicionX_Slips300Izq}
            y="307.598"
            display="inline"
          >
            <tspan 
            y="309.598"
            className={getColorEnBaseFactura(341, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE341" 
            x={PosicionX_Slips300Izq} 
            y="307.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(341) !== undefined ? getObjectSlip(341).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(341, 'left')}            >
              {/* EMBARCACION 341 */}
              {/* {`${getObjectSlip(341) !== undefined ? `${getObjectSlip(341).Nombre_barco.substring(0,12)} ${getObjectSlip(341).Embarcacion !== 0 ? getObjectSlip(341).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 341, leftPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(341)}>{getTamañoEmbarcacion(341, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb340"
            x={PosicionX_Slips300Der}
            y="298.598"
            display="inline"
          >
            <tspan 
            y="300.598"
            className={getColorEnBaseFactura(340, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE340" 
            x={PosicionX_Slips300Der} 
            y="298.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(340) !== undefined ? getObjectSlip(340).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(340, 'right')}
            >
              {/* EMBARCACION 340 */}
              {/* {`${getObjectSlip(340) !== undefined ? `${getObjectSlip(340).Nombre_barco.substring(0,12)} ${getObjectSlip(340).Embarcacion !== 0 ? getObjectSlip(340).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(340, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(340)}>{getTamañoEmbarcacion(340, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb339"
            x={PosicionX_Slips300Izq}
            y="298.598"
            display="inline"
          >
            <tspan 
            y="300.598"
            className={getColorEnBaseFactura(339, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE339" 
            x={PosicionX_Slips300Izq} 
            y="298.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(339) !== undefined ? getObjectSlip(339).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(339, 'left')}
            >
              {/* EMBARCACION 339 */}
              {/* {`${getObjectSlip(339) !== undefined ? `${getObjectSlip(339).Nombre_barco.substring(0,12)} ${getObjectSlip(339).Embarcacion !== 0 ? getObjectSlip(339).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(339, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(339)}>{getTamañoEmbarcacion(339, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb338"
            x={PosicionX_Slips300Der}
            y="286.598"
            display="inline"
          >
            <tspan 
            y="288.598"
            className={getColorEnBaseFactura(338, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE338" 
            x={PosicionX_Slips300Der} 
            y="286.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(338) !== undefined ? getObjectSlip(338).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(338, 'right')}
            >
              {/* EMBARCACION 338 */}
              {/* {`${getObjectSlip(338) !== undefined ? `${getObjectSlip(338).Nombre_barco.substring(0,12)} ${getObjectSlip(338).Embarcacion !== 0 ? getObjectSlip(338).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(338, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(338)}>{getTamañoEmbarcacion(338, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb337"
            x={PosicionX_Slips300Izq}
            y="286.598"
            display="inline"
          >
            <tspan 
            y="288.598"
            className={getColorEnBaseFactura(337, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE337" 
            x={PosicionX_Slips300Izq} 
            y="286.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(337) !== undefined ? getObjectSlip(337).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(337, 'left')}
            >
              {/* EMBARCACION 337 */}
              {/* {`${getObjectSlip(337) !== undefined ? `${getObjectSlip(337).Nombre_barco.substring(0,12)} ${getObjectSlip(337).Embarcacion !== 0 ? getObjectSlip(337).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(337, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(337)}>{getTamañoEmbarcacion(337, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb336"
            x={PosicionX_Slips300Der} 
            y="276.598"
            display="inline"
          >
            <tspan 
            y="278.598"
            className={getColorEnBaseFactura(336, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE336" 
            x={PosicionX_Slips300Der}  
            y="276.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(336) !== undefined ? getObjectSlip(336).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(336, 'right')}
            >
              {/* EMBARCACION 336 */}
              {/* {`${getObjectSlip(336) !== undefined ? `${getObjectSlip(336).Nombre_barco.substring(0,12)} ${getObjectSlip(336).Embarcacion !== 0 ? getObjectSlip(336).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 336, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(336)}>{getTamañoEmbarcacion(336, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb335"
            x={PosicionX_Slips300Izq}
            y="276.598"
            display="inline"
          >
            <tspan 
            y="278.598"
            className={getColorEnBaseFactura(335, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE335" 
            x={PosicionX_Slips300Izq} 
            y="276.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(335) !== undefined ? getObjectSlip(335).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(335, 'left')}
            >
              {/* EMBARCACION 335 */}
              {/* {`${getObjectSlip(335) !== undefined ? `${getObjectSlip(335).Nombre_barco.substring(0,12)} ${getObjectSlip(335).Embarcacion !== 0 ? getObjectSlip(335).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(335, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(335)}>{getTamañoEmbarcacion(335, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb334"
            x={PosicionX_Slips300Der}
            y="264.598"
            display="inline"
          >
            <tspan 
            y="266.598"
            className={getColorEnBaseFactura(334, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE334" 
            x={PosicionX_Slips300Der} 
            y="264.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(334) !== undefined ? getObjectSlip(334).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(334, 'right')}
            >
              {/* EMBARCACION 334 */}
              {/* {`${getObjectSlip(334) !== undefined ? `${getObjectSlip(334).Nombre_barco.substring(0,12)} ${getObjectSlip(334).Embarcacion !== 0 ? getObjectSlip(334).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(334, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(334)}>{getTamañoEmbarcacion(334, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TXtEmb333"
            x={PosicionX_Slips300Izq}
            y="264.598"
            display="inline"
          >
            <tspan 
            y="266.598"
            className={getColorEnBaseFactura(333, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE333" 
            x={PosicionX_Slips300Izq} 
            y="264.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(333) !== undefined ? getObjectSlip(333).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(333, 'left')}
            >
              {/* EMBARCACION 333 */}
              {/* {`${getObjectSlip(333) !== undefined ? `${getObjectSlip(333).Nombre_barco.substring(0,12)} ${getObjectSlip(333).Embarcacion !== 0 ? getObjectSlip(333).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(333, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(333)}>{getTamañoEmbarcacion(333, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb332"
            x={PosicionX_Slips300Der}
            y="255.598"
            display="inline"
          >
            <tspan 
            y="257.598"
            className={getColorEnBaseFactura(332, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE332" 
            x={PosicionX_Slips300Der} 
            y="255.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(332) !== undefined ? getObjectSlip(332).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(332, 'right')}
           >
              {/* EMBARCACION 332 */}
              {/* `${getObjectSlip(332) !== undefined ? `${getObjectSlip(332).Nombre_barco.substring(0,12)} ${getObjectSlip(332).Embarcacion !== 0 ? getObjectSlip(332).Embarcacion : "DISPONIBLE"}` : ""}` */}
              {`${getNombreEmbarcacion(332, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(332)}>{getTamañoEmbarcacion(332, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb331"
            x={PosicionX_Slips300Izq}
            y="255.598"
            display="inline"
          >
            <tspan 
            y="257.598"
            className={getColorEnBaseFactura(331, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE331" 
            x={PosicionX_Slips300Izq} 
            y="255.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(331) !== undefined ? getObjectSlip(331).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(331, 'left')}
            >
              {/* EMBARCACION 331 */}
              {/* {`${getObjectSlip(331) !== undefined ? `${getObjectSlip(331).Nombre_barco.substring(0,12)} ${getObjectSlip(331).Embarcacion !== 0 ? getObjectSlip(331).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(331, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(331)}>{getTamañoEmbarcacion(331, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb330"
            x={PosicionX_Slips300Der}
            y="243.598"
            display="inline"
          >
            <tspan 
            y="245.598"
            className={getColorEnBaseFactura(330, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE330" 
            x={PosicionX_Slips300Der} 
            y="243.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(330) !== undefined ? getObjectSlip(330).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(330, 'right')}
            >
              {/* EMBARCACION 330 */}
              {/* {`${getObjectSlip(330) !== undefined ? `${getObjectSlip(330).Nombre_barco.substring(0,12)} ${getObjectSlip(330).Embarcacion !== 0 ? getObjectSlip(330).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(330, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(330)}>{getTamañoEmbarcacion(330, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb329"
            x={PosicionX_Slips300Izq}
            y="243.598"
            display="inline"
          >
            <tspan 
            y="245.598"
            className={getColorEnBaseFactura(329, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE329" 
            x={PosicionX_Slips300Izq} 
            y="243.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(329) !== undefined ? getObjectSlip(329).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(329, 'left')}
            >
              {/* EMBARCACION 329 */}
              {/* {`${getObjectSlip(329) !== undefined ? `${getObjectSlip(329).Nombre_barco.substring(0,12)} ${getObjectSlip(329).Embarcacion !== 0 ? getObjectSlip(329).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(329, leftPosition)}`} 
            </tspan>
            <tspan className={getColorLongitudCSS(329)}>{getTamañoEmbarcacion(329, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb328"
            x={PosicionX_Slips300Der}
            y="234.598"
            display="inline"
          >
            <tspan 
            y="236.598"
            className={getColorEnBaseFactura(328, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE328" 
            x={PosicionX_Slips300Der} 
            y="234.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(328) !== undefined ? getObjectSlip(328).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(328, 'right')}
            >
              {/* EMBARCACION 328 */}
              {/* {`${getObjectSlip(328) !== undefined ? `${getObjectSlip(328).Nombre_barco.substring(0,12)} ${getObjectSlip(328).Embarcacion !== 0 ? getObjectSlip(328).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 328, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(328)}>{getTamañoEmbarcacion(328, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb327"
            x={PosicionX_Slips300Izq}
            y="234.598"
            display="inline"
          >
            <tspan 
            y="236.598"
            className={getColorEnBaseFactura(327, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE327" 
            x={PosicionX_Slips300Izq} 
            y="234.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(327) !== undefined ? getObjectSlip(327).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(327, 'left')}
            >
              {/* EMBARCACION 327 */}
              {/* {`${getObjectSlip(327) !== undefined ? `${getObjectSlip(327).Nombre_barco.substring(0,12)} ${getObjectSlip(327).Embarcacion !== 0 ? getObjectSlip(327).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(327, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(327)}>{getTamañoEmbarcacion(327, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb326"
            x={PosicionX_Slips300Der}
            y="221.598"
            display="inline"
          >
            <tspan 
            y="223.598"
            className={getColorEnBaseFactura(326, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE326" 
            x={PosicionX_Slips300Der} 
            y="221.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(326) !== undefined ? getObjectSlip(326).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(326, 'right')}
            >
              {/* EMBARCACION 326 */}
              {/* {`${getObjectSlip(326) !== undefined ? `${getObjectSlip(326).Nombre_barco.substring(0,12)} ${getObjectSlip(326).Embarcacion !== 0 ? getObjectSlip(326).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(326, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(326)}>{getTamañoEmbarcacion(326, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb325"
            x={PosicionX_Slips300Izq}
            y="221.598"
            display="inline"
          >
            <tspan 
            y="223.598"
            className={getColorEnBaseFactura(325, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE325" 
            x={PosicionX_Slips300Izq} 
            y="221.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(325) !== undefined ? getObjectSlip(325).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(325, 'left')}
            >
              {/* EMBARCACION 325 */}
              {/* {`${getObjectSlip(325) !== undefined ? `${getObjectSlip(325).Nombre_barco.substring(0,12)} ${getObjectSlip(325).Embarcacion !== 0 ? getObjectSlip(325).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(325, leftPosition)}`} 
            </tspan>
            <tspan className={getColorLongitudCSS(325)}>{getTamañoEmbarcacion(325, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb324"
            x={PosicionX_Slips300Der}
            y="212.598"
            display="inline"
          >
            <tspan 
            y="214.598"
            className={getColorEnBaseFactura(324, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE324" 
            x={PosicionX_Slips300Der} 
            y="212.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(324) !== undefined ? getObjectSlip(324).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(324, 'right')}
            >
              {/* EMBARCACION 324 */}
              {/* {`${getObjectSlip(324) !== undefined ? `${getObjectSlip(324).Nombre_barco.substring(0,12)} ${getObjectSlip(324).Embarcacion !== 0 ? getObjectSlip(324).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(324, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(324)}>{getTamañoEmbarcacion(324, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb323"
            x={PosicionX_Slips300Izq}
            y="212.598"
            display="inline"
          >
            <tspan 
            y="214.598"
            className={getColorEnBaseFactura(323, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE323" 
            x={PosicionX_Slips300Izq} 
            y="212.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(323) !== undefined ? getObjectSlip(323).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(323, 'left')}
            >
              {/* EMBARCACION 323 */}
              {/* {`${getObjectSlip(323) !== undefined ? `${getObjectSlip(323).Nombre_barco.substring(0,12)} ${getObjectSlip(323).Embarcacion !== 0 ? getObjectSlip(323).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(323, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(323)}>{getTamañoEmbarcacion(323, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb322"
            x={PosicionX_Slips300Der}
            y="201.598"
            display="inline"
          >
            <tspan 
            y="203.598"
            className={getColorEnBaseFactura(322, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE322" 
            x={PosicionX_Slips300Der} 
            y="201.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(322) !== undefined ? getObjectSlip(322).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(322, 'right')}
            >
              {/* EMBARCACION 322 */}
              {/* {`${getObjectSlip(322) !== undefined ? `${getObjectSlip(322).Nombre_barco.substring(0,12)} ${getObjectSlip(322).Embarcacion !== 0 ? getObjectSlip(322).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion( 322, rightPosition )}`}
            </tspan>
            <tspan className={getColorLongitudCSS(322)}>{getTamañoEmbarcacion(322, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb321"
            x={PosicionX_Slips300Izq}
            y="201.598"
            display="inline"
          >
            <tspan 
            y="203.598"
            className={getColorEnBaseFactura(321, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE321" 
            x={PosicionX_Slips300Izq} 
            y="201.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(321) !== undefined ? getObjectSlip(321).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(321, 'left')}
            >
              {/* EMBARCACION 321 */}
              {/* {`${getObjectSlip(321) !== undefined ? `${getObjectSlip(321).Nombre_barco.substring(0,12)} ${getObjectSlip(321).Embarcacion !== 0 ? getObjectSlip(321).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(321, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(321)}>{getTamañoEmbarcacion(321, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb320"
            x={PosicionX_Slips300Der}
            y="191.598"
            display="inline"
          >
            <tspan 
            y="193.598"
            className={getColorEnBaseFactura(320, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE320" 
            x={PosicionX_Slips300Der} 
            y="191.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(320) !== undefined ? getObjectSlip(320).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(320, 'right')}
            >
              {/* EMBARCACION 320 */}
              {/* {`${getObjectSlip(320) !== undefined ? `${getObjectSlip(320).Nombre_barco.substring(0,12)} ${getObjectSlip(320).Embarcacion !== 0 ? getObjectSlip(320).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(320, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(320)}>{getTamañoEmbarcacion(320, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb319"
            x={PosicionX_Slips300Izq}
            y="191.598"
            display="inline"
          >
            <tspan 
            y="193.598"
            className={getColorEnBaseFactura(319, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE319" 
            x={PosicionX_Slips300Izq} 
            y="191.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(319) !== undefined ? getObjectSlip(319).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(319, 'left')}
            >
              {/* EMBARCACION 319 */}
              {/* {`${getObjectSlip(319) !== undefined ? `${getObjectSlip(319).Nombre_barco.substring(0,12)} ${getObjectSlip(319).Embarcacion !== 0 ? getObjectSlip(319).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(319, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(319)}>{getTamañoEmbarcacion(319, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb318"
            x={PosicionX_Slips300Der}
            y="179.598"
            display="inline"
          >
            <tspan 
            y="181.598"
            className={getColorEnBaseFactura(318, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE318" 
            x={PosicionX_Slips300Der} 
            y="179.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(318) !== undefined ? getObjectSlip(318).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(318, 'right')}
            >
              {/* EMBARCACION 318 */}
              {/* {`${getObjectSlip(318) !== undefined ? `${getObjectSlip(318).Nombre_barco.substring(0,12)} ${getObjectSlip(318).Embarcacion !== 0 ? getObjectSlip(318).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(318, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(318)}>{getTamañoEmbarcacion(318, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb317"
            x={PosicionX_Slips300Izq}
            y="179.598"
            display="inline"
          >
            <tspan 
            y="181.598"
            className={getColorEnBaseFactura(317, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE317" 
            x={PosicionX_Slips300Izq} 
            y="179.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(317) !== undefined ? getObjectSlip(317).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(317, 'left')}
            >
              {/* EMBARCACION 317 */}
              {/* {`${getObjectSlip(317) !== undefined ? `${getObjectSlip(317).Nombre_barco.substring(0,12)} ${getObjectSlip(317).Embarcacion !== 0 ? getObjectSlip(317).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(317, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(317)}>{getTamañoEmbarcacion(317, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb316"
            x={PosicionX_Slips300Der}
            y="171.598"
            display="inline"
          >
            <tspan 
            y="173.598"
            className={getColorEnBaseFactura(316, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE316" 
            x={PosicionX_Slips300Der} 
            y="171.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(316) !== undefined ? getObjectSlip(316).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(316, 'right')}
            >
              {/* EMBARCACION 316 */}
              {/* {`${getObjectSlip(316) !== undefined ? `${getObjectSlip(316).Nombre_barco.substring(0,12)} ${getObjectSlip(316).Embarcacion !== 0 ? getObjectSlip(316).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(316, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(316)}>{getTamañoEmbarcacion(316, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb315"
            x={PosicionX_Slips300Izq}
            y="171.598"
            display="inline"
          >
            <tspan 
            y="173.598"
            className={getColorEnBaseFactura(315, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE315" 
            x={PosicionX_Slips300Izq} 
            y="171.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(315) !== undefined ? getObjectSlip(315).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(315, 'left')}
            >
              {/* EMBARCACION 315 */}
              {/* {`${getObjectSlip(315) !== undefined ? `${getObjectSlip(315).Nombre_barco.substring(0,12)} ${getObjectSlip(315).Embarcacion !== 0 ? getObjectSlip(315).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(315, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(315)}>{getTamañoEmbarcacion(315, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb314"
            x={PosicionX_Slips300Der}
            y="159.598"
            display="inline"
          >
            <tspan 
            y="161.598"
            className={getColorEnBaseFactura(314, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE314" 
            x={PosicionX_Slips300Der} 
            y="159.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(314) !== undefined ? getObjectSlip(314).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(314, 'right')}
            >
              {/* EMBARCACION 314 */}
              {/* {`${getObjectSlip(314) !== undefined ? `${getObjectSlip(314).Nombre_barco.substring(0,12)} ${getObjectSlip(314).Embarcacion !== 0 ? getObjectSlip(314).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(314, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(314)}>{getTamañoEmbarcacion(314, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtxEmb313"
            x={PosicionX_Slips300Izq}
            y="159.598"
            display="inline"
          >
            <tspan 
            y="161.598"
            className={getColorEnBaseFactura(313, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE313" 
            x={PosicionX_Slips300Izq} 
            y="159.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(313) !== undefined ? getObjectSlip(313).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(313, 'left')}
            >
              {/* EMBARCACION 313 */}
              {/* {`${getObjectSlip(313) !== undefined ? `${getObjectSlip(313).Nombre_barco.substring(0,12)} ${getObjectSlip(313).Embarcacion !== 0 ? getObjectSlip(313).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(313, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(313)}>{getTamañoEmbarcacion(313, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb312"
            x={PosicionX_Slips300Der}
            y="149.598"
            display="inline"
          >
            <tspan 
            y="151.598"
            className={getColorEnBaseFactura(312, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE312" 
            x={PosicionX_Slips300Der} 
            y="149.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(312) !== undefined ? getObjectSlip(312).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(312, 'right')}
            >
              {/* EMBARCACION 312 */}
              {/* {`${getObjectSlip(312) !== undefined ? `${getObjectSlip(312).Nombre_barco.substring(0,12)} ${getObjectSlip(312).Embarcacion !== 0 ? getObjectSlip(312).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(312, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(312)}>{getTamañoEmbarcacion(312, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb311"
            x={PosicionX_Slips300Izq}
            y="149.598"
            display="inline"
          >
            <tspan 
            y="151.598"
            className={getColorEnBaseFactura(311, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE311" 
            x={PosicionX_Slips300Izq} 
            y="149.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(311) !== undefined ? getObjectSlip(311).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(311, 'left')}
            >
              {/* EMBARCACION 311 */}
              {/* {`${getObjectSlip(311) !== undefined ? `${getObjectSlip(311).Nombre_barco.substring(0,12)} ${getObjectSlip(311).Embarcacion !== 0 ? getObjectSlip(311).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(311, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(311)}>{getTamañoEmbarcacion(311, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb310"
            x={PosicionX_Slips300Der}
            y="138.598"
            display="inline"
          >
            <tspan 
            y="140.598"
            className={getColorEnBaseFactura(310, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE310" 
            x={PosicionX_Slips300Der} 
            y="138.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(310) !== undefined ? getObjectSlip(310).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(310, 'right')}
            >
              {/* EMBARCACION 310 */}
              {/* {`${getObjectSlip(310) !== undefined ? `${getObjectSlip(310).Nombre_barco.substring(0,12)} ${getObjectSlip(310).Embarcacion !== 0 ? getObjectSlip(310).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(310, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(310)}>{getTamañoEmbarcacion(310, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb309"
            x={PosicionX_Slips300Izq}
            y="138.598"
            display="inline"
          >
            <tspan 
            y="140.598"
            className={getColorEnBaseFactura(309, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE309" 
            x={PosicionX_Slips300Izq} 
            y="138.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(309) !== undefined ? getObjectSlip(309).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(309, 'left')}
            >
              {/* EMBARCACION 309 */}
              {/* {`${getObjectSlip(309) !== undefined ? `${getObjectSlip(309).Nombre_barco.substring(0,12)} ${getObjectSlip(309).Embarcacion !== 0 ? getObjectSlip(309).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(309, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(309)}>{getTamañoEmbarcacion(309, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb308"
            x={PosicionX_Slips300Der}
            y="128.598"
            display="inline"
          >
            <tspan 
            y="130.598"
            className={getColorEnBaseFactura(308, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE308" 
            x={PosicionX_Slips300Der} 
            y="128.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(308) !== undefined ? getObjectSlip(308).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(308, 'right')}
            >
              {/* EMBARCACION 308 */}
              {/* {`${getObjectSlip(308) !== undefined ? `${getObjectSlip(308).Nombre_barco.substring(0,12)} ${getObjectSlip(308).Embarcacion !== 0 ? getObjectSlip(308).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(308, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(308)}>{getTamañoEmbarcacion(308, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb307"
            x={PosicionX_Slips300Izq}
            y="128.598"
            display="inline"
          >
            <tspan 
            y="130.598"
            className={getColorEnBaseFactura(307, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE307" 
            x={PosicionX_Slips300Izq} 
            y="128.598" 
            strokeWidth="0.265"
            className={getNombreClaseCSS(307, 'left')}
            // className={getObjectSlip(307) !== undefined ? getObjectSlip(307).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            >
              {/* EMBARCACION 307 */}
              {/* {`${getObjectSlip(307) !== undefined ? `${getObjectSlip(307).Nombre_barco.substring(0,12)} ${getObjectSlip(307).Embarcacion !== 0 ? getObjectSlip(307).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(307, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(307)}>{getTamañoEmbarcacion(307, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb306"
            x={PosicionX_Slips300Der}
            y="117.598"
            display="inline"
          >
            <tspan 
            y="119.598"
            className={getColorEnBaseFactura(306, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE306" 
            x={PosicionX_Slips300Der} 
            y="117.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(306) !== undefined ? getObjectSlip(306).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(306, 'right')}
            >
              {/* EMBARCACION 306 */}
              {/* {`${getObjectSlip(306) !== undefined ? `${getObjectSlip(306).Nombre_barco.substring(0,12)} ${getObjectSlip(306).Embarcacion !== 0 ? getObjectSlip(306).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(306, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(306)}>{getTamañoEmbarcacion(306, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb305"
            x={PosicionX_Slips300Izq}
            y="120.108"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="122.108"
            className={getColorEnBaseFactura(305, leftPosition)}
            >•</tspan>
            <tspan
              id="TE305"
              x={PosicionX_Slips300Izq}
              y="120.108"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(305) !== undefined ? getObjectSlip(305).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(305, 'left')}
            >
              {/* EMBARCACION 305 */}
              {/* {`${getObjectSlip(305) !== undefined ? `${getObjectSlip(305).Nombre_barco.substring(0,12)} ${getObjectSlip(305).Embarcacion !== 0 ? getObjectSlip(305).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(305, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(305)}>{getTamañoEmbarcacion(305, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb304"
            x={PosicionX_Slips300Der}
            y="108.598"
            display="inline"
          >
            <tspan 
            y="110.598"
            className={getColorEnBaseFactura(304, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE304" 
            x={PosicionX_Slips300Der} 
            y="108.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(304) !== undefined ? getObjectSlip(304).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(304, 'right')}
            >
              {/* EMBARCACION 304 */}
              {/* {`${getObjectSlip(304) !== undefined ? `${getObjectSlip(304).Nombre_barco.substring(0,12)} ${getObjectSlip(304).Embarcacion !== 0 ? getObjectSlip(304).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(304, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(304)}>{getTamañoEmbarcacion(304, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb303"
            x={PosicionX_Slips300Izq}
            y="108.598"
            display="inline"
          >
            <tspan 
            y="110.598"
            className={getColorEnBaseFactura(303, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE303" 
            x={PosicionX_Slips300Izq} 
            y="108.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(303) !== undefined ? getObjectSlip(303).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(303, 'left')}
            >
              {/* EMBARCACION 303 */}
              {/* {`${getObjectSlip(303) !== undefined ? `${getObjectSlip(303).Nombre_barco.substring(0,12)} ${getObjectSlip(303).Embarcacion !== 0 ? getObjectSlip(303).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(303, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(303)}>{getTamañoEmbarcacion(303, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb302"
            x={PosicionX_Slips300Der}
            y="96.598"
            display="inline"
          >
            <tspan 
            y="98.598"
            className={getColorEnBaseFactura(302, rightPosition)}
            >•</tspan>
            <tspan 
            id="TE302" 
            x={PosicionX_Slips300Der} 
            y="96.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(302) !== undefined ? getObjectSlip(302).Embarcacion !== 0 ? "text-color-normal" : getObjectSlip(302).Compartido === "S" ? "text-color-compartido" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(302, 'right')}
            >
              {/* EMBARCACION 302 */}
              {/* {`${getObjectSlip(302) !== undefined ? `${getObjectSlip(302).Nombre_barco.substring(0,12)} ${getObjectSlip(302).Embarcacion !== 0 ? getObjectSlip(302).Embarcacion : getObjectSlip(302).Compartido === "S" ? getObjectSlip(302).Observacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(302, rightPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(302)}>{getTamañoEmbarcacion(302, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb301"
            x={PosicionX_Slips300Izq}
            y="96.598"
            display="inline"
          >
            <tspan 
            y="97.598"
            className={getColorEnBaseFactura(301, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE301" 
            x={PosicionX_Slips300Izq} 
            y="96.598" 
            strokeWidth="0.265"
            // className={getObjectSlip(301) !== undefined ? getObjectSlip(301).Embarcacion !== 0 ? "text-color-normal" : getObjectSlip(301).Compartido === "S" ? "text-color-compartido" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(301, 'left')}
            >
              {/* EMBARCACION 301 */}
              {/* {`${getObjectSlip(301) !== undefined ? `${getObjectSlip(301).Nombre_barco.substring(0,12)} ${getObjectSlip(301).Embarcacion !== 0 ? getObjectSlip(301).Embarcacion : getObjectSlip(301).Compartido === "S" ? getObjectSlip(301).Observacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(301, leftPosition)}
            </tspan>
            <tspan className={getColorLongitudCSS(301)}>{getTamañoEmbarcacion(301, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb236"
            x={PosicionX_Slips200Der}
            y="322.434"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="324.434"
            className={getColorEnBaseFactura(236, rightPosition)}
            >•</tspan>
            <tspan
              id="TE236"
              x={PosicionX_Slips200Der}
              y="322.434"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(236) !== undefined ? getObjectSlip(236).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(236, 'right')}
            >
              {/* EMBARCACION 236 */}
              {/* {`${getObjectSlip(236) !== undefined ? `${getObjectSlip(236).Nombre_barco.substring(0,12)} ${getObjectSlip(236).Embarcacion !== 0 ? getObjectSlip(236).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(236, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(236)}>{getTamañoEmbarcacion(236, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb235"
            x={PosicionX_Slips200Izq}
            y="321.945"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="323.945"
            className={getColorEnBaseFactura(235, leftPosition)}
            >•</tspan>
            <tspan
              id="TE235"
              x={PosicionX_Slips200Izq}
              y="321.945"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(235) !== undefined ? getObjectSlip(235).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(235, 'left')}
            >
              {/* EMBARCACION 235 */}
              {/* {`${getObjectSlip(235) !== undefined ? `${getObjectSlip(235).Nombre_barco.substring(0,12)} ${getObjectSlip(235).Embarcacion !== 0 ? getObjectSlip(235).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(235, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(235)}>{getTamañoEmbarcacion(235, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb234"
            x={PosicionX_Slips200Der}
            y="311.42"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="313.42"
            className={getColorEnBaseFactura(234, rightPosition)}
            >•</tspan>
            <tspan
              id="TE234"
              x={PosicionX_Slips200Der}
              y="311.42"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(234) !== undefined ? getObjectSlip(234).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(234, 'right')}
            >
              {/* EMBARCACION 234 */}
              {/* {`${getObjectSlip(234) !== undefined ? `${getObjectSlip(234).Nombre_barco.substring(0,12)} ${getObjectSlip(234).Embarcacion !== 0 ? getObjectSlip(234).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(234, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(234)}>{getTamañoEmbarcacion(234, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb233"
            x={PosicionX_Slips200Izq}
            y="310.963"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="312.963"
            className={getColorEnBaseFactura(233, leftPosition)}
            >•</tspan>
            <tspan
              id="TE233"
              x={PosicionX_Slips200Izq}
              y="310.963"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(233) !== undefined ? getObjectSlip(233).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(233, 'left')}
            >
              {/* EMBARCACION 233 */}
              {/* {`${getObjectSlip(233) !== undefined ? `${getObjectSlip(233).Nombre_barco.substring(0,12)} ${getObjectSlip(233).Embarcacion !== 0 ? getObjectSlip(233).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(233, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(233)}>{getTamañoEmbarcacion(233, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb232"
            x={PosicionX_Slips200Der}
            y="302.434"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="304.434"
            className={getColorEnBaseFactura(232, rightPosition)}
            >•</tspan>
            <tspan
              id="TE232"
              x={PosicionX_Slips200Der}
              y="302.434"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(232) !== undefined ? getObjectSlip(232).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(232, 'right')}
            >
              {/* EMBARCACION 232 */}
              {/* {`${getObjectSlip(232) !== undefined ? `${getObjectSlip(232).Nombre_barco.substring(0,12)} ${getObjectSlip(232).Embarcacion !== 0 ? getObjectSlip(232).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(232, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(232)}>{getTamañoEmbarcacion(232, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb231"
            x={PosicionX_Slips200Izq}
            y="301.959"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="303.959"
            className={getColorEnBaseFactura(231, leftPosition)}
            >•</tspan>
            <tspan
              id="TE231"
              x={PosicionX_Slips200Izq}
              y="301.959"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(231) !== undefined ? getObjectSlip(231).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(231, 'left')}
            >
              {/* EMBARCACION 231 */}
              {/* {`${getObjectSlip(231) !== undefined ? `${getObjectSlip(231).Nombre_barco.substring(0,12)} ${getObjectSlip(231).Embarcacion !== 0 ? getObjectSlip(231).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(231, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(231)}>{getTamañoEmbarcacion(231, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb230"
            x={PosicionX_Slips200Der}
            y="290.435"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="292.435"
            className={getColorEnBaseFactura(230, rightPosition)}
            >•</tspan>
            <tspan
              id="TE230"
              x={PosicionX_Slips200Der}
              y="290.435"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(230) !== undefined ? getObjectSlip(230).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(230, 'right')}
            >
              {/* EMBARCACION 230 */}
              {/* {`${getObjectSlip(230) !== undefined ? `${getObjectSlip(230).Nombre_barco.substring(0,12)} ${getObjectSlip(230).Embarcacion !== 0 ? getObjectSlip(230).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(230, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(230)}>{getTamañoEmbarcacion(230, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb229"
            x={PosicionX_Slips200Izq}
            y="289.963"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="291.963"
            className={getColorEnBaseFactura(229, leftPosition)}
            >•</tspan>
            <tspan
              id="TE229"
              x={PosicionX_Slips200Izq}
              y="289.963"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(229) !== undefined ? getObjectSlip(229).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(229, 'left')}
            >
              {/* EMBARCACION 229 */}
              {/* {`${getObjectSlip(229) !== undefined ? `${getObjectSlip(229).Nombre_barco.substring(0,12)} ${getObjectSlip(229).Embarcacion !== 0 ? getObjectSlip(229).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(229, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(229)}>{getTamañoEmbarcacion(229, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb228"
            x={PosicionX_Slips200Der}
            y="280.435"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="282.435"
            className={getColorEnBaseFactura(228, rightPosition)}
            >•</tspan>
            <tspan
              id="TE228"
              x={PosicionX_Slips200Der}
              y="280.435"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(228) !== undefined ? getObjectSlip(228).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(228, 'right')}
            >
              {/* EMBARCACION 228 */}
              {/* {`${getObjectSlip(228) !== undefined ? `${getObjectSlip(228).Nombre_barco.substring(0,12)} ${getObjectSlip(228).Embarcacion !== 0 ? getObjectSlip(228).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(228, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(228)}>{getTamañoEmbarcacion(228, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb227"
            x={PosicionX_Slips200Izq}
            y="279.946"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="281.946"
            className={getColorEnBaseFactura(227, leftPosition)}
            >•</tspan>
            <tspan
              id="TE227"
              x={PosicionX_Slips200Izq}
              y="279.946"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(227) !== undefined ? getObjectSlip(227).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(227, 'left')}
            >
              {/* EMBARCACION 227 */}
              {/* {`${getObjectSlip(227) !== undefined ? `${getObjectSlip(227).Nombre_barco.substring(0,12)} ${getObjectSlip(227).Embarcacion !== 0 ? getObjectSlip(227).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(227, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(227)}>{getTamañoEmbarcacion(227, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb226"
            x={PosicionX_Slips200Der}
            y="268.435"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="270.435"
            className={getColorEnBaseFactura(226, rightPosition)}
            >•</tspan>
            <tspan
              id="TE226"
              x={PosicionX_Slips200Der}
              y="268.435"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(226) !== undefined ? getObjectSlip(226).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(226, 'right')}
            >
              {/* EMBARCACION 226 */}
              {/* {`${getObjectSlip(226) !== undefined ? `${getObjectSlip(226).Nombre_barco.substring(0,12)} ${getObjectSlip(226).Embarcacion !== 0 ? getObjectSlip(226).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(226, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(226)}>{getTamañoEmbarcacion(226, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb225"
            x={PosicionX_Slips200Izq}
            y="267.946"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="269.946"
            className={getColorEnBaseFactura(225, leftPosition)}
            >•</tspan>
            <tspan
              id="TE225"
              x={PosicionX_Slips200Izq}
              y="267.946"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(225) !== undefined ? getObjectSlip(225).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(225, 'left')}
            >
              {/* EMBARCACION 225 */}
              {/* {`${getObjectSlip(225) !== undefined ? `${getObjectSlip(225).Nombre_barco.substring(0,12)} ${getObjectSlip(225).Embarcacion !== 0 ? getObjectSlip(225).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(225, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(225)}>{getTamañoEmbarcacion(225, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb224"
            x={PosicionX_Slips200Der}
            y="259.421"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="261.421"
            className={getColorEnBaseFactura(224, rightPosition)}
            >•</tspan>
            <tspan
              id="TE224"
              x={PosicionX_Slips200Der}
              y="259.421"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(224) !== undefined ? getObjectSlip(224).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(224, 'right')}
            >
              {/* EMBARCACION 224 */}
              {/* {`${getObjectSlip(224) !== undefined ? `${getObjectSlip(224).Nombre_barco.substring(0,12)} ${getObjectSlip(224).Embarcacion !== 0 ? getObjectSlip(224).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(224, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(224)}>{getTamañoEmbarcacion(224, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb223"
            x={PosicionX_Slips200Izq}
            y="258.964"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="260.964"
            className={getColorEnBaseFactura(223, leftPosition)}
            >•</tspan>
            <tspan
              id="TE223"
              x={PosicionX_Slips200Izq}
              y="258.964"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(223) !== undefined ? getObjectSlip(223).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(223, 'left')}
            >
              {/* EMBARCACION 223 */}
              {/* {`${getObjectSlip(223) !== undefined ? `${getObjectSlip(223).Nombre_barco.substring(0,12)} ${getObjectSlip(223).Embarcacion !== 0 ? getObjectSlip(223).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(223, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(223)}>{getTamañoEmbarcacion(223, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb222"
            x={PosicionX_Slips200Der}
            y="247.435"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="249.435"
            className={getColorEnBaseFactura(222, rightPosition)}
            >•</tspan>
            <tspan
              id="TE222"
              x={PosicionX_Slips200Der}
              y="247.435"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(222) !== undefined ? getObjectSlip(222).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(222, 'right')}
            >
              {/* EMBARCACION 222 */}
              {/* {`${getObjectSlip(222) !== undefined ? `${getObjectSlip(222).Nombre_barco.substring(0,12)} ${getObjectSlip(222).Embarcacion !== 0 ? getObjectSlip(222).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(222, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(222)}>{getTamañoEmbarcacion(222, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb221"
            x={PosicionX_Slips200Izq}
            y="246.96"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="248.96"
            className={getColorEnBaseFactura(221, leftPosition)}
            >•</tspan>
            <tspan
              id="TE221"
              x={PosicionX_Slips200Izq}
              y="246.96"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(221) !== undefined ? getObjectSlip(221).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(221, 'left')}
            >
              {/* EMBARCACION 221 */}
              {/* {`${getObjectSlip(221) !== undefined ? `${getObjectSlip(221).Nombre_barco.substring(0,12)} ${getObjectSlip(221).Embarcacion !== 0 ? getObjectSlip(221).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(221, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(221)}>{getTamañoEmbarcacion(221, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb220"
            x={PosicionX_Slips200Der}
            y="238.436"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="240.436"
            className={getColorEnBaseFactura(220, rightPosition)}
            >•</tspan>
            <tspan
              id="TE220"
              x={PosicionX_Slips200Der}
              y="238.436"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(220) !== undefined ? getObjectSlip(220).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(220, 'right')}
            >
              {/* EMBARCACION 220 */}
              {/* {`${getObjectSlip(220) !== undefined ? `${getObjectSlip(220).Nombre_barco.substring(0,12)} ${getObjectSlip(220).Embarcacion !== 0 ? getObjectSlip(220).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(220, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(220)}>{getTamañoEmbarcacion(220, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb219"
            x={PosicionX_Slips200Izq}
            y="237.964"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="239.964"
            className={getColorEnBaseFactura(219, leftPosition)}
            >•</tspan>
            <tspan
              id="TE219"
              x={PosicionX_Slips200Izq}
              y="237.964"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(219) !== undefined ? getObjectSlip(219).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(219, 'left')}
            >
              {/* EMBARCACION 219 */}
              {/* {`${getObjectSlip(219) !== undefined ? `${getObjectSlip(219).Nombre_barco.substring(0,12)} ${getObjectSlip(219).Embarcacion !== 0 ? getObjectSlip(219).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(219, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(219)}>{getTamañoEmbarcacion(219, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb218"
            x={PosicionX_Slips200Der}
            y="225.436"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="227.436"
            className={getColorEnBaseFactura(218, rightPosition)}
            >•</tspan>
            <tspan
              id="TE218"
              x={PosicionX_Slips200Der}
              y="225.436"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(218) !== undefined ? getObjectSlip(218).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(218, 'right')}
            >
              {/* EMBARCACION 218 */}
              {/* {`${getObjectSlip(218) !== undefined ? `${getObjectSlip(218).Nombre_barco.substring(0,12)} ${getObjectSlip(218).Embarcacion !== 0 ? getObjectSlip(218).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(218, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(218)}>{getTamañoEmbarcacion(218, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb217"
            x={PosicionX_Slips200Izq}
            y="224.943"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="226.943"
            className={getColorEnBaseFactura(217, leftPosition)}
            >•</tspan>
            <tspan
              id="TE217"
              x={PosicionX_Slips200Izq}
              y="224.943"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(217) !== undefined ? getObjectSlip(217).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(217, 'left')}
            >
              {/* EMBARCACION 217 */}
              {/* {`${getObjectSlip(217) !== undefined ? `${getObjectSlip(217).Nombre_barco.substring(0,12)} ${getObjectSlip(217).Embarcacion !== 0 ? getObjectSlip(217).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(217, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(217)}>{getTamañoEmbarcacion(217, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb216"
            x={PosicionX_Slips200Der}
            y="216.436"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="218.436"
            className={getColorEnBaseFactura(216, rightPosition)}
            >•</tspan>
            <tspan
              id="TE216"
              x={PosicionX_Slips200Der}
              y="216.436"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(216) !== undefined ? getObjectSlip(216).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(216, 'right')}
            >
              {/* EMBARCACION 216 */}
              {/* {`${getObjectSlip(216) !== undefined ? `${getObjectSlip(216).Nombre_barco.substring(0,12)} ${getObjectSlip(216).Embarcacion !== 0 ? getObjectSlip(216).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(216, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(216)}>{getTamañoEmbarcacion(216, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb215"
            x={PosicionX_Slips200Izq}
            y="215.943"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="215.943"
            className={getColorEnBaseFactura(215, leftPosition)}
            >•</tspan>
            <tspan
              id="TE215"
              x={PosicionX_Slips200Izq}
              y="215.943"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(215) !== undefined ? getObjectSlip(215).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(215, 'left')}
            >
              {/* EMBARCACION 215 */}
              {/* {`${getObjectSlip(215) !== undefined ? `${getObjectSlip(215).Nombre_barco.substring(0,12)} ${getObjectSlip(215).Embarcacion !== 0 ? getObjectSlip(215).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(215, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(215)}>{getTamañoEmbarcacion(215, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb214"
            x={PosicionX_Slips200Der}
            y="205.422"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="207.422"
            className={getColorEnBaseFactura(214, rightPosition)}
            >•</tspan>
            <tspan
              id="TE214"
              x={PosicionX_Slips200Der}
              y="205.422"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(214) !== undefined ? getObjectSlip(214).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(214, 'right')}
            >
              {/* EMBARCACION 214 */}
              {/* {`${getObjectSlip(214) !== undefined ? `${getObjectSlip(214).Nombre_barco.substring(0,12)} ${getObjectSlip(214).Embarcacion !== 0 ? getObjectSlip(214).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(214, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(214)}>{getTamañoEmbarcacion(214, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb213"
            x={PosicionX_Slips200Izq}
            y="204.965"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="206.965"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(213, leftPosition)}
            >•</tspan>
            <tspan
              id="TE213"
              x={PosicionX_Slips200Izq}
              y="204.965"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(213) !== undefined ? getObjectSlip(213).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(213, 'left')}
            >
              {/* EMBARCACION 213 */}
              {/* {`${getObjectSlip(213) !== undefined ? `${getObjectSlip(213).Nombre_barco.substring(0,12)} ${getObjectSlip(213).Embarcacion !== 0 ? getObjectSlip(213).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(213, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(213)}>{getTamañoEmbarcacion(213, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb212"
            x={PosicionX_Slips200Der}
            y="195.436"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="197.436"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(212, rightPosition)}
            >•</tspan>
            <tspan
              id="TE212"
              x={PosicionX_Slips200Der}
              y="195.436"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(212) !== undefined ? getObjectSlip(212).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(212, 'right')}
            >
              {/* EMBARCACION 212 */}
              {/* {`${getObjectSlip(212) !== undefined ? `${getObjectSlip(212).Nombre_barco.substring(0,12)} ${getObjectSlip(212).Embarcacion !== 0 ? getObjectSlip(212).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(212, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(212)}>{getTamañoEmbarcacion(212, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb211"
            x={PosicionX_Slips200Izq}
            y="194.959"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="196.959"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(211, leftPosition)}
            >•</tspan>
            <tspan
              id="TE211"
              x={PosicionX_Slips200Izq}
              y="194.959"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(211) !== undefined ? getObjectSlip(211).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(211, 'left')}
            >
              {/* EMBARCACION 211 */}
              {/* {`${getObjectSlip(211) !== undefined ? `${getObjectSlip(211).Nombre_barco.substring(0,12)} ${getObjectSlip(211).Embarcacion !== 0 ? getObjectSlip(211).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(211, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(211)}>{getTamañoEmbarcacion(211, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb210"
            x={PosicionX_Slips200Der}
            y="183.437"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="185.437"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(210, rightPosition)}
            >•</tspan>
            <tspan
              id="TE210"
              x={PosicionX_Slips200Der}
              y="183.437"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(210) !== undefined ? getObjectSlip(210).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(210, 'right')}
            >
              {/* EMBARCACION 210 */}
              {/* {`${getObjectSlip(210) !== undefined ? `${getObjectSlip(210).Nombre_barco.substring(0,12)} ${getObjectSlip(210).Embarcacion !== 0 ? getObjectSlip(210).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(210, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(210)}>{getTamañoEmbarcacion(210, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb209"
            x={PosicionX_Slips200Izq}
            y="179.598"
            display="inline"
          >
            <tspan 
            y="181.598"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(209, leftPosition)}
            >•</tspan>
            <tspan 
            id="TE209" 
            x={PosicionX_Slips200Izq} 
            y="179.598" 
            strokeWidth="0.265" 
            // className={getObjectSlip(209) !== undefined ? getObjectSlip(209).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(209, 'left')}
            >
              {/* EMBARCACION 209 */}
              {/* {`${getObjectSlip(209) !== undefined ? `${getObjectSlip(209).Nombre_barco.substring(0,12)} ${getObjectSlip(209).Embarcacion !== 0 ? getObjectSlip(209).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(209, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(209)}>{getTamañoEmbarcacion(209, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb208"
            x={PosicionX_Slips200Der}
            y="175.437"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="177.437"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(208, rightPosition)}
            >•</tspan>
            <tspan
              id="TE208"
              x={PosicionX_Slips200Der}
              y="175.437"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(208) !== undefined ? getObjectSlip(208).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(208, 'right')}
            >
              {/* EMBARCACION 208 */}
              {/* {`${getObjectSlip(208) !== undefined ? `${getObjectSlip(208).Nombre_barco.substring(0,12)} ${getObjectSlip(208).Embarcacion !== 0 ? getObjectSlip(208).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(208, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(208)}>{getTamañoEmbarcacion(208, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb207"
            x={PosicionX_Slips200Izq}
            y="174.948"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="176.948"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(207, leftPosition)}
            >•</tspan>
            <tspan
              id="TE207"
              x={PosicionX_Slips200Izq}
              y="174.948"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(207) !== undefined ? getObjectSlip(207).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(207, 'left')}
            >
              {/* EMBARCACION 207 */}
              {/* {`${getObjectSlip(207) !== undefined ? `${getObjectSlip(207).Nombre_barco.substring(0,12)} ${getObjectSlip(207).Embarcacion !== 0 ? getObjectSlip(207).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(207, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(207)}>{getTamañoEmbarcacion(207, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb206"
            x={PosicionX_Slips200Der}
            y="163.437"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="165.437"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(206, rightPosition)}
            >•</tspan>
            <tspan
              id="TE206"
              x={PosicionX_Slips200Der}
              y="163.437"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(206) !== undefined ? getObjectSlip(206).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(206, 'right')}
            >
              {/* EMBARCACION 206 */}
              {/* {`${getObjectSlip(206) !== undefined ? `${getObjectSlip(206).Nombre_barco.substring(0,12)} ${getObjectSlip(206).Embarcacion !== 0 ? getObjectSlip(206).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(206, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(206)}>{getTamañoEmbarcacion(206, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb205"
            x={PosicionX_Slips200Izq}
            y="162.948"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="164.948"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(205, leftPosition)}
            >•</tspan>
            <tspan
              id="TE205"
              x={PosicionX_Slips200Izq}
              y="162.948"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(205) !== undefined ? getObjectSlip(205).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(205, 'left')}
            >
              {/* EMBARCACION 205 */}
              {/* {`${getObjectSlip(205) !== undefined ? `${getObjectSlip(205).Nombre_barco.substring(0,12)} ${getObjectSlip(205).Embarcacion !== 0 ? getObjectSlip(205).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(205, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(205)}>{getTamañoEmbarcacion(205, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb204"
            x={PosicionX_Slips200Der}
            y="153.423"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="155.423"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(204, rightPosition)}
            >•</tspan>
            <tspan
              id="TE204"
              x={PosicionX_Slips200Der}
              y="153.423"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(204) !== undefined ? getObjectSlip(204).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(204, 'right')}
            >
              {/* EMBARCACION 204 */}
              {/* {`${getObjectSlip(204) !== undefined ? `${getObjectSlip(204).Nombre_barco.substring(0,12)} ${getObjectSlip(204).Embarcacion !== 0 ? getObjectSlip(204).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(204, rightPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(204)}>{getTamañoEmbarcacion(204, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb203"
            x={PosicionX_Slips200Izq}
            y="152.966"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="154.966"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(203, leftPosition)}
            >•</tspan>
            <tspan
              id="TE203"
              x={PosicionX_Slips200Izq}
              y="152.966"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(203) !== undefined ? getObjectSlip(203).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(203, 'left')}
            >
              {/* EMBARCACION 203 */}
              {/* {`${getObjectSlip(203) !== undefined ? `${getObjectSlip(203).Nombre_barco.substring(0,12)} ${getObjectSlip(203).Embarcacion !== 0 ? getObjectSlip(203).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(203, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(203)}>{getTamañoEmbarcacion(203, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb202"
            x={PosicionX_Slips200Der}
            y="142.437"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="144.437"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(202, rightPosition)}
            >•</tspan>
            <tspan
              id="TE202"
              x={PosicionX_Slips200Der}
              y="142.437"
              strokeWidth="0.265"
              fontSize="4.939"
              className={getNombreClaseCSS(202, 'right')}
            >
              {/* EMBARCACION 202 */}
              {/* {`${getObjectSlip(202) !== undefined ? `${getObjectSlip(202).Nombre_barco.substring(0,12)} ${getObjectSlip(202).Embarcacion !== 0 ? getObjectSlip(202).Embarcacion : getObjectSlip(202).Compartido === "S" ? getObjectSlip(202).Observacion : "DISPONIBLE"}` : ""}`} */}
              {getNombreEmbarcacion(202, rightPosition)}
              
            </tspan>
            <tspan className={getColorLongitudCSS(202)}>{getTamañoEmbarcacion(202, rightPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb201"
            x={PosicionX_Slips200Izq}
            y="141.962"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="143.962"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(201, leftPosition)}
            >•</tspan>
            <tspan
              id="TE201"
              x={PosicionX_Slips200Izq}
              y="141.962"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(201) !== undefined ? getObjectSlip(201).Embarcacion !== 0 ? "text-color-normal" : getObjectSlip(201).Compartido === "S" ? "text-color-compartido" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(201, 'left')}
            >
              {/* EMBARCACION 201 */}
              {getNombreEmbarcacion(201, leftPosition)}

            </tspan>
            <tspan className={getColorLongitudCSS(201)}>{getTamañoEmbarcacion(201, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb110"
            x={PosicionX_Slips100Izq}
            y="322.756"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="324.756"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(110, leftPosition)}
            >•</tspan>
            <tspan
              id="TE110"
              x={PosicionX_Slips100Izq}
              y="322.756"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(110) !== undefined ? getObjectSlip(110).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(110, 'left')}
              >
              {/* EMBARCACION 110 */}
              {/* {`${getObjectSlip(110) !== undefined ? `${getObjectSlip(110).Nombre_barco.substring(0,12)} ${getObjectSlip(110).Embarcacion !== 0 ? getObjectSlip(110).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(110, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(110)}>{getTamañoEmbarcacion(110, leftPosition)}</tspan>
          </text>
          <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="TxtEmb110-A"
          x={PosicionX_Slips100Izq}
          y="328.302"
          display="inline"
          imageRendering="auto"
          transform="rotate(-.365)"
        >
          <tspan 
            y="330.302"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(111, leftPosition)}
            >•</tspan>
          <tspan
            id="TE110-2"
            x={PosicionX_Slips100Izq}
            y="328.302"
            strokeWidth="0.265"
            fontSize="4.939"
            // className={getObjectSlip(111) !== undefined ? getObjectSlip(111).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
            className={getNombreClaseCSS(111, 'left')}
          >
            {/* EMBARCACION 110-1 */}
            {/* {`${getObjectSlip(111) !== undefined ? `${getObjectSlip(111).Nombre_barco.substring(0,12)} ${getObjectSlip(111).Embarcacion !== 0 ? getObjectSlip(111).Embarcacion : "DISPONIBLE"}` : ""}`} */}
            {`${getNombreEmbarcacion(111, leftPosition)}`}
          </tspan>
          <tspan className={getColorLongitudCSS(111)}>{getTamañoEmbarcacion(111, leftPosition)}</tspan>
        </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb109"
            x={PosicionX_Slips100Izq}
            y="311.759"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="313.759"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(109, leftPosition)}
            >•</tspan>
            <tspan
              id="TE109"
              x={PosicionX_Slips100Izq}
              y="311.759"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(109) !== undefined ? getObjectSlip(109).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(109, 'left')}
            >
              {/* EMBARCACION 109 */}
              {/* {`${getObjectSlip(109) !== undefined ? `${getObjectSlip(109).Nombre_barco.substring(0,12)} ${getObjectSlip(109).Embarcacion !== 0 ? getObjectSlip(109).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(109, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(109)}>{getTamañoEmbarcacion(109, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb108"
            x={PosicionX_Slips100Izq}
            y="302.759"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="304.759"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(108, leftPosition)}
            >•</tspan>
            <tspan
              id="TE108"
              x={PosicionX_Slips100Izq}
              y="302.759"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(108) !== undefined ? getObjectSlip(108).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(108, 'left')}
            >
              {/* EMBARCACION 108 */}
              {/* {`${getObjectSlip(108) !== undefined ? `${getObjectSlip(108).Nombre_barco.substring(0,12)} ${getObjectSlip(108).Embarcacion !== 0 ? getObjectSlip(108).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(108, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(108)}>{getTamañoEmbarcacion(108, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb107"
            x={PosicionX_Slips100Izq}
            y="290.742"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="292.742"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(107, leftPosition)}
            >•</tspan>
            <tspan
              id="TE107"
              x={PosicionX_Slips100Izq}
              y="290.742"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(107) !== undefined ? getObjectSlip(107).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(107, 'left')}
            >
              {/* EMBARCACION 107 */}
              {/* {`${getObjectSlip(107) !== undefined ? `${getObjectSlip(107).Nombre_barco.substring(0,12)} ${getObjectSlip(107).Embarcacion !== 0 ? getObjectSlip(107).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(107, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(107)}>{getTamañoEmbarcacion(107, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb106"
            x={PosicionX_Slips100Izq}
            y="280.76"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="282.76"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(106, leftPosition)}
            >•</tspan>
            <tspan
              id="TE106"
              x={PosicionX_Slips100Izq}
              y="280.76"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(106) !== undefined ? getObjectSlip(106).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(106, 'left')}
            >
              {/* EMBARCACION 106 */}
              {/* {`${getObjectSlip(106) !== undefined ? `${getObjectSlip(106).Nombre_barco.substring(0,12)} ${getObjectSlip(106).Embarcacion !== 0 ? getObjectSlip(106).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(106, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(106)}>{getTamañoEmbarcacion(106, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb105"
            x={PosicionX_Slips100Izq}
            y="268.742"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="270.742"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(105, leftPosition)}
            >•</tspan>
            <tspan
              id="TE105"
              x={PosicionX_Slips100Izq}
              y="268.742"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(105) !== undefined ? getObjectSlip(105).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(105, 'left')}
            >
              {/* EMBARCACION 105 */}
              {/* {`${getObjectSlip(105) !== undefined ? `${getObjectSlip(105).Nombre_barco.substring(0,12)} ${getObjectSlip(105).Embarcacion !== 0 ? getObjectSlip(105).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(105, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(105)}>{getTamañoEmbarcacion(105, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb104"
            x={PosicionX_Slips100Izq}
            y="259.746"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="261.746"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(104, leftPosition)}
            >•</tspan>
            <tspan
              id="TE104"
              x={PosicionX_Slips100Izq}
              y="259.746"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(104) !== undefined ? getObjectSlip(104).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(104, 'left')}
              >
              {/* EMBARCACION 104 */}
              {/* {`${getObjectSlip(104) !== undefined ? `${getObjectSlip(104).Nombre_barco.substring(0,12)} ${getObjectSlip(104).Embarcacion !== 0 ? getObjectSlip(104).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(104, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(104)}>{getTamañoEmbarcacion(104, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb103"
            x={PosicionX_Slips100Izq}
            y="247.76"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="249.76"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(102, leftPosition)}
            >•</tspan>
            <tspan
              id="TE103"
              x={PosicionX_Slips100Izq}
              y="247.76"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(103) !== undefined ? getObjectSlip(103).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(103, 'left')}
            >
              {/* EMBARCACION 103 */}
              {/* {`${getObjectSlip(103) !== undefined ? `${getObjectSlip(103).Nombre_barco.substring(0,12)} ${getObjectSlip(103).Embarcacion !== 0 ? getObjectSlip(103).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(103, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(103)}>{getTamañoEmbarcacion(103, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb102"
            x={PosicionX_Slips100Izq}
            y="238.76"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
          >
            <tspan 
            y="240.76"
            // x={getBulletPosition(803, PosicionX_Slips800Izq, leftPosition)}
            className={getColorEnBaseFactura(102, leftPosition)}
            >•</tspan>
            <tspan
              id="TE102"
              x={PosicionX_Slips100Izq}
              y="238.76"
              strokeWidth="0.265"
              fontSize="4.939"
              // className={getObjectSlip(102) !== undefined ? getObjectSlip(102).Embarcacion !== 0 ? "text-color-normal" : "text-color-libre" : "text-color-normal"}
              className={getNombreClaseCSS(102, 'left')}
            >
              {/* EMBARCACION 102 */}
              {/* {`${getObjectSlip(102) !== undefined ? getObjectSlip(102).Nombre_barco : ""} ${getObjectSlip(102) !== undefined ? getObjectSlip(102).Embarcacion : ""}`} */}
              {/* {`${getObjectSlip(102) !== undefined ? `${getObjectSlip(102).Nombre_barco.substring(0,12)} ${getObjectSlip(102).Embarcacion !== 0 ? getObjectSlip(102).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              {`${getNombreEmbarcacion(102, leftPosition)}`}
            </tspan>
            <tspan className={getColorLongitudCSS(102)}>{getTamañoEmbarcacion(102, leftPosition)}</tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              InkscapeFontSpecification: "Arial",
              WebkitTextAlign: "center",
              textAlign: "center",
            }}
            id="TxtEmb101"
            x={PosicionX_Slips100Izq}
            y="225.756"
            display="inline"
            imageRendering="auto"
            transform="rotate(-.365)"
            

          >
            
            <tspan
              id="TE101"
              x={PosicionX_Slips100Izq}
              y="225.756"
              strokeWidth="0.265"
              fontSize="4.939"
              // className='text-color-normal'
              className="text-color-combustible slips-names-align-left"
            >
              {/* EMBARCACION 101 */}
              {/* {`${getObjectSlip(101) !== undefined ? `${getObjectSlip(101).Nombre_barco.substring(0,12)} ${getObjectSlip(101).Embarcacion !== 0 ? getObjectSlip(101).Embarcacion : "DISPONIBLE"}` : ""}`} */}
              COMBUSTIBLE
            </tspan>
          </text>
        </g>
        <g
        id="layer4"
        fill="#000"
        strokeWidth="0.265"
        fontFamily="Arial"
        fontSize="8.467"
        textAnchor="middle"
      >
        <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="text4218"
          x={NombresIndicadoresX}
          y="51.388"
        >
          <tspan
            id="tspan4216"
            x={NombresIndicadoresX}
            y="51.388"
            strokeWidth="0.265"
            className='text-style-slips-disponibles'
          >
            {/* Slips Disponibles: */}
            {`Slips Disponibles`}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="text5004"
          x={ValoresIndicadoresX}
          y="52.103"
          
        >
          <tspan id="tspan5002" x={ValoresIndicadoresX} y="52.103" strokeWidth="0.265" className='text-style-slips-disponibles'>
            {/* {indicators.AvailableSlips} */}
            {`:  ${indicators.AvailableSlips}`}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="text4218-1"
          x={NombresIndicadoresX}
          y="63.723"
        >
          <tspan
            id="tspan4216-2"
            x={NombresIndicadoresX}
            y="63.723"
            strokeWidth="0.265"
            className='text-style-slips-ocupados'
          >
            {/* Slips Ocupados :{" "} */}
            {`Slips Ocupados`}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="text5004-5"
          x={ValoresIndicadoresX}
          y="63.645"
        >
          <tspan id="tspan5002-5" x={ValoresIndicadoresX} y="63.645" strokeWidth="0.265" className='text-style-slips-ocupados'>
            {/* {indicators.BusySlips} */}
            {`:  ${indicators.BusySlips}`}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="text4218-1-4"
          x={NombresIndicadoresX}
          y="76.917"
        >
          <tspan
            id="tspan4216-2-0"
            x={NombresIndicadoresX}
            y="76.917"
            strokeWidth="0.265"
            className='text-style-slips-totales'
          >
            {/* Total de Slips :{" "} */}
            {/* {`Total de Slips       :`} */}
            {`Total de Slips`}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="text5004-5-0"
          x={ValoresIndicadoresX}
          y="76.839"
        >
          <tspan id="tspan5002-5-6" x={ValoresIndicadoresX} y="76.839" strokeWidth="0.265" className='text-style-slips-totales'>
            {`:  ${indicators.TotalSlips}`}
          </tspan>
        </text>
      </g>
      <g id="layer5">
        <text
          xmlSpace="preserve"
          style={{
            InkscapeFontSpecification: "Arial",
            WebkitTextAlign: "center",
            textAlign: "center",
          }}
          id="text7082"
          x="336.061"
          y="125.11"
          fill="#000"
          strokeWidth="0.265"
          fontFamily="Arial"
          fontSize="42.333"
          // opacity="0.3"
          opacity={!errorConnection ? "0" : "0.3"}
          textAnchor="middle"
          // className='animate__animated animate__zoomIn'
          // className='animate__animated animate__zoomInDown'
          className={!errorConnection ? "" : "animate__animated animate__zoomInDown"}
        >
          <tspan
            id="tspan7133"
            x="336.061"
            y="125.11"
            strokeWidth="0.265"
            fontSize="42.333"
          >
            ERROR EN CONEXIÓN CON
          </tspan>
          <tspan
            id="tspan7084"
            x="336.061"
            y="178.027"
            strokeWidth="0.265"
            fontSize="42.333"
          >
            EL SERVIDOR.
          </tspan>
        </text>
      </g>
      </svg>
     {/* <ToastContainer/> */}
     

    </>
  )
}

export default TableroEmbarcaciones